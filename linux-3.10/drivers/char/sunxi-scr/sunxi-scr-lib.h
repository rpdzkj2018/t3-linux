//
#ifndef __SUNXI_SCR_LIB_H__
#define __SUNXI_SCR_LIB_H__
//
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/err.h>
//
s32 scr_get_base(struct device_node **pnode,
                void __iomem **base, s8 *compatible);

void scr_put_base(struct device_node *pnode, void __iomem *base);

#endif
