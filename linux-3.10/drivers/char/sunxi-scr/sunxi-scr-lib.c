//head
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/err.h>

#include "sunxi-scr.h"
#include "sunxi-scr-lib.h"

s32 scr_get_base(struct device_node **pnode,
                void __iomem **base, s8 *compatible)
{
        *pnode = of_find_compatible_node(NULL, NULL, compatible);
        if (IS_ERR_OR_NULL(*pnode)) {
                SCR_ERR("Failed to find \"%s\" in dts.\n", compatible);
                return -ENXIO;
        }

        *base = of_iomap(*pnode, 0); /* reg[0] must be accessable. */
        if (*base == NULL) {
                SCR_ERR("Unable to remap IO\n");
                return -ENXIO;
        }
        SCR_DBG("Base addr of \"%s\" is %p\n", compatible, *base);
        return 0;
}

void scr_put_base(struct device_node *pnode, void __iomem *base)
{
        SCR_DBG("base = %p\n", base);
        if (base)
                iounmap(base);
        if (pnode)
                of_node_put(pnode);
}
