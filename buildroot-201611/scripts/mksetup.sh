# scripts/mksetup.sh
#
# (c) Copyright 2013
# Allwinner Technology Co., Ltd. <www.allwinnertech.com>
# James Deng <csjamesdeng@allwinnertech.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

. buildroot-201611/scripts/mkcmd.sh

function mksetup()
{
    init_disclaimer
    if [ -f ".buildconfig" ];then
    	. .buildconfig
    	export PRE_LICHEE_GNUEABI=$LICHEE_GNUEABI
    	echo $PRE_LICHEE_GNUEABI
    	unset LICHEE_CHIP
    	unset LICHEE_ARCH
    	unset LICHEE_PLATFORM
    	unset LICHEE_KERN_VER
    	unset LICHEE_BOARD
    	unset LICHEE_GNUEABI
    else
    	PRE_LICHEE_GNUEABI="NULL"
    	echo "no .buildconfig"
    fi
    
    
    rm -f .buildconfig
    printf "\n"
    printf "Welcome to mkscript setup progress\n"

    select_board

    init_defconf
}

# Setup all variables in setup.
mksetup

