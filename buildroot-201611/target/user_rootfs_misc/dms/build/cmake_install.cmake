# Install script for directory: /home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/bin/dms" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/bin/dms")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/bin/dms"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/bin/dms")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/bin" TYPE EXECUTABLE FILES "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/build/dms")
  if(EXISTS "$ENV{DESTDIR}/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/bin/dms" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/bin/dms")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/bin/dms"
         OLD_RPATH "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib:/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/opencv:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/bin/dms")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libhobotdms_sdk.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libx264.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libturbojpeg.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libhobotdms_hal.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libhobot_dms.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libxvidcore.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libv4l2.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libjpeg.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libdms_sdk_protobuf_parser.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libstorage_manager.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libupload_config.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libopencv_world.so;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libturbojpeg.so.0;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libxvidcore.so.4;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libx264.so.148;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libjpeg.so.62.2.0;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libopencv_world.so.3.1.0;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libturbojpeg.so.0.1.0;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libxvidcore.so.4.3;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libopencv_world.so.3.1;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libjpeg.so.62;/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib/libv4l2.so.0")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/lib" TYPE FILE FILES
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libhobotdms_sdk.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libx264.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libturbojpeg.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libhobotdms_hal.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libhobot_dms.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libxvidcore.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libv4l2.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libjpeg.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libdms_sdk_protobuf_parser.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libstorage_manager.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libupload_config.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libopencv_world.so"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libturbojpeg.so.0"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libxvidcore.so.4"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libx264.so.148"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libjpeg.so.62.2.0"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libopencv_world.so.3.1.0"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libturbojpeg.so.0.1.0"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libxvidcore.so.4.3"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libopencv_world.so.3.1"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libjpeg.so.62"
    "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/lib/libv4l2.so.0"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/etc")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner" TYPE DIRECTORY FILES "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/etc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner/data")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/allwinner" TYPE DIRECTORY FILES "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/data")
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
