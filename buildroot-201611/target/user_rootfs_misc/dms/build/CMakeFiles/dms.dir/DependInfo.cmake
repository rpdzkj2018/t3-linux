# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/src/main.cc" "/home/huanglibin/t7linux/buildroot-201611/target/user_rootfs_misc/dms/build/CMakeFiles/dms.dir/src/main.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../include/google"
  "../include/hobot-dms"
  "../include/ooencv"
  "../include/opencv2"
  "../include/hobotlog"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
