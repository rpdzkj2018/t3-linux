//
// Copyright 2017 Horizon Robotics.
//

#ifndef DMS_DMS_H_
#define DMS_DMS_H_

#include <hobot-dms/base/base.h>

#ifdef __GNUC__
#ifdef DMS_EXPORT
#define DMS_API __attribute__((visibility("default")))
#else
#define DMS_API
#endif
#define __stdcall
#elif defined(_WIN32)
#ifdef DMS_EXPORT
#define DMS_API __declspec(dllexport)
#else
#define DMS_API __declspec(dllimport)
#endif
#else
#error "No platform specified!"
#endif

#ifdef __cplusplus
extern "C" {
#endif

DMS_API int __stdcall DMS_GetVersion();
DMS_API int __stdcall DMS_Init(const char *config_file,
                               const DMSUVCParams *uvc_params);
DMS_API void __stdcall DMS_Fini();
DMS_API int __stdcall DMS_FeedImage(const DMSImage *image);
DMS_API int __stdcall DMS_FeedCAN(const DMSCANInfo *can);
DMS_API int __stdcall DMS_FeedJNI(const DMSJNIInfo *jni);
DMS_API int __stdcall DMS_GetOutput(DMSOutputData *output, bool with_image);
DMS_API int __stdcall DMS_SetCallBack(uint32_t type, DMSCallBack callbak);
DMS_API int __stdcall DMS_RunImageCheck(void);
DMS_API int __stdcall DMS_FeedImei(const char *imei, int length);
#ifdef __cplusplus
}
#endif

#endif  // DMS_DMS_H_
