//
// Copyright 2016 Horizon Robotics.
//

#ifndef SRC_MODULES_OUTPUT_DISPLAY_MODULE_H_
#define SRC_MODULES_OUTPUT_DISPLAY_MODULE_H_

#include "hobot-dms/base/dms_module.h"

namespace HobotDMS {
namespace HAL {
class Display;
}

class DisplayModule : public DMSModule {
 public:
  DisplayModule();
  virtual ~DisplayModule();

  int DMSModuleInit(hobot::RunContext *context) override;

  void Reset() override;
  void Fini();

  FORWARD_DECLARE(DisplayModule, 0);

 private:
  HAL::Display *mp_display_dev;
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_OUTPUT_DISPLAY_MODULE_H_
