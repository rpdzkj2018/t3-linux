//
// Copyright 2017 Horizon Robotics.
//

#ifndef SRC_MODULES_OUTPUTCOLLECT_MODULE_H_
#define SRC_MODULES_OUTPUTCOLLECT_MODULE_H_

#include <atomic>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_module.h"

#ifdef TIME_TO_CRASH
#include <chrono>
#endif

namespace HobotDMS {

class OutputCollectModule : public DMSModule {
 public:
  OutputCollectModule();
  virtual ~OutputCollectModule();

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void OnConfigUpdate() override;
  void Fini();

  FORWARD_DECLARE(OutputCollectModule, 0);

 private:
  std::atomic<int> output_img_mode_;
  std::vector<uint8_t> jpg_buf_;
  int jpg_in_size_;
#ifdef TIME_TO_CRASH
  static std::chrono::time_point<std::chrono::system_clock> startTime;
  static std::chrono::seconds limitTime;
  std::chrono::time_point<std::chrono::system_clock> curTime;
#endif
};

}  // end of namespace HobotDMS

#endif  //  SRC_MODULES_OUTPUTCOLLECT_MODULE_H_
