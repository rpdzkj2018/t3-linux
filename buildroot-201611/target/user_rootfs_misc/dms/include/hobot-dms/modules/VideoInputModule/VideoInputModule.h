//
// Copyright 2017 Horizon Robotics.
//

#ifndef SRC_MODULES_VIDEOINPUT_MODULE_H_
#define SRC_MODULES_VIDEOINPUT_MODULE_H_

#include <string>
#include "hal/videoio.h"
#include "hobot-dms/base/dms_module.h"

namespace HobotDMS {

class VideoInputModule : public DMSModule {
 public:
  VideoInputModule();
  ~VideoInputModule();

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

  FORWARD_DECLARE(VideoInputModule, 0);

 private:
  HAL::VideoDecoder *mp_video_decoder;
  int64_t m_video_pos;
#ifdef HR_x86
  std::string m_save_img_dir;
#endif
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_VIDEOINPUT_MODULE_H_
