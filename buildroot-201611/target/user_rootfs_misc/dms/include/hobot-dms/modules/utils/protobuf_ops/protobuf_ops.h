#ifndef SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_PROTOBUF_OPS_H_
#define SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_PROTOBUF_OPS_H_

#include <stdint.h>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include "hobot-dms/base/message/frame.h"
#include "hobot-dms/base/utils/logging/DMSLog.h"
#include "hobot-dms/base/utils/serialize/serializer.h"

namespace HobotDMS {
#define VERSION 2018050301

/// class DMSProtoWriter
class DMSProtoWriter {
 public:
  int startWriter(const std::string &filename, int64_t version = VERSION);
  int write(Frame &frame);
  void stopWriter();

 private:
  std::string mFileName;
  std::ofstream mOfs;
  MetaSerializer *m_serializer = nullptr;
};

/// class DMSProtoReader
class DMSProtoReader {
  typedef struct ProtoInfo {
    size_t prtPos;
    size_t prtLen;
  } ProtoInfo_t;

 public:
  int startReader(const std::string &filename);
  int getPos() { return mInput_cnt; }
  int readOne(Frame *pFrame, bool image_info = true, bool algo_info = true,
              bool policy_info = true, bool event_info = true,
              bool veh_info = true, bool check_info = true);
  int seekTo(int pos);
  int getFrameCnt(void);
  void stopReader();

 private:
  MetaDeserializer *m_deserializer = nullptr;
  inline size_t GetFileSize(std::ifstream &ifs) {
    // 保留原始位置
    size_t cur_pos = ifs.tellg();
    ifs.seekg(0, ifs.end);
    size_t size = ifs.tellg();
    ifs.seekg(cur_pos);
    return size;
  }
  std::string mFileName;
  std::ifstream mIfs;
  std::vector<char> mProtoBufVec;
  std::vector<ProtoInfo_t> mProtoInfoVec;
  int mInput_cnt;
};

}  // namespace HobotDMS

#endif  // SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_PROTOBUF_OPS_H_
