//
// Copyright 2017 Horizon Robotics.
//
#ifndef SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_INSPARSER_H_
#define SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_INSPARSER_H_

#include "hal/ins_info.h"

namespace HobotDMS {
class VehicleInfoFrame;
class INSParser {
 public:
  INSParser() = default;
  ~INSParser() = default;
  // TODO related config file
  int Init();
  void Fini();
  int Parse2VehInfo(const INSInfo &ins, VehicleInfoFrame &veh);

 private:
};
}
#endif  // SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_INSPARSER_H_
