//
// Copyright 2017 Horizon Robotics.
//
#ifndef SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_FILE_UTILS_H_
#define SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_FILE_UTILS_H_

#include <fstream>
#include <string>
#include <vector>
namespace HobotDMS {
// split filename(abs or related to rel_root) to full path and name
int split_dir_name(const std::string &rel_root, const std::string &name_in,
                   std::string *dir_out, std::string *name_out);
size_t GetFileSize(std::ifstream &ifs);
int mk_multi_dirs(std::string dst_dirs);
bool get_re_filelist(const std::string &path_, const std::string &re_str_,
                     std::vector<std::string> &result_);
int copy_file(const std::string &src, const std::string &dst);
int rename_file(const std::string &src, const std::string &dst);
// if failed, errno will set
int dir_or_file_exist(const std::string &file_name);
// only could rm file, if failed, errno will set
int rm_file(const std::string &file_name);
// get full path, if path_name is give, they will add
// std::string get_full_path(const std::string path_name = "");
}  // namespace HobotDMS
#endif  // SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_FILE_UTILS_H_
