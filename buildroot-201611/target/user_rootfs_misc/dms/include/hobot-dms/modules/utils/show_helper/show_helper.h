//
// CopyRight 2017 Horizon Robotics.
//
#ifndef SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_SHOW_HELPER_SHOW_HELPER_H_
#define SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_SHOW_HELPER_SHOW_HELPER_H_

#include <deque>
#include <vector>
#include "hobot-dms/base/message/frame.h"
#include "hobot-dms/base/utils/logging/DMSLog.h"
#include "opencv2/opencv.hpp"
#include "vision_type/vision_type.hpp"

namespace HobotDMS {
/// class BaseShow
template <typename T>
class BaseShow {
 public:
  /*
         * N: N种数据集
         * M: 数据长度为M
         * point_: 数据绘图框左上坐标点位置
         * width: 绘图框宽度
         * height: 绘图框高度
         * num_of_mark: 在width的基础上, 共有num_of_mark个刻度
         * extend_height: 额外的高度
         * extend_width: 额外的宽度
         */
  BaseShow(int color_flag_, int N, int M, const Point &point_,
           const int &width_ = 200, const int &height_ = 100,
           const int &num_of_mark_ = 10, const int &extend_height = 29,
           const int &extend_width = 30);

  virtual ~BaseShow();
  // 向索引为index_的数据列中压入数据.
  virtual bool pushData(const int &index_, const T &t_);
  // 压入脸部有无数据
  virtual bool pushFaceData(const int &hasface_);
  // 对数据进行裁剪 new_data= (old_data - offset_ol_) / offset_hi_
  virtual void setThreshold(const int &index_, const float &offset_hi_ = 1.0,
                            const float &offset_lo_ = 0.0);
  // 对索引为index_的数据集设置显示颜色
  virtual bool setColor(const int &index_, const cv::Scalar &color_);
  // 得到数据宽高
  virtual void getSize(int &N_, int &M_);
  // 绘图
  virtual void showAll(cv::Mat &rMat_);

 protected:
  std::vector<std::deque<T>> m_data;            // 数据
  std::vector<cv::Scalar> m_data_color;         // 数据颜色
  std::vector<float> m_offset_lo, m_offset_hi;  // 截断参数
  std::deque<int> m_has_face;                   // 有无脸
  cv::Point m_left_up_point;                    // 坐上坐标
  int m_data_length;                            // 数据长度
  int m_num_of_mark;                            // 刻度数目
  int m_draw_height, m_draw_width;              // 数据绘制区域长宽
  int m_all_height, m_all_width;                // 总区域长宽
  int m_N, m_M;                                 // 数据长宽

 protected:
  virtual bool checkDataSize();           // 检查数据长度, 少补多删
  virtual void showAxis(cv::Mat &rMat_);  // 绘制坐标轴
  virtual void showBackGround(cv::Mat &rMat_);  // 绘制背景
  virtual void dataOperate();  // 在数据推入之前,进行数据运算
  virtual void userOperate(cv::Mat &rMat_);   // 用户自定义运算
  virtual void drawData(cv::Mat &rMat_) = 0;  // 绘制图像
  int m_color_flag;
};

// class to show left eye open score.
class C_ShowLeftEyeScore : public BaseShow<float> {
 public:
  C_ShowLeftEyeScore(int color_flag_, const Point &point_,
                     const int &width_ = 200, const int &height_ = 100,
                     const int &num_of_mark_ = 10);
  ~C_ShowLeftEyeScore();

 protected:
  void drawData(cv::Mat &rMat_);
};

// class to show right eye open score.
class C_ShowRightEyeScore : public BaseShow<float> {
 public:
  C_ShowRightEyeScore(int color_flag_, const Point &point_,
                      const int &width_ = 200, const int &height_ = 100,
                      const int &num_of_mark_ = 10);
  ~C_ShowRightEyeScore();

 protected:
  void drawData(cv::Mat &rMat_);
};

// class to show phone clsf exist score
class C_ShowPhoneClsfScore : public BaseShow<float> {
 public:
  C_ShowPhoneClsfScore(int color_flag_, const Point &point_,
                       const int &width_ = 200, const int &height_ = 100,
                       const int &num_of_mark_ = 10);
  ~C_ShowPhoneClsfScore();
  float m_avr_exist_score;

 protected:
  void drawData(cv::Mat &rMat_);
};

// class to show smoke clsf exist score
class C_ShowSmokeClsfScore : public BaseShow<float> {
 public:
  C_ShowSmokeClsfScore(int color_flag_, const Point &point_,
                       const int &width_ = 200, const int &height_ = 100,
                       const int &num_of_mark_ = 10);
  ~C_ShowSmokeClsfScore();
  float m_avr_exist_score;

 protected:
  void drawData(cv::Mat &rMat_);
};


// class to show status
class C_ShowStatus : public BaseShow<warn_state> {
 public:
  C_ShowStatus(int color_flag_, const Point &point_, const int &width_ = 200,
               const int &height_ = 100, const int &num_of_mark_ = 10,
               const int &extend_height_ = 0, const int &extend_width_ = 30);
  ~C_ShowStatus();
  bool pushData(const size_t &index_, const warn_state &t_);

 protected:
  void drawData(cv::Mat &rMat_);
  void showAxis(cv::Mat &rMat_);

 private:
  float m_none = 0.0;
  float m_w_noface = 0.1;
  float m_e_noface = 0.1;
  float m_w_distract = 0.4;
  float m_e_distract = 0.4;
  float m_w_fatigue = 0.7;
  float m_e_fatigue = 0.7;
  float m_danger = 0.1;
};

// class to show time
class C_ShowTime : public BaseShow<double> {
 public:
  C_ShowTime(int color_flag_, const Point &point_, const int &width_ = 200,
             const int &height_ = 20, const int &num_of_mark_ = 5);
  ~C_ShowTime();

 protected:
  void drawData(cv::Mat &rMat_);
  void showAxis(cv::Mat &rMat_);
};

// class to show Dist
class C_Show_Dist_0 : public BaseShow<float> {
 public:
  C_Show_Dist_0(int color_flag_, const Point &point_, const int &width_ = 200,
                const int &height_ = 100, const int &num_of_mark_ = 10);
  ~C_Show_Dist_0();

 protected:
  void drawData(cv::Mat &rMat_);
};
class C_Show_Dist_1 : public BaseShow<float> {
 public:
  C_Show_Dist_1(int color_flag_, const Point &point_, const int &width_ = 200,
                const int &height_ = 100, const int &num_of_mark_ = 10);
  ~C_Show_Dist_1();

 protected:
  void drawData(cv::Mat &rMat_);
};
class C_Show_Dist_2 : public BaseShow<float> {
 public:
  C_Show_Dist_2(int color_flag_, const Point &point_, const int &width_ = 200,
                const int &height_ = 100, const int &num_of_mark_ = 10);
  ~C_Show_Dist_2();

 protected:
  void drawData(cv::Mat &rMat_);
};

// TODO(others)  步骤一, 继承Base<typename T>
class C_Show_Dist_3 : public BaseShow<float> {
 public:
  C_Show_Dist_3(int color_flag_, const Point &point_, const int &width_ = 200,
                const int &height_ = 100, const int &num_of_mark_ = 10,
                const int &extend_height_ = 45, const int &extend_width_ = 30);
  ~C_Show_Dist_3();

 protected:
  // TODO(others): 步骤二, 重载必要的函数, 必须要重载drawData函数
  void drawData(cv::Mat &rMat_);
};

///  Show_helper 类, 集成所有子绘图类.
class Show_helper {
 public:
  void Gray2RGB(cv::Mat &rMat_);
  void ShowScoreECG(cv::Mat &rMat_, const Frame *pFrame_);
  void ShowFaceDir(cv::Mat &rMat_, const WarningFrame *pWarningFrame_);
  void ShowFaceDirBox(cv::Mat &rMat_, const LDMKFrame *pLDMKFrame_);
  void ShowLandmark(cv::Mat &rMat_, const LDMKFrame *pLandmark_);
  void ShowOriginalFaceROIs(cv::Mat &rMat_, const FaceFrame *pFaceFrame_);
  void ShowFilteredFaceROIs(cv::Mat &rMat_, const FaceFrame *pFaceFrame_);
  void ShowDetectFaceROI(cv::Mat &rMat_, const FaceFrame *pFaceFrame_);
  void ShowSmokeROI(cv::Mat &rMat_, const SmokeClsfFrame *pSmokeFrame_);
  void ShowPhoneClsfROI(cv::Mat &rMat_, const CellphoneFrame *pCellpFrame);
  void ShowEyeROIs(cv::Mat &rMat_, const EyeClsfFrame *pEyeClsFrame_);
  void ShowWarnResult(cv::Mat &rMat_, const WarningFrame *pWarningFrame_);
  void ShowEventResult(cv::Mat &rMat_, const EventFrame *pEventFrame);
  void ShowVehicleInfo(cv::Mat &rMat_, const VehicleInfoFrame &pVehicle_,
                       const cv::Point &point = cv::Point(980, 500),
                       float fontscale = 0.8);
  void ShowEyeNose(cv::Mat &rMat_, const LDMKFrame *pLDMKFrame_,
                   const WarningFrame *pWarningFrame_);
  void ShowID(cv::Mat &rMat_, const Frame *pFrame_);
  void ShowEyesInfo(cv::Mat &rMat_, const EyeClsfFrame *pEyeFrame_);
  bool Reset();
  Show_helper() = delete;
  explicit Show_helper(int color_flag_);
  ~Show_helper();

 private:
  template <typename Dtype>
  void ShowRectColor(cv::Mat &rMat_, const Rect_<Dtype> &rect,
                     const cv::Scalar &color = cv::Scalar(255, 255, 255)) {
    int l = static_cast<int>(rect.l);
    int r = static_cast<int>(rect.r);
    int t = static_cast<int>(rect.t);
    int b = static_cast<int>(rect.b);
    cv::rectangle(rMat_, cv::Point(l, t), cv::Point(r, b), color, 1);
  }

  BaseShow<float> *m_show_lefteyescore = nullptr;
  BaseShow<float> *m_show_righteyescore = nullptr;
  BaseShow<float> *m_show_phoneclsfscore = nullptr;
  BaseShow<float> *m_show_smokeclsfscore = nullptr;
  BaseShow<float> *m_show_lefteyescore_avr = nullptr;
  BaseShow<float> *m_show_righteyescore_avr = nullptr;
  // BaseShow<warn_state> *m_show_status = nullptr;
  BaseShow<double> *m_show_time = nullptr;
  BaseShow<float> *m_show_dist_0 = nullptr;
  BaseShow<float> *m_show_dist_1 = nullptr;
  BaseShow<float> *m_show_dist_2 = nullptr;
  // TODO(others)  步骤三, 在Show_helper 中声明
  BaseShow<float> *m_show_dist_3 = nullptr;
  int m_color_flag;
};

}  // namespace HobotDMS

#endif  // SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_SHOW_HELPER_SHOW_HELPER_H_
