//
// Copyright 2018 Horizon Robotics.
//
#ifndef SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_CANPARSER_H_
#define SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_CANPARSER_H_

#include "hal/can_info.h"

namespace HobotDMS {
class VehicleInfoFrame;
class CANParser {
 public:
  CANParser() = default;
  ~CANParser() = default;
  // TODO related config file
  int Init();
  void Fini();
  int Parse2VehInfo(const CANInfo_ &can, VehicleInfoFrame &veh);

 private:
};
}
#endif  // SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_UTILS_CANPARSER_H_
