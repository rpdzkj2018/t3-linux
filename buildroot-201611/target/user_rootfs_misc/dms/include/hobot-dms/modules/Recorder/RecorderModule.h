//
// Copyright 2017 Horizon Robotics.
//
#ifndef SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_RECORDER_RECORDERMODULE_H_
#define SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_RECORDER_RECORDERMODULE_H_

#include <hobot-dms/base/dms_module.h>
#include <unistd.h>
#include "hal/videoio.h"
#include "hobot-dms/base/utils/serialize/serializer.h"

namespace HobotDMS {
class DMSProtoWriter;
class RecorderModule : public DMSModule {
 public:
  RecorderModule();
  ~RecorderModule();
  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();
  FORWARD_DECLARE(RecorderModule, 0);

 private:
#ifdef ENABLE_STORAGE
  bool checkStorageAndWriter(int64_t frame_time);
#endif
  bool useCustomNameAndWriter();
  int64_t m_start_time;
  std::string m_file_path;
  int m_video_height = -1;
  int m_video_width = -1;
  HAL::FrameFormat m_fmt;
  DMSProtoWriter *mp_proto_file_writer;
  HAL::VideoEncoder *mp_encoder;
  int m_enable_video;
  int m_enable_proto;
  int m_use_custom_name;
  std::string m_out_video_name;
  std::string m_out_proto_name;

  HobotDMSContext *dms_context;
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_RECORDER_RECORDERMODULE_H_
