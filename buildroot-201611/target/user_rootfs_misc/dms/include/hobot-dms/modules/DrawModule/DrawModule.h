//
// Copyright 2016 Horizon Robotics.
//

#ifndef SRC_MODULES_OUTPUT_DRAW_MODULE_H_
#define SRC_MODULES_OUTPUT_DRAW_MODULE_H_

#include <hobot/hobot.h>
#include "hobot-dms/base/dms_module.h"
#include "opencv2/opencv.hpp"

namespace HobotDMS {

class Show_helper;
class DrawModule : public DMSModule {
 public:
  DrawModule();
  virtual ~DrawModule();

  int DMSModuleInit(hobot::RunContext *context) override;

  void Reset() override;
  void Fini();

  FORWARD_DECLARE(DrawModule, 0);

 private:
  Show_helper *mp_show_helper;
  cv::Mat m_bkgnd;
  cv::Mat m_logo;
  cv::Mat m_show;
  int m_draw_color;
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_OUTPUT_DRAW_MODULE_H_
