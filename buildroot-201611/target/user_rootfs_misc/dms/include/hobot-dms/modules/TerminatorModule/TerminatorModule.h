//
// Copyright 2016 Horizon Robotics.
//

#ifndef SRC_MODULES_OUTPUT_OUTPUT_MODULE_H_
#define SRC_MODULES_OUTPUT_OUTPUT_MODULE_H_

#include <list>

#include <hobot/hobot.h>

#include "hobot-dms/base/dms_module.h"

namespace HobotDMS {

class TerminatorModule : public DMSModule {
 public:
  TerminatorModule();
  virtual ~TerminatorModule();

  int DMSModuleInit(hobot::RunContext *context) override;

  void Reset() override;
  void Fini();

  FORWARD_DECLARE(TerminatorModule, 0);

 private:
  int64_t m_cur_can_tms;

};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_OUTPUT_OUTPUT_MODULE_H_
