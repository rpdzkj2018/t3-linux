//
// Copyright 2018 Horizon Robotics.
//
// Created by Colin Chu (liuhuo.chu@hobot.cc)
// 2018-07-07

#ifndef SRC_FACE_ORIENTATION_H_
#define SRC_FACE_ORIENTATION_H_

#include <memory>
#include "hobot-dms/base/message/ldmk_frame.h"
#include "opencv2/opencv.hpp"

namespace HobotDMS {

class OrientationEstimate {
 public:
  OrientationEstimate(
      std::vector<cv::Point3d> &model_pts, cv::Mat &cam_matrix,
      cv::Mat dist_coeffs = cv::Mat::zeros(4, 1, cv::DataType<double>::type));
  ~OrientationEstimate(){};

  virtual bool Estimate(std::vector<cv::Point2d> &img_pts, cv::Mat &rvec,
                        cv::Mat &tvec);

  virtual float ReprojectError(std::vector<cv::Point2d> &img_pts, cv::Mat &rvec,
                               cv::Mat &tvec,
                               std::vector<cv::Point2d> &proj_pts);


 protected:
  std::vector<cv::Point3d> m_model_pts;
  cv::Mat m_cam_matrix;
  cv::Mat m_dist_coeffs;
};

class FaceOrientation : public OrientationEstimate {
 public:
  FaceOrientation(
      bool f_auto_correct = true,
      std::vector<cv::Point3d> model_pts = std::vector<cv::Point3d>(),
      std::vector<int> Ldmk_idxs = std::vector<int>(),
      cv::Mat cam_matrix = cv::Mat(),
      cv::Mat dist_coeffs = cv::Mat::zeros(4, 1, cv::DataType<double>::type));
  ~FaceOrientation(){};

  virtual bool Estimate(Landmarks21_f ldmk, float faceH_, Vector_3f &rvec,
                        Vector_3f &tvec);
  virtual void Reset();

 private:
  std::vector<int> m_Ldmk_idxs;

  // Variables for auto correct the face model
  enum {
    AUTO_CORRECT_NONE = 0,
    AUTO_CORRECT_L1 = 1,
    AUTO_CORRECT_L2 = 2,
    AUTO_CORRECT_L3 = 3
  };
  void SetAutoCorrectLevel(int level);
  void AutoCorrectStateTrans(float proj_err);
  bool DoCorrect(std::vector<cv::Point2d> &img_pts,
                 std::vector<cv::Point2d> &proj_pts, float &proj_err);
  bool m_auto_correct;
  int m_correct_state;
  int m_Ldmk_diff_cnt;
  int m_Ldmk_diffs;
  float m_rot_lim;
  std::vector<float> m_Ldmk_diff_x;
  std::vector<float> m_Ldmk_diff_y;
  std::vector<cv::Point3d> m_model_pts_origin;
};
}

#endif  // SRC_MODULES_LDMK_MODULE_H_
