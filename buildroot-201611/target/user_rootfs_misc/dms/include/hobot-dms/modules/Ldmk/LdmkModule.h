//
// Copyright 2017 Horizon Robotics.
/* DMS代码快要被ADAS的人毁了, 文特我搞不定了......*/
//

#ifndef SRC_MODULES_LDMK_MODULE_H_
#define SRC_MODULES_LDMK_MODULE_H_

#include <SimpleIni/SimpleIniExt.h>
#include <memory>
#include "face_orientation.h"
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/dms_typedef.h"
#include "hobot-dms/base/message/ldmk_frame.h"
#include "hobot-dms/base/utils/quaternion.h"

namespace dms_sdk_api {
class LDMKDetect;
}

namespace hobot {
class MXNetFaceKeyPoint;
}
namespace HobotDMS {

class Frame;
class LDMKInfo_Local;
class LDMKInfo_Post;
class PostDynParams;
class PostStaticParams;
class LDMKInfo_Local;

typedef enum face_calib_state_ {
  FACE_CALIB_STATE_NONE = 0,
  FACE_CALIB_STATE_START = 1,
  FACE_CALIB_STATE_FINISHED = 2,
  FACE_CALIB_STATE_FAILED = 3,
  FACE_CALIB_STATE_SIZE = 4
} face_calib_state;

typedef enum face_calib_type_ {
  FACE_CALIB_TYPE_NONE = 0,
  FACE_CALIB_TYPE_AUTO = 1,
  FACE_CALIB_TYPE_MANUAL = 2,
  FACE_CALIB_TYPE_SIZE = 3
} face_calib_type;

class LDMKModule : public DMSSDKModule {
 public:
  LDMKModule();
  ~LDMKModule();

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

  FORWARD_DECLARE(LDMKModule, 0);

 private:
  int DoProcess(Frame *frame, cv::Mat &img_mat,
                dms_sdk_api::PFaceAttrBLF &faceinfo);
  std::unique_ptr<hobot::MXNetFaceKeyPoint> mp_detector;
  int InitDetectHandle(hobot::RunContext *context);
  void PostProcess(Frame *);  // 后处理函数
  void PostProcess_DYA(Frame *);
  void PostProcess_RPY(Frame *);
  std::deque<std::shared_ptr<LDMKInfo_Local>> m_post_data_ldmkinfo;
  std::deque<std::shared_ptr<LDMKInfo_Local>> m_post_data_ldmkinfo_dya;

  LDMKInfo_Post m_post_local_data;
  LDMKInfo_Post m_post_local_data_2;

  TimeStamp m_TimeLastFace;

  int m_CalibFrameCnt;
  Quaternion m_CalibRotQ;
  Vector_3f m_CalibTvec;
  float m_SumMth2FaceH;
  int m_GradientTms;
  Vector_3f m_LastRvec;
  Vector_3f m_LastTvec;
  Vector_3f m_SumTvec;
  FaceOrientation m_FaceOrientation;
  dms_sdk_api::LDMKDetect *mp_ldmk_detect;

  /*************** FACE   CALIB   *********************/
 private:
  int m_calib_frames;
  int m_calib_failed_frames;
  int m_auto_calib_speed_thld;
  int m_no_face_total_frames;
  int m_face_orient_abnormal_frames;
  int m_face_orient_R_thld;
  int m_face_orient_P_thld;
  int m_face_orient_Y_thld;
  int m_auto_calib_config;
  face_calib_state m_face_calib_stat;
  face_calib_type m_face_calib_type;

  bool FaceCalibStart(hobot::RunContext *context);
  void FaceCalibUpdate(float &gain, Frame *frame, const EulerAngleF rpy);
  void FaceCalibDataReset();
  void FaceCalibProcessNoFaceFrame(Frame *frame);
  Quaternion FaceCalibRvec2Quat(Vector_3f rvec);
  Vector_3f FaceCalibQuat2Revc(Quaternion quat);
};

}  // end of namespace HobotDMS

#endif  //  SRC_MODULES_INPUT_LDMK_MODULE_H_
