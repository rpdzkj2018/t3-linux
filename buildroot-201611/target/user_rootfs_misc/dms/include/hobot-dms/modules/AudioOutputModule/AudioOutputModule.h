//
// Copyright 2016 Horizon Robotics.
//

#ifndef SRC_MODULES_OUTPUT_AUDIOOUTPUT_MODULE_H_
#define SRC_MODULES_OUTPUT_AUDIOOUTPUT_MODULE_H_

#include "hobot-dms/base/dms_module.h"

namespace HobotDMS {
namespace HAL {
class Audio;
}

class AudioOutputModule : public DMSModule {
 public:
  AudioOutputModule();
  virtual ~AudioOutputModule();

  int DMSModuleInit(hobot::RunContext *context) override;

  void Reset() override;
  void Fini();

  FORWARD_DECLARE(AudioOutputModule, 0);

 private:
  HAL::Audio *mp_audio_dev;
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_OUTPUT_AUDIOOUTPUT_MODULE_H_
