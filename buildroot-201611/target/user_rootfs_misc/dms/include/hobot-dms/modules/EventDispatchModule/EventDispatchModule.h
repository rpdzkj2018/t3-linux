//
// Copyright 2016 Horizon Robotics.
//

#ifndef SRC_MODULES_EVENTDISPATCHMODULE_H
#define SRC_MODULES_EVENTDISPATCHMODULE_H

#include <hobot/hobot.h>
#include <stdint.h>
#include <memory>
#include <vector>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_module.h"

namespace HobotNebula {
class EventConfig;
}

namespace HobotDMS {
class DMSProtoWriter;
class Frame;
class JpegHandle;
class HobotDMSContext;
class SDKOutMetaSerializer;
class EventDispatchModule : public DMSModule {
 public:
  enum EVENTDISPATCH_FORWARD { FORWARD_UPLOAD = 0, FORWARD_OUT = 1 };
  EventDispatchModule();
  virtual ~EventDispatchModule();
  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

#if (defined ENABLE_UPLOAD) && (defined ENABLE_STORAGE) && \
    (!defined ENABLE_UPLOAD_IN_PROTO)
  FORWARD_DECLARE(EventDispatchModule, FORWARD_UPLOAD);
#endif
  FORWARD_DECLARE(EventDispatchModule, FORWARD_OUT);

 private:
  std::unique_ptr<SDKOutMetaSerializer> mp_event_ser;
  std::vector<uint8_t> m_meta_buf;

#if (defined ENABLE_UPLOAD) && (defined ENABLE_STORAGE)
  int writeJpegFile(const std::string &fname_, void *data_, int size_);
  int writeProtoFile(const std::string &fname_, Frame &frame);
  std::vector<uint8_t> m_thumnail_buf;
  std::vector<char> m_jpg_buf;
  std::vector<char> m_thumb_jpg_buf;
  std::vector<DMSEventAttach> m_attaches;
  std::unique_ptr<DMSProtoWriter> mp_proto_writer;
  JpegHandle *mp_jpeghandle;
  std::unique_ptr<HobotNebula::EventConfig> event_config_;
#endif
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_EVENTDISPATCHMODULE_H
