#ifndef SRC_MODULES_JNIINPUT_MODULE_H_
#define SRC_MODULES_JNIINPUT_MODULE_H_

#include <mutex>

#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/message/frame.h"

namespace HobotDMS {
class Frame;

class JniInputModule : public DMSModule {
 public:
  JniInputModule();
  virtual ~JniInputModule();
  enum JNIINPUT_FORWARD { FORWARD_JNI = 0, FORWARD_UPLOAD = 1 };

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

  FORWARD_DECLARE(JniInputModule, FORWARD_JNI);
  FORWARD_DECLARE(JniInputModule, FORWARD_UPLOAD);

  bool do_cali_;
  std::deque<JniFrame> d_jni_holder_;
  std::mutex d_jni_holder_mtx_;
  inline void set_do_cali();
  inline void reset_do_cali();
};
}  // namespace HobotDMS

#endif /* ifndef SRC_MODULES_JNIINPUT_MODULE_H_ */
