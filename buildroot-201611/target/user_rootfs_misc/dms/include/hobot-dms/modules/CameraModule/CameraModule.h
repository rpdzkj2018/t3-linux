//
// Copyright 2017 Horizon Robotics.
//

#ifndef SRC_MODULES_CAMERA_MODULE_H_
#define SRC_MODULES_CAMERA_MODULE_H_

#include "hobot-dms/base/dms_input_module.h"
#include "hal/camera.h"
#include <chrono>

namespace HobotDMS {

class CameraModule : public DMSInputModule {
 public:
  CameraModule();
  ~CameraModule();

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

  FORWARD_DECLARE(CameraModule, 0);

  Frame *GetFrameImpl(hobot::spRunContext context) override;

 private:
  HAL::Camera *mp_camera_device;
  uint32_t m_img_id;
  uint32_t m_interval;
  std::chrono::steady_clock::time_point m_last_tp;
};

}  // end of namespace HobotDMS

#endif  //  SRC_MODULES_INPUT_CAMERA_MODULE_H_
