//
// Copyright 2017 Horizon Robotics.
//
#ifndef INCLUDE_HOBOT_DMS_MODULES_EYECLSF_EYECLSFMODULE_H_
#define INCLUDE_HOBOT_DMS_MODULES_EYECLSF_EYECLSFMODULE_H_

#include <memory>
#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/message/eyeclsf_frame.h"
#include "hobot-dms/base/message/ldmk_frame.h"

namespace hobot {
class MXNetEyeClsf;
}

namespace cv {
class Mat;
}
namespace dms_sdk_api {
class EyeDetect;
class EyeResult;
}

namespace HobotDMS {
class Frame;
class EyeInfo_Local;
class HobotDMSContext;

class EyeClsfModule : public DMSSDKModule {
 public:
  EyeClsfModule();
  ~EyeClsfModule();

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

  FORWARD_DECLARE(EyeClsfModule, 0);

 private:
  int preprocess(const cv::Mat &mGray, const Landmarks21_f &LDMK, eye_id id,
                 cv::Mat &rEyeMat, EyeInfo &rEyeInfo);

  int InitDetectHandle(hobot::RunContext *context);
  int DoProcess(Frame *frame, cv::Mat &img_mat,
                dms_sdk_api::PFaceAttrBLF &faceinfo);
  void PostProcess(Frame *);  // 后处理函数
  std::deque<std::shared_ptr<EyeInfo_Local>> m_post_data_eyeinfo;
  EyeInfo_Post m_post_local_data;
  int m_GradientTms;
  bool m_inited;

  dms_sdk_api::EyeDetect *mp_eye_detect;
};

}  // end of namespace HobotDMS

#endif  //  SRC_MODULES_INPUT_EYECLSF_MODULE_H_
