//
// Copyright 2017 Horizon Robotics.
//
#ifndef SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_FACEDETECT_FACEDETECTMODULE_H_
#define SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_FACEDETECT_FACEDETECTMODULE_H_
#include <memory>
#include <string>
#include <string>
#include <vector>
#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/message/frame.h"
#include "opencv2/opencv.hpp"
namespace dms_sdk_api {
class FaceDetect;
template <typename T>
class FaceBBox;
typedef FaceBBox<float> FaceBBoxF;
typedef FaceBBoxF FaceResult;
}
namespace AlphaFunc {
class AlphaDetAPI;
}

namespace HobotDMS {
class HobotDMSContext;
class FaceInfo_Local;
class FaceDetectModule : public DMSSDKModule {
 public:
  FaceDetectModule();
  ~FaceDetectModule();

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();
  FORWARD_DECLARE(FaceDetectModule, 0);
  // FORWARD_DECLARE_STACK_SIZE(FaceDetectModule, 0, 0x100000);

 private:
  int DoProcess(Frame *frame, cv::Mat &img_mat,
                dms_sdk_api::PFaceAttrBLF &faceinfo);
#ifdef ENABLE_TIME_CRASH
  long m_crash_time_s;
#endif
  void PostProcess(Frame *);  // 后处理函数
  int InitDetectHandle(hobot::RunContext *context);
  void choose_bestFace(std::vector<dms_sdk_api::FaceResult> &faceResults,
                       std::vector<FBox_f> *pFilteredFaceROIs);
  void refresh_faceROI(const std::vector<FBox_f> &filteredFaceROIs,
                       Rect *pDetectROI);
  bool IsNextDetectROIEmpty();
  int m_video_resolution_width;
  int m_video_resolution_height;
  FBox_<int> m_nextDetectROI;
  std::deque<std::shared_ptr<FaceInfo_Local>> m_post_data_ab_faceinfo;
  std::deque<std::shared_ptr<FaceInfo_Local>> m_post_data_faceinfo;
  FaceInfo_Post m_post_local_data;

  int m_GradientTms;
  dms_sdk_api::FaceDetect *mp_face_detect;
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_FACEDETECT_FACEDETECTMODULE_H_
