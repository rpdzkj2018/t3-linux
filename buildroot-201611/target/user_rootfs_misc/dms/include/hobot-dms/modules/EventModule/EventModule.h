//
// Copyright 2016 Horizon Robotics.
//

#ifndef SRC_MODULES_OUTPUTRENDERMODULE_H
#define SRC_MODULES_OUTPUTRENDERMODULE_H

#include <hobot/hobot.h>
#include <stdint.h>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/message/event_frame.h"
#include "hobot-dms/base/message/warnning_frame.h"

namespace HobotDMS {
class Frame;
class VehicleInfoFrame;
typedef enum cal_state_ {
  CAL_ST_INIT = 0,
  CAL_ST_ACCUMULATE = 1,
  CAL_ST_SUB_ACCUMULATE_1 = 2,
  CAL_ST_OUTPUT = 3,
  CAL_ST_SUB_ACCUMULATE_2 = 4,
  CAL_ST_SIZE = 5
} cal_state;

class OutEvent {
 public:
  OutEvent();
  virtual ~OutEvent();
  void Finit();
  virtual int Init(hobot::Config *) = 0;
  virtual EventResult StateMachineConversion(TimeStamp,
                                             const VehicleInfoFrame &,
                                             warn_state) = 0;
  TimeStamp m_main_clock;            // 主累加器
  TimeStamp m_sub1_clock;            // 子累加器
  TimeStamp m_out_clock;             // 附加累加器
  TimeStamp m_sub2_clock;            // 子累加器
  TimeStamp m_main_clock_base_time;  // 主累加器基准时间
  TimeStamp m_sub1_clock_base_time;  // 子累加器基准时间
  TimeStamp m_out_clock_base_time;   // 附加累加器基准时间
  TimeStamp m_sub2_clock_base_time;  // 子累加器基准时间
  cal_state m_cur_cal_state;         // 当前状态
  cal_state m_next_cal_state;        // 下一个状态
};

class DFWOutEvent : public OutEvent {
 public:
  DFWOutEvent();
  ~DFWOutEvent() = default;
  int Init(hobot::Config *);
  EventResult StateMachineConversion(TimeStamp, const VehicleInfoFrame &,
                                     warn_state);
  int UpdateTmsThld(int speed);

 private:
  int m_dfw_warn_border_speed;             // 速度边界
  int m_dfw_warn_speed_hysteresis;         // 滞回量
  TimeStamp m_dfw_warn_low_speed_ms_thld;  // 低于速度边界时, 时间阈值
  TimeStamp m_dfw_warn_high_speed_ms_thld;  // 高于速度边界时, 时间阈值
  TimeStamp m_normal_state_tolerance_tms;  // 容忍间隔
  TimeStamp m_time_alarm_interval;         // Event发生间隔
  bool m_warn_flag;
};
const int DDW_STATE_SIZE = 2;
class DDWOutEvent : public OutEvent {
 public:
  DDWOutEvent();
  ~DDWOutEvent() = default;
  int Init(hobot::Config *);
  EventResult StateMachineConversion(TimeStamp, const VehicleInfoFrame &,
                                     warn_state);
  int UpdateTmsThld(int speed);
  void SetFaceDir(face_dir f_dir);

 private:
  TimeStamp m_speed_border_1_ms_thld;  // 速度边界_1以下的时间阈值
  int m_speed_border_1;                // 速度边界_1
  TimeStamp m_speed_border_2_ms_thld;  // 速度边界_1与速度边界2之间的时间阈值
  int m_speed_border_2;                // 速度边界_2
  TimeStamp m_speed_border_3_ms_thld;  // 超过速度边界_3的速度阈值
  TimeStamp m_face_back_to_front_ms_thld;  // 脸部回正时间阈值
  TimeStamp m_normal_state_tolerance_tms;  // 容忍间隔
  TimeStamp m_time_alarm_interval;         // Event发生间隔
  int m_face_dir;
  event_state m_ddw_state[DDW_STATE_SIZE];
  TimeStamp m_last_evt_out_time;  // time of last event output
  inline event_state GetDDWStatus();
};

class DYAOutEvent : public OutEvent {
 public:
  DYAOutEvent();
  ~DYAOutEvent() = default;
  int Init(hobot::Config *);
  EventResult StateMachineConversion(TimeStamp, const VehicleInfoFrame &,
                                     warn_state);

 private:
  bool m_has_send;
  TimeStamp m_time_alarm_interval;
  TimeStamp m_next_send_border_time;
};

class DCAOutEvent : public OutEvent {
 public:
  DCAOutEvent();
  ~DCAOutEvent() = default;
  int Init(hobot::Config *);
  EventResult StateMachineConversion(TimeStamp, const VehicleInfoFrame &,
                                     warn_state);

 private:
  bool m_has_send;
  TimeStamp m_time_alarm_interval;
  TimeStamp m_next_send_border_time;
};

class DSAOutEvent : public OutEvent {
 public:
  DSAOutEvent();
  ~DSAOutEvent() = default;
  int Init(hobot::Config *);
  EventResult StateMachineConversion(TimeStamp, const VehicleInfoFrame &,
                                     warn_state);

 private:
  bool m_has_send;
  TimeStamp m_time_alarm_interval;
  TimeStamp m_next_send_border_time;
};

class DAAOutEvent : public OutEvent {
 public:
  DAAOutEvent();
  ~DAAOutEvent() = default;
  int Init(hobot::Config *);
  EventResult StateMachineConversion(TimeStamp, const VehicleInfoFrame &,
                                     warn_state);

 private:
  bool m_has_send;
  TimeStamp m_time_alarm_interval;
  TimeStamp m_next_send_border_time;
};

class CALIBOutEvent : public OutEvent {
 public:
  CALIBOutEvent();
  ~CALIBOutEvent() = default;
  int Init(hobot::Config *);
  EventResult StateMachineConversion(TimeStamp, const VehicleInfoFrame &,
                                     warn_state);
};


class EventModule : public DMSModule {
 public:
  EventModule();
  virtual ~EventModule();
  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

  FORWARD_DECLARE(EventModule, 0);

 private:
  void OutputEvent(Frame *p_frame);
  void WriteEventToFrame(const EventResult &, EventFrame &);

  DFWOutEvent dfw_out_event;
  DDWOutEvent ddw_out_event;
  DYAOutEvent dya_out_event;
  DCAOutEvent dca_out_event;
  DSAOutEvent dsa_out_event;
  DAAOutEvent daa_out_event;
  CALIBOutEvent calib_out_event;
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_OUTPUTRENDERMODULE_H
