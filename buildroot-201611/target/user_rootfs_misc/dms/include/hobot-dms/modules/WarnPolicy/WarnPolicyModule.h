//
// Copyright 2017 Horizon Robotics.
//

#ifndef SRC_MODULES_WARNPOLICY_MODULE_H_
#define SRC_MODULES_WARNPOLICY_MODULE_H_

#include <deque>
#include <memory>
#include <mutex>
#include <thread>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/dms_typedef.h"
#include "hobot-dms/base/message/face_frame.h"
#include "hobot-dms/base/message/vehicle_info_frame.h"
#include "hobot-dms/base/message/warnning_frame.h"
#include "hobot-dms/base/utils/serialize/serializer.h"

namespace HobotDMS {
struct DynParams {
  int LrAngOffset = 0;
  int UdAngOffset = 0;
  void clear() {
    LrAngOffset = 0;
    UdAngOffset = 0;
  }
};

class Frame;

class Policy {
 public:
  enum switch_state { OFF, ON };
  Policy();
  virtual ~Policy();

 public:
  virtual int Init(hobot::Config *conf) = 0;
  virtual void Judge(Frame *frame, int work_mode, int enable_vehinfo,
                     const VehicleInfoFrame &vehinfo);
  virtual void Finit() = 0;
  virtual void CheckEnableOrNot(int enable_vehinfo,
                                const VehicleInfoFrame &vehinfo) = 0;
  virtual void StateTransit(Frame *p_frame) = 0;
  switch_state m_cur_switch;  // 记录功能模块是否启用
  int m_is_enable;

  /*
* 0: 无条件关闭, 不关心任意外部条件, 诸如: 速度, 转向等. 该功能完全关闭.
* 1: 无条件开启, 不关心任意外部条件, 诸位: 速度, 转向等. 该功能完全开启,
** 适用于室内测试使用.(默认值)
* 2: 有条件停启, 关心外部条件, 诸如: 速度, 转向等. 功能的停启依赖外界条件,
** 满足阈值条件即启动, 不满足阈值即关闭, 适用于室内室外的实际场景测试.
*/
};


class DFWPolicy : public Policy {
 public:
  DFWPolicy();
  ~DFWPolicy();
  int Init(hobot::Config *conf);
  void Finit();
  void CheckEnableOrNot(int enable_vehinfo, const VehicleInfoFrame &);
  void StateTransit(Frame *p_frame);

 private:
  float m_left_eye_close_score_thld;
  float m_right_eye_close_score_thld;
  float m_upper_border_speed;
  float m_lower_border_speed;
  int m_turn_light_sensitive;
  int m_steering_sensitive;
  float m_steering_angle_thld;
  float m_max_yaw;
  float m_min_yaw;
  float m_max_pitch;
  float m_min_pitch;
};

class DDWPolicy : public Policy {
 public:
  DDWPolicy();
  ~DDWPolicy();
  int Init(hobot::Config *conf);
  void Finit();
  void CheckEnableOrNot(int enable_vehinfo, const VehicleInfoFrame &);
  void StateTransit(Frame *p_frame);

 private:
  DynParams m_dyn_params;
  void UpdateDDW(LDMKInfo_Post &ldmk_post, DDW_result &ddw);
  float m_upper_border_speed;
  float m_lower_border_speed;
  int m_turn_light_sensitive;
  int m_steering_sensitive;
  float m_steering_angle_thld;
  float m_left_ang_thld;
  float m_right_ang_thld;
  float m_up_ang_thld;
  float m_down_ang_thld;
};

class DYAPolicy : public Policy {
 public:
  DYAPolicy();
  ~DYAPolicy();
  int Init(hobot::Config *conf);
  void Finit();
  void CheckEnableOrNot(int enable_vehinfo, const VehicleInfoFrame &);
  void StateTransit(Frame *p_frame);

 private:
  float m_upper_border_speed;
  float m_lower_border_speed;
  float m_scl_mth_face_thld;
  int m_speed_sensitive;
  TimeStamp m_dya_start_time;
  TimeStamp m_dya_time_thld;
};

class DSAPolicy : public Policy {
 public:
  DSAPolicy();
  ~DSAPolicy();
  int Init(hobot::Config *conf);
  void Finit();
  void CheckEnableOrNot(int enable_vehinfo, const VehicleInfoFrame &);
  void StateTransit(Frame *p_frame);

 private:
  float m_upper_border_speed;
  float m_lower_border_speed;
  int m_speed_sensitive;
  float m_smoke_exist_score_thld;
  float m_pre_smoke_exist_score;
  float m_pre_smoke_noexist_score;
};

class DCAPolicy : public Policy {
 public:
  DCAPolicy();
  ~DCAPolicy();
  int Init(hobot::Config *conf);
  void Finit();
  void CheckEnableOrNot(int enable_vehinfo, const VehicleInfoFrame &);
  void StateTransit(Frame *p_frame);

 private:
  float m_upper_border_speed;
  float m_lower_border_speed;
  int m_speed_sensitive;
  float m_phone_exist_score_thld;
  float m_pre_phone_exist_score;
  float m_pre_phone_noexist_score;
};

class DAAPolicy : public Policy {
 public:
  DAAPolicy();
  ~DAAPolicy();
  int Init(hobot::Config *conf);
  void Finit();
  void CheckEnableOrNot(int enable_vehinfo, const VehicleInfoFrame &);
  void StateTransit(Frame *p_frame);

 private:
  float m_upper_border_speed;
  float m_lower_border_speed;
  int m_speed_sensitive;
  TimeStamp m_no_face_start_time;
  TimeStamp m_no_face_time_thld;
};

class CALIBPolicy : public Policy {
 public:
  CALIBPolicy();
  ~CALIBPolicy();
  int Init(hobot::Config *conf);
  void Finit();
  void CheckEnableOrNot(int enable_vehinfo, const VehicleInfoFrame &);
  void StateTransit(Frame *p_frame);

 private:
};


class WarnPolicyModule : public DMSModule {
 public:
  WarnPolicyModule();
  ~WarnPolicyModule();
  int DMSModuleInit(hobot::RunContext *context) override;
  int CheckVehInfo(const VehicleInfoFrame &, TimeStamp);
  void Fini();
  void Reset() override;
  FORWARD_DECLARE(WarnPolicyModule, 0);


 private:
  DFWPolicy m_dfw_policy;
  DDWPolicy m_ddw_policy;
  DYAPolicy m_dya_policy;
  DSAPolicy m_dsa_policy;
  DCAPolicy m_dca_policy;
  DAAPolicy m_daa_policy;
  CALIBPolicy m_calib_policy;

 private:
  int m_vehinfo_delay_tms_thld;
};

}  // end of namespace HobotDMS

#endif  //  SRC_MODULES_WARNPOLICY_MODULE_H_
