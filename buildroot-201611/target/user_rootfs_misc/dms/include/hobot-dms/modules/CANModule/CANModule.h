
//
// Copyright 2017 Horizon Robotics.
//

#ifndef SRC_MODULES_CAN_MODULE_H_
#define SRC_MODULES_CAN_MODULE_H_

#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/message/sensor_frame.h"
#include "hal/can.h"
#include <mutex>
#include <deque>
#include <memory>

namespace HobotDMS {

// class CANParser;
class HobotDMSContext;
class CANParser;

class CANModule : public DMSModule {
 public:
  CANModule();
  ~CANModule();

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  // void Init(){};
  void Fini();

  FORWARD_DECLARE(CANModule, 0);
  FORWARD_DECLARE(CANModule, 1);

 private:
  int GetCANInfo(HobotDMSContext *dms_context);
  HAL::CAN *mp_can_device;
  std::deque<CANInfo> m_can_holder;
  std::mutex m_can_holder_mtx;
  std::unique_ptr<CANParser> mp_can_parser;
};

}  // end of namespace HobotDMS

#endif  //  SRC_MODULES_INPUT_CAN_MODULE_H_
