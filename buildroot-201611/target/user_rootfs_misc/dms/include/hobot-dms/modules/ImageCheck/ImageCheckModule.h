//
// Copyright 2017 Horizon Robotics.
//

#ifndef SRC_MODULES_INPUT_IMAGE_CHECK_MODULE_H_
#define SRC_MODULES_INPUT_IMAGE_CHECK_MODULE_H_

#include "ImageQuality.h"
#include "hobot-dms/base/base.h"
#include "hobot-dms/base/dms_context.h"
#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/message/frame.h"

namespace HobotDMS {

enum CheckImageModuleState { RUN = 0, STOP = 1 };

class ImageCheckModule : public DMSModule {
 public:
  ImageCheckModule();
  ~ImageCheckModule();

  enum InputSlot {
    DATA_SLOT = 0,
    FEEDBACK_SLOT = 1
    //  INICONTROL_SLOT = 2
  };

  enum { DMS_ERROR_BLURRED = 1, DMS_ERROR_SHELTER = 2, DMS_ERROR_FREEZED = 3 };

  enum { DMS_NEXT_CHECK = 0, DMS_AUTO_START = 1 };
  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();


  FORWARD_DECLARE(ImageCheckModule, 0);

 private:
  bool m_wait(Frame *frame, int wait_type);
  void m_iq_run(Frame *frame);
  void m_clear();
  CImageQuality m_IQ;
  int m_height;
  int m_width;
  int m_check_cnt;
  int m_skip_cnt;
  int m_new_check_interval;
  int m_auto_start_interval;
  int m_roi_x;
  int m_roi_y;
  int m_roi_width;
  int m_roi_height;
  int m_blur_thresh;
  int m_shelter_thresh;
  int m_freeze_thresh;
  int m_check_state;
  int m_cnt;
  int m_blurred_cnt;
  int m_shelter_cnt;
  int m_freezed_cnt;
  int m_count_thresh;
  bool m_next_check;
};

}  // end of namespace HobotADAS

#endif  // SRC_MODULES_INPUT_IMAGE_CHECK_MODULE_H_
