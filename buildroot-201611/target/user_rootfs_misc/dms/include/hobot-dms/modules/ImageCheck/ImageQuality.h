//
// Copyright 2017 Horizon Robotics.
//
#ifndef SRC_MODULES_INPUT_IMAGEQUALITY_H_
#define SRC_MODULES_INPUT_IMAGEQUALITY_H_

#include <opencv2/opencv.hpp>
#include <vector>


class CImageQuality {
  typedef struct {
    int vote;
    float fAveLength;
  } BLUR_HIST;
  typedef struct {
    CvPoint pt;
    CvScalar ave;
    int flag;
  } BLOB_PIXEL;

  typedef struct {
    CvRect rect;
    std::vector<BLOB_PIXEL> vtPixel;
  } BLOB_REGION;

 public:
  struct IQresult {
    float BlurScore;
    float FreezeScore;
    float ShelterScore;
  };
  CImageQuality();
  ~CImageQuality();
  int initial(bool iscolor, cv::Rect2f roi);
  int reset();
  int run(const cv::Mat &img, IQresult &result);
  int IQBlur(float &score);
  int IQFreeze(float &score);
  int IQShelter(float &score);
  bool m_firstframe;
  bool m_IsColor;
  bool m_enable_blur;
  bool m_enable_freeze;
  bool m_enable_shelter;
  int m_ori_width;  // after resize
  int m_ori_height;
  int m_width;  // after roi
  int m_height;
  cv::Mat m_img_gray;
  cv::Mat m_img_rgb;
  cv::Mat m_img_hsv;
  cv::Mat m_last_img_gray;
  cv::Rect2f m_roi;

 private:
  std::vector<CvPoint> m_vtBlurEdgePixel;  // produce by blur,used by shelter
  bool CheckBlurEdge(int nGraySpan, int nDist);
  int m_nBlurEdgePixelUseful;  // for shelter
  void IMG_Ave(CvRect rect, CvScalar &ave);
  void IMG_Cluster(std::vector<BLOB_PIXEL> vtPixel,
                   std::vector<BLOB_REGION> &vtBlob, int nChannelDist);
};

#endif  // SRC_MODULES_INPUT_IMAGEQUALITY_H_
