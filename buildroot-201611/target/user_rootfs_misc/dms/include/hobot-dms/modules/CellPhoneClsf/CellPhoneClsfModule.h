//
// Copyright 2017 Horizon Robotics.
//
#ifndef SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_CELLPHONECLSF_CELLPHONECLSF_H_
#define SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_CELLPHONECLSF_CELLPHONECLSF_H_
#include <memory>
#include <string>
#include <vector>
#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/message/cellphone_frame.h"
#include "hobot-dms/base/message/frame.h"
#include "opencv2/opencv.hpp"

namespace hobot {
class MXNetCellPhoneClsf;
}
namespace dms_sdk_api {
class PhoneDetect;
}

namespace HobotDMS {
class Frame;
class PhoneInfo_Local;
class HobotDMSContext;
class CellPhoneClsfModule : public DMSSDKModule {
 public:
  CellPhoneClsfModule();
  ~CellPhoneClsfModule();
  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

  FORWARD_DECLARE(CellPhoneClsfModule, 0);

 private:
  int DoProcess(Frame *frame, cv::Mat &img_mat,
                dms_sdk_api::PFaceAttrBLF &faceinfo);
  void PostProcess(Frame *);  // 后处理函数
  int InitDetectHandle(hobot::RunContext *context);
  bool m_inited;
  int m_max_GradientTms;
  int m_min_GradientTms;
  std::deque<std::shared_ptr<PhoneInfo_Local>> m_post_data_phoneinfo;
  Cellphone_Post m_post_local_data;
  dms_sdk_api::PhoneDetect *m_phone_detect;
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_INCLUDE_HOBOT_DMS_MODULES_CELLPHONECLSF_CELLPHONECLSF_H_
