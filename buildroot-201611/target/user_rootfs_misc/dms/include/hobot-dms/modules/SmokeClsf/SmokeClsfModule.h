//
// Copyright 2017 Horizon Robotics.
//
#ifndef INCLUDE_HOBOT_DMS_MODULES_SMOKECLSF_SMOKECLSFMODULE_H_
#define INCLUDE_HOBOT_DMS_MODULES_SMOKECLSF_SMOKECLSFMODULE_H_

#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/message/face_frame.h"
#include "hobot-dms/base/message/smokeclsf_frame.h"

#include <memory>

namespace hobot {
class MXNetSmokeClsf;
}
namespace dms_sdk_api {
class SmokeDetect;
class SmokeResult;
}
namespace cv {
class Mat;
}

namespace HobotDMS {
class Frame;
class SmokeInfo_Local;

class SmokeClsfModule : public DMSSDKModule {
 public:
  SmokeClsfModule();
  ~SmokeClsfModule();

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

  FORWARD_DECLARE(SmokeClsfModule, 0);

 private:
  int DoProcess(Frame *frame, cv::Mat &img_mat,
                dms_sdk_api::PFaceAttrBLF &faceinfo);
  void PostProcess(Frame *frame_);  // 后处理函数
  int InitDetectHandle(hobot::RunContext *context);
  std::deque<std::shared_ptr<SmokeInfo_Local>> m_post_data_smokeinfo;
  Smoke_Post m_post_local_data;
  int m_max_GradientTms;
  int m_min_GradientTms;
  bool m_inited;
  dms_sdk_api::SmokeDetect *m_smoke_detect;
};

}  // end of namespace HobotDMS

#endif  //  SRC_MODULES_INPUT_SMOKECLSF_MODULE_H_
