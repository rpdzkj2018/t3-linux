//
// Copyright 2017 Horizon Robotics.
//

#ifndef SRC_MODULES_INPUTCOLLECT_MODULE_H_
#define SRC_MODULES_INPUTCOLLECT_MODULE_H_

#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/dms_defines.h"
#include "hal/can_info.h"
#include "hal/ins_info.h"
#include <deque>
#include <mutex>
#include <memory>

namespace HobotDMS {

class CANParser;
class INSParser;

class InputCollectModule : public DMSModule {
 public:
  enum COLLECT_FORWARD { FORWARD_IMG = 0, FORWARD_CAN = 1, FORWARD_INS = 2 };
  InputCollectModule();
  virtual ~InputCollectModule();

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

  FORWARD_DECLARE(InputCollectModule, FORWARD_IMG);
  FORWARD_DECLARE(InputCollectModule, FORWARD_CAN);
  FORWARD_DECLARE(InputCollectModule, FORWARD_INS);

 private:
  std::deque<CANInfo> m_can_holder;
  std::mutex m_can_holder_mtx;
  std::deque<INSInfo> m_ins_holder;
  std::mutex m_ins_holder_mtx;
  std::unique_ptr<CANParser> mp_can_parser;
  std::unique_ptr<INSParser> mp_ins_parser;
};

}  // end of namespace HobotDMS

#endif  //  SRC_MODULES_INPUT_INPUTCOLLECT_MODULE_H_
