//
// Copyright 2017 Horizon Robotics.
//

#ifndef SRC_MODULES_METAINPUT_MODULE_H_
#define SRC_MODULES_METAINPUT_MODULE_H_

#include "hobot-dms/base/dms_module.h"
#include "hobot-dms/base/utils/serialize/serializer.h"
#include "hobot-dms/base/message/video_position.h"
#include <mutex>
#include <condition_variable>

namespace HobotDMS {
// class MetaDeserializer;
class DMSProtoReader;
class Frame;

class MetaInputModule : public DMSModule {
 public:
  enum OUTPUT_FORWARD { FORWARD_FEEDBACK, FORWARD_OUT, FORWARD_EOF };
  MetaInputModule();
  ~MetaInputModule();

  int DMSModuleInit(hobot::RunContext *context) override;
  void Reset() override;
  void Fini();

  // FORWARD_DECLARE_STACK_SIZE(MetaInputModule, 0, 0x100000);
  FORWARD_DECLARE(MetaInputModule, FORWARD_FEEDBACK);
  FORWARD_DECLARE(MetaInputModule, FORWARD_OUT);

 private:
  // Read the binary, calculate the proto's pos and size.
  DMSProtoReader *m_proto_file_reader;
  VideoPosition m_pos;
  // bool m_pos_flag;
  std::mutex m_pos_mtx;
  TimeStamp m_next_ts;
  // std::condition_variable m_pos_cv;
  Frame *mp_cached_frame;
  int m_frame_intvl;
};

}  // end of namespace HobotDMS
#endif  // SRC_MODULES_METAINPUT_MODULE_H_
