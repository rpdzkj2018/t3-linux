//
// Copyright 2017 Horizon Robotics.
//

#ifndef SRC_MODULES_USERCTRL_MODULE_H_
#define SRC_MODULES_USERCTRL_MODULE_H_

#include "hobot-dms/base/dms_input_module.h"

#include <future>

class UserControl;

namespace HobotDMS {
class UserControlModule : public DMSModule {
 public:
  UserControlModule();

  ~UserControlModule();

  int DMSModuleInit(hobot::RunContext *context) override;

  void Reset() override;

  void Fini();

  void ProgressSchedule(void);

  void UsrCtrlThread(void);
  void UsrCtrlMsgThread(void);

  FORWARD_DECLARE(UserControlModule, 0);

  FORWARD_DECLARE(UserControlModule, 1);

  //  Frame *GetFrameImpl(hobot::spRunContext context) override;

 private:
  UserControl *mpUsrCtrl;
  int64_t mVideoPos = 0;
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_USERCTRL_MODULE_H_
