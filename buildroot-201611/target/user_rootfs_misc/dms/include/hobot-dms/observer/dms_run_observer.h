//
// Copyright 2016 Horizon Robotics.
//

#ifndef SRC_OBSERVER_DMS_RUN_OBSERVER_H_
#define SRC_OBSERVER_DMS_RUN_OBSERVER_H_

#include <list>
#include <hobot/hobot.h>

namespace HobotDMS {

class HobotDMSContext;
class DMSRunObserver : public hobot::RunObserver {
 public:
  explicit DMSRunObserver(HobotDMSContext *dms_context);
  ~DMSRunObserver();

  void OnResult(hobot::Module *from, int forward_index,
                hobot::spMessage output) override;

  void OnError(hobot::Module *from, int forward_index,
               hobot::spMessage err) override;

 private:
  HobotDMSContext *dms_context_;
};

}  // end of namespace HobotDMS

#endif  // SRC_OBSERVER_DMS_RUN_OBSERVER_H_
