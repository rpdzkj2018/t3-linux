//
// Copyright 2016 Horizon Robotics.
//

#ifndef SRC_MODULES_INPUT_INPUT_MODULE_H_
#define SRC_MODULES_INPUT_INPUT_MODULE_H_

#include <string>
#include "hobot-dms/base/dms_module.h"

namespace HobotDMS {

class Frame;

class DMSInputModule : public DMSModule {
 public:
  explicit DMSInputModule(std::string class_name) : DMSModule(class_name) {
    m_frame_id = 0;
  }

  virtual ~DMSInputModule() {}

 protected:
  virtual Frame *GetFrameImpl(hobot::spRunContext context) = 0;
  uint32_t m_frame_id;
};

}  // end of namespace HobotDMS

#endif  // SRC_MODULES_INPUT_INPUT_MODULE_H_
