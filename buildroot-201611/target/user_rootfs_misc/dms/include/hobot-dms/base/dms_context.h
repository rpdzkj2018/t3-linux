//
// Copyright 2016 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_CONTEXT_H_
#define HOBOT_DMS_DATA_STRUCTURE_CONTEXT_H_

#include <hobot/hobot.h>

#include <condition_variable>
#include <list>
#include <map>
#include <mutex>
#include <string>
#include <vector>

#include "hobot-dms/base/base.h"

#ifdef ENABLE_STORAGE
namespace HobotNebula_StorageManager {
class StorageManager;
}
#endif

namespace HobotDMS {
class HobotDMS;
class FrameHolder;
class OutputFrame;
class JpegHandle;

class DMSModuleContext : public hobot::Message {
 public:
  std::string config_dir_;
  hobot::spConfig config_;
};

class HobotDMSContext : public DMSModuleContext {
 public:
  enum HobotDMSMode {
    HobotDMSMode_InputMode = 0,
    HobotDMSMode_PlaybackLevel = 1,
    HobotDMSMode_ColorMode = 2,
    HobotDMSMode_OutputMode = 3,
    HobotDMSMode_DrawMode = 4,
    HobotDMSMode_EngineMode = 5,
    HobotDMSMode_Count = 6
  };

  enum HobotDMSEnable {
    HobotDMSEnable_ModuleFaceDetect = 0,
    HobotDMSEnable_ModuleLDMK = 1,
    HobotDMSEnable_ModuleEyeClsf = 2,
    HobotDMSEnable_ModuleWarnPolicy = 3,
    HobotDMSEnable_ModuleRecorder = 4,
    HobotDMSEnable_ModuleDisplay = 5,
    HobotDMSEnable_ModuleAudioOut = 6,
    HobotDMSEnable_ModuleDraw = 7,
    HobotDMSEnable_ModuleEventDispatch = 8,
    HobotDMSEnable_ModuleCellphoneClsf = 9,
    HobotDMSEnable_ModuleSmokeClsf = 10,
    HobotDMSEnable_Count = 11

  };

  enum EngineMode { ENGINE_MODE_NORMAL = 0, ENGINE_MODE_CALIB = 1 };

  enum PlaybackLevel {
    PLAYBACK_LEVEL_NONE = 0,
    PLAYBACK_LEVEL_RAW = 1,
  };

  enum OutputMode {
    OUTPUT_MODE_NONE = 0,
    OUTPUT_MODE_CALLBACK = 1,
    OUTPUT_MODE_API = 2
  };

  enum InputMode {
    INPUT_MODE_FILE = 0,
    INPUT_MODE_HARDWARE = 1,
    INPUT_MODE_API_ALL = 2,
    INPUT_MODE_API_CAN = 3
  };

  explicit HobotDMSContext();
  ~HobotDMSContext();

  int Init(const hobot::spMessage msg, const DMSUVCParams *uvc_params);
  void Fini();

  std::vector<int> mode_;
  std::vector<uint8_t> enable_;

  int frame_width_;
  int frame_height_;
  int64_t frame_count_;
  std::string android_package_name;
  int m_frame_fps_tims_ms = 0;
  int m_enable_calib = 0;
  int m_calib_frames = 0;
  std::string calib_file;

  FrameHolder *frame_holder_;

  typedef std::vector<DMSCallBack> callbacks_t;
  void SetCallback(uint32_t type, DMSCallBack callback);
  int CallCallbacks(uint32_t type, void *data);

  // callback mode output
  std::mutex callback_mutex_;
  std::map<uint32_t, callbacks_t> dms_callbacks_;

  DMSUVCParams uvc_params_;

  // api mode output
  std::mutex output_mutex_;
  std::condition_variable output_cond_;
  std::list<OutputFrame *> output_;

  // inited module list
  std::vector<DMSModuleStatus> modules_inited_;

  // module map
  std::map<std::string, hobot::Module *> modules_map_;
  void InitModuleMap(const std::vector<hobot::Module *> &modules);
  void ReleaseModuleMap();

  // storage manager

#ifdef ENABLE_STORAGE
  std::unique_ptr<HobotNebula_StorageManager::StorageManager>
      mp_storage_manager;
#endif
  std::unique_ptr<JpegHandle> mp_jpeghandle;


 private:
  void bindCore();
};

}  // end of namespace HobotDMS

#endif  // HOBOT_DMS_DATA_STRUCTURE_CONTEXT_H_
