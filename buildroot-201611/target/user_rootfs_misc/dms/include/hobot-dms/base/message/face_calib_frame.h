//
// Copyright 2018 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_FACE_CALIB_FRAME_H
#define HOBOT_DMS_DATA_STRUCTURE_FACE_CALIB_FRAME_H


#include <string>
#include <vector>

#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_typedef.h"


namespace HobotDMS {
typedef enum face_calib_result_ {
  FACE_CALIB_NONE = 0,
  FACE_CALIB_START = 1,
  FACE_CALIB_FINISHED = 2,
  FACE_CALIB_NOFACE = 3,
  FACE_CALIB_ABNORMALFACE = 4,
  FACE_CALIB_FAILED = 5,
  FACE_CALIB_SIZE = 6
} face_calib_result;

/**
 * @brief: FaceCalibFrame
 */
class FaceCalibFrame {
 public:
  FaceCalibFrame() : m_face_calib_result(FACE_CALIB_NONE){};
  ~FaceCalibFrame() = default;
  // FaceCalibFrame(const FaceCalibFrame &) = delete;
  // FaceCalibFrame &operator =(const FaceCalibFrame &) = delete;

  face_calib_result m_face_calib_result;
  Vector_3f m_rvec;
  void ClearData() {
    m_face_calib_result = FACE_CALIB_NONE;
    m_rvec.Clear();
  }
};
}  // namespace HobotDMS


#endif /* ifndef HOBOT_DMS_DATA_STRUCTURE_FACE_CALIB_FRAME_H */
