//
// Copyright 2016 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_VIDEOPOSITION_H_
#define HOBOT_DMS_DATA_STRUCTURE_VIDEOPOSITION_H_

#include <hobot/hobot.h>
#include <stdint.h>
#include "hobot-dms/base/permission.h"

namespace HobotDMS {

class VideoPosition {
 public:
  VideoPosition(int64_t flushed = 0, int64_t next = 0)
      : m_flushed_pos(flushed),
        m_next_pos(next),
        m_is_eof(false),
        m_is_userseek(false) {}
  int64_t m_flushed_pos;
  int64_t m_next_pos;
  bool m_is_eof;
  bool m_is_userseek;
  bool m_is_save;
};

}  // end of namespace HobotDMS

#endif  // HOBOT_DMS_DATA_STRUCTURE_FRAME_H_
