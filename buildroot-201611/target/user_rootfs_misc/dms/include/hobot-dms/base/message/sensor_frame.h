//
// Copyright 2016 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_SENSOR_INPUT_H_
#define HOBOT_DMS_DATA_STRUCTURE_SENSOR_INPUT_H_

#include <string.h>
#include <mutex>

#include "hal/can_info.h"
#include "hal/ins_info.h"
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/utils/buffer_t/buffer_t.h"

namespace HobotDMS {

template <typename T>
class SensorFrame {
 public:
  SensorFrame(size_t max_size = 100) : m_frame_raw(max_size) {}
  ~SensorFrame() {}
  SensorFrame(const SensorFrame &) = delete;
  SensorFrame &operator=(const SensorFrame &) = delete;
  int push(const T *p_frame) {
    std::unique_lock<std::mutex> lck(m_frame_mtx);
    m_frame_raw.push(p_frame);
    return 0;
  }
  int seek(TimeStamp time, T &info) const {
    std::unique_lock<std::mutex> lck(m_frame_mtx);
    T *i = m_frame_raw.seekbytime(time);
    if (!i)
      return -1;
    else {
      info = *i;
      return 0;
    }
  }
  // T *next(T *curr) const;
  int curr(T &info) const {
    std::unique_lock<std::mutex> lck(m_frame_mtx);
    T *i = m_frame_raw.curPos();
    if (!i)
      return -1;
    else {
      info = *i;
      return 0;
    }
  }

 private:
  mutable std::mutex m_frame_mtx;
  // std::vector<CANFrameRaw> m_can_frame_raw;
  mutable BufferT<T> m_frame_raw;
};
typedef SensorFrame<CANInfo> SensorCANFrame;
typedef SensorFrame<INSInfo> SensorINSFrame;

}  // end of namespace HobotDMS

#endif  // HOBOT_DMS_DATA_STRUCTURE_SENSOR_INPUT_H_
