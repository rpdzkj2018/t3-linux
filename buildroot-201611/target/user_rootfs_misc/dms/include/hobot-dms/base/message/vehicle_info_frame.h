//
// Copyright 2018 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_VEHICLE_INFO_H_
#define HOBOT_DMS_DATA_STRUCTURE_VEHICLE_INFO_H_

#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_typedef.h"

namespace HobotDMS {
typedef enum turn_light_st_ {
  LIGHT_ST_INVALID,
  LIGHT_ST_OFF,
  LIGHT_ST_LEFT,
  LIGHT_ST_RIGHT,
  LIGHT_ST_LEFT_RIGHT,
  LIGHT_ST_SIZE
} turn_light_st;

typedef enum gears_st_ {
  GEARS_ST_INVALID,
  GEARS_ST_NP,
  GEARS_ST_F,
  GEARS_ST_R,
  GEARS_ST_SIZE,
} gears_st;

class VehicleInfoFrame {
 public:
  VehicleInfoFrame()
      : wheel_angle_(0),
        turn_light_(LIGHT_ST_INVALID),
        gears_(GEARS_ST_INVALID),
        veh_speed_(-1),
        timestamp_(-1) {}
  ~VehicleInfoFrame() = default;
  VehicleInfoFrame(const VehicleInfoFrame &) = delete;
  VehicleInfoFrame &operator=(const VehicleInfoFrame &) = delete;
  void ClearData() {
    wheel_angle_ = 0;
    turn_light_ = LIGHT_ST_INVALID;
    gears_ = GEARS_ST_INVALID;
    veh_speed_ = -1;
    timestamp_ = -1;
  }
  turn_light_st turn_light_;
  gears_st gears_;
  int wheel_angle_;
  int veh_speed_;
  TimeStamp timestamp_;
};
}

#endif
