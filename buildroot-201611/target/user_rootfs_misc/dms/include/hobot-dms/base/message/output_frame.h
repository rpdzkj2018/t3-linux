//
// Copyright 2016 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_OUTPUT_H_
#define HOBOT_DMS_DATA_STRUCTURE_OUTPUT_H_

#include <stdint.h>
#include <string>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/message/image_frame.h"

namespace HobotDMS {

class OutputFrame {
 public:
  OutputFrame();
  ~OutputFrame();
  void ClearData();

  ImageFrame m_img_frame;
  DMS_WARNSTATE m_st;
};

}  // end of namespace HobotDMS

#endif  // HOBOT_DMS_DATA_STRUCTURE_OUTPUT_H_
