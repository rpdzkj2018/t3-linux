// Copyright 2018 Hobot Company

// define faceframe
#ifndef SRC_BASE_INCLUDE_HOBOT_DMS_BASE_MESSAGE_WARNNING_FRAME_H_
#define SRC_BASE_INCLUDE_HOBOT_DMS_BASE_MESSAGE_WARNNING_FRAME_H_


#include <stdint.h>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_typedef.h"

namespace HobotDMS {
typedef enum warn_state_ {
  WARN_ST_STAY_CUR = 0,
  WARN_ST_NORMAL = 1,
  WARN_ST_LV1 = 2,
  WARN_ST_LV2 = 3,
  WARN_ST_SIZE = 4
} warn_state;

typedef enum face_dir_ {
  FACE_DIR_NONE = 0x00,
  FACE_DIR_FRONT = 0x01,
  FACE_DIR_LEFT = 0x02,
  FACE_DIR_RIGHT = 0x04,
  FACE_DIR_UP = 0x08,
  FACE_DIR_DOWN = 0x10,
} face_dir;


// params
class DFW_policy_params {
 public:
  DFW_policy_params() {}
  ~DFW_policy_params() = default;
  void clear() {}

 public:
};

class DDW_policy_params {
 public:
  DDW_policy_params()
      : lr_ang(0),
        ud_ang(0),
        lr_ang_offset(0),
        ud_ang_offset(0),
        u_ang_thld(30),
        d_ang_thld(-30),
        l_ang_thld(-45),
        r_ang_thld(45),
        f_dir(face_dir::FACE_DIR_FRONT) {}
  ~DDW_policy_params() = default;
  void clear() {
    lr_ang = 0;
    ud_ang = 0;
    lr_ang_offset = 0;
    ud_ang_offset = 0;
    u_ang_thld = 30;
    d_ang_thld = -30;
    l_ang_thld = -45;
    r_ang_thld = 45;
    f_dir = face_dir::FACE_DIR_FRONT;
  }

 public:
  int lr_ang;
  int ud_ang;
  int lr_ang_offset;
  int ud_ang_offset;
  int u_ang_thld;
  int d_ang_thld;
  int l_ang_thld;
  int r_ang_thld;
  face_dir f_dir;
};

class DYA_policy_params {
 public:
  DYA_policy_params() : scl_mthH_2_faceH_thld(0.2) {}
  ~DYA_policy_params() = default;
  void clear() { scl_mthH_2_faceH_thld = 0.2; }

 public:
  float scl_mthH_2_faceH_thld;
};

class DCA_policy_params {
 public:
  DCA_policy_params() {}
  ~DCA_policy_params() = default;
  void clear() {}
};

class DSA_policy_params {
 public:
  DSA_policy_params() {}
  ~DSA_policy_params() = default;
  void clear() {}
};

class DAA_policy_params {
 public:
  DAA_policy_params() {}
  ~DAA_policy_params() = default;
  void clear() {}
};

// result
class DFW_result {
 public:
  DFW_result() : state(WARN_ST_NORMAL), params() {}
  ~DFW_result() = default;
  void clear() {
    state = WARN_ST_NORMAL;
    params.clear();
  }

 public:
  warn_state state;
  DFW_policy_params params;
};

class DDW_result {
 public:
  DDW_result() : state(WARN_ST_NORMAL), params() {}
  ~DDW_result() = default;
  void clear() {
    state = WARN_ST_NORMAL;
    params.clear();
  }

  warn_state state;
  DDW_policy_params params;
};

class DYA_result {
 public:
  DYA_result() : state(WARN_ST_NORMAL), params() {}
  ~DYA_result() = default;
  void clear() {
    state = WARN_ST_NORMAL;
    params.clear();
  }

 public:
  warn_state state;
  DYA_policy_params params;
};

class DCA_result {
 public:
  DCA_result() : state(WARN_ST_NORMAL), params() {}
  ~DCA_result() = default;
  void clear() {
    state = WARN_ST_NORMAL;
    params.clear();
  }

 public:
  warn_state state;
  DCA_policy_params params;
};

class DSA_result {
 public:
  DSA_result() : state(WARN_ST_NORMAL), params() {}
  ~DSA_result() = default;
  void clear() {
    state = WARN_ST_NORMAL;
    params.clear();
  }

 public:
  warn_state state;
  DSA_policy_params params;
};

class DAA_result {
 public:
  DAA_result() : state(WARN_ST_NORMAL), params() {}
  ~DAA_result() = default;
  void clear() {
    state = WARN_ST_NORMAL;
    params.clear();
  }

 public:
  warn_state state;
  DAA_policy_params params;
};

class CALIB_result {
 public:
  CALIB_result() : state() {}
  ~CALIB_result() = default;
  void clear() { state = WARN_ST_NORMAL; }

 public:
  warn_state state;
};

class WarningFrame {
 public:
  WarningFrame()
      : m_DFW(), m_DDW(), m_DYA(), m_DCA(), m_DSA(), m_DAA(), m_CALIB() {}
  // m_policy_params()
  ~WarningFrame() = default;
  WarningFrame(const WarningFrame &) = delete;
  WarningFrame &operator=(const WarningFrame &) = delete;
  void ClearData() {
    m_DFW.clear();
    m_DDW.clear();
    m_DYA.clear();
    m_DCA.clear();
    m_DSA.clear();
    m_DAA.clear();
    m_CALIB.clear();
  }

 public:
  DFW_result m_DFW;
  DDW_result m_DDW;
  DYA_result m_DYA;
  DCA_result m_DCA;
  DSA_result m_DSA;
  DAA_result m_DAA;
  CALIB_result m_CALIB;
};

}  // namespace HobotDMS

#endif  // SRC_BASE_INCLUDE_HOBOT_DMS_BASE_MESSAGE_WARNNING_FRAME_H_
