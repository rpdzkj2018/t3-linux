// Copyright [2018] <Haoming.Ju>

#ifndef SRC_BASE_INCLUDE_HOBOT_DMS_BASE_MESSAGE_FACE_DIR_FRAME_H_
#define SRC_BASE_INCLUDE_HOBOT_DMS_BASE_MESSAGE_FACE_DIR_FRAME_H_


namespace HobotDMS {
enum class FaceDirEnum {
  FACE_DIR_NONE = 0x00,
  FACE_DIR_FRONT = 0x01,
  FACE_DIR_LEFT = 0x02,
  FACE_DIR_RIGHT = 0x04,
  FACE_DIR_UP = 0x08,
  FACE_DIR_DOWN = 0x10,
};
class FaceDirFrame {
 public:  // public function
  FaceDirFrame()
      : m_face_direction(FaceDirEnum::FACE_DIR_NONE),
        m_angle_roll(-1.0f),
        m_angle_pitch(-1.0f),
        m_angle_yaw(-1.0f),
        m_unit_vec_x(-1.0f),
        m_unit_vec_y(-1.0f),
        m_unit_vec_z(-1.0f) {}
  ~FaceDirFrame() {}
  void ClearData() {
    m_face_direction = FaceDirEnum::FACE_DIR_NONE;
    m_angle_roll = -1.0f;
    m_angle_pitch = -1.0f;
    m_angle_yaw = -1.0f;
    m_unit_vec_x = -1.0f;
    m_unit_vec_y = -1.0f;
    m_unit_vec_z = -1.0f;
  }

 public:  // public variable
  FaceDirEnum m_face_direction;
  float m_angle_roll;
  float m_angle_pitch;
  float m_angle_yaw;
  float m_unit_vec_x;
  float m_unit_vec_y;
  float m_unit_vec_z;
};
}  // namespace HobotDMS
#endif  // SRC_BASE_INCLUDE_HOBOT_DMS_BASE_MESSAGE_FACE_DIR_FRAME_H_
