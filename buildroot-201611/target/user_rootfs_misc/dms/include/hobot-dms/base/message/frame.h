//
// Copyright 2016 Horizon Robotics.
//

#ifndef SRC_BASE_INCLUDE_HOBOT_DMS_BASE_MESSAGE_FRAME_H_
#define SRC_BASE_INCLUDE_HOBOT_DMS_BASE_MESSAGE_FRAME_H_


#include <list>
#include <mutex>
#include <utility>
#include <vector>

#include <hobot/hobot.h>
#include <stdint.h>
#include "hobot-dms/base/message/cellphone_frame.h"
#include "hobot-dms/base/message/event_frame.h"
#include "hobot-dms/base/message/eyeclsf_frame.h"
#include "hobot-dms/base/message/face_calib_frame.h"
#include "hobot-dms/base/message/face_dir_frame.h"
#include "hobot-dms/base/message/face_frame.h"
#include "hobot-dms/base/message/image_check_frame.h"
#include "hobot-dms/base/message/image_frame.h"
#include "hobot-dms/base/message/jniFrame.h"
#include "hobot-dms/base/message/ldmk_frame.h"
#include "hobot-dms/base/message/sensor_frame.h"
#include "hobot-dms/base/message/smokeclsf_frame.h"
#include "hobot-dms/base/message/vehicle_info_frame.h"
#include "hobot-dms/base/message/warnning_frame.h"
#include "hobot-dms/base/permission.h"

namespace HobotDMS {
class Frame {
 public:
  Frame();
  Frame(int width, int height, int color_mode);

  ~Frame();

  // bool DeepCopy(const Frame *frame);
  void ClearData();

  ImageFrame m_img_frame;
  int32_t m_frame_id;
  bool m_is_userseek;
  bool m_is_eof;
  bool m_is_save;

  // singleton sensor frames
  SensorCANFrame *mp_caninfo_frame;
  SensorINSFrame *mp_insinfo_frame;
  FaceFrame m_face_frame;
  FaceDirFrame m_face_dir_frame;  // TODO(Haoming): 未打通数据流
  LDMKFrame m_ldmk_frame;
  EyeClsfFrame m_eyeclsf_frame;
  WarningFrame m_warning_frame;
  CellphoneFrame m_cellphone_frame;
  SmokeClsfFrame m_smokeclsf_frame;
  EventFrame m_event_frame;
  VehicleInfoFrame m_vehinfo_frame;
  ImageCheckFrame m_imgcheck_frame;
  JniFrame m_jni_frame;
  FaceCalibFrame m_facecalib_frame;
  // std::vector<hobot::spMessage> attached_data_;
};

}  // end of namespace HobotDMS

#endif  // SRC_BASE_INCLUDE_HOBOT_DMS_BASE_MESSAGE_FRAME_H_
