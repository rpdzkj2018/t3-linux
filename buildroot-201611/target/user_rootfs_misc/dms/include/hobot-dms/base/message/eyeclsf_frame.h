#ifndef HOBOT_DMS_DATA_STRUCTURE_EYECLSF_FRAME_H_
#define HOBOT_DMS_DATA_STRUCTURE_EYECLSF_FRAME_H_
#include <array>
#include <vector>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_typedef.h"

// define eyeclsfframe

namespace HobotDMS {
typedef enum eye_st_ {
  EYE_ST_OPEN,
  EYE_ST_CLOSE,
  EYE_ST_UNKNOW,
  EYE_ST_SIZE
} eye_st;

template <typename Dtype>
class EyeInfo_ {
 public:
  EyeInfo_() { clear(); }
  EyeInfo_(const Rect_<Dtype> &roi, float open_score = -1,
           float close_score = -1, eye_st st = EYE_ST_UNKNOW,
           float close_thld = -1) {
    this->m_roi = roi;
    this->m_open_score = open_score;
    this->m_close_score = close_score;
    if (close_thld < 0)
      this->m_status = st;
    else
      this->m_status = m_close_score > close_thld ? EYE_ST_CLOSE : EYE_ST_OPEN;
  }
  ~EyeInfo_() = default;
  EyeInfo_(const EyeInfo_<Dtype> &r_eye_status) {
    this->m_status = r_eye_status.m_status;
    this->m_roi = r_eye_status.m_roi;
    this->m_open_score = r_eye_status.m_open_score;
    this->m_close_score = r_eye_status.m_close_score;
  }
  EyeInfo_ &operator=(const EyeInfo_<Dtype> &r_eye_status) {
    this->m_status = r_eye_status.m_status;
    this->m_roi = r_eye_status.m_roi;
    this->m_open_score = r_eye_status.m_open_score;
    this->m_close_score = r_eye_status.m_close_score;
    return *this;
  }
  void clear() {
    m_status = EYE_ST_UNKNOW;
    m_open_score = -1;
    m_close_score = -1;
    m_roi.Clear();
  }
  const Rect_<Dtype> &roi() const { return m_roi; }
  Rect_<Dtype> &mutable_roi() { return m_roi; }
  float open_score() const { return m_open_score; }
  float close_score() const { return m_close_score; }
  eye_st status() const { return m_status; }
  void set_score(float open_score, float close_score, eye_st st = EYE_ST_UNKNOW,
                 float close_thld = -1) {
    this->m_open_score = open_score;
    this->m_close_score = close_score;
    if (close_thld < 0)
      this->m_status = st;
    else
      this->m_status = m_close_score > close_thld ? EYE_ST_CLOSE : EYE_ST_OPEN;
  }

 private:
  eye_st m_status;
  Rect_<Dtype> m_roi;
  float m_open_score;
  float m_close_score;
};
enum eye_id { EYE_ID_RIGHT = 0, EYE_ID_LEFT, EYE_ID_SIZE };
typedef EyeInfo_<int> EyeInfo;
typedef std::array<EyeInfo, EYE_ID_SIZE> DEyeInfo;


class EyeInfo_Post {
 public:
  EyeInfo_Post()
      : mGradientL(0.0),
        mGradientR(0.0),
        mSumReyeCloseConf(0.0),
        mSumLeyeCloseConf(0.0),
        mAvrReyeCloseConf(0.0),
        mAvrLeyeCloseConf(0.0) {}
  ~EyeInfo_Post() = default;
  void clear() {
    mGradientL = 0.0;
    mGradientR = 0.0;
    mSumReyeCloseConf = 0.0;
    mSumLeyeCloseConf = 0.0;
    mAvrReyeCloseConf = 0.0;
    mAvrLeyeCloseConf = 0.0;
  }

 public:
  float mGradientL;         // 左眼close分数梯度
  float mGradientR;         // 右眼closs分数梯度
  float mSumReyeCloseConf;  // 右眼close分数总和
  float mSumLeyeCloseConf;  // 左眼close分数总和
  float mAvrReyeCloseConf;  // 右眼close分数均值
  float mAvrLeyeCloseConf;  // 左眼close分数均值
};

class EyeClsfFrame {
 public:
  EyeClsfFrame() = default;
  ~EyeClsfFrame() = default;
  EyeClsfFrame(const EyeClsfFrame &) = delete;
  EyeClsfFrame &operator=(const EyeClsfFrame &) = delete;
  void ClearData() {
    for (size_t i = 0; i < EYE_ID_SIZE; i++) m_eye_info[i].clear();
    m_post_data.clear();
  }
  DEyeInfo m_eye_info;
  EyeInfo_Post m_post_data;
};
}

#endif  // HOBOT_DMS_DATA_STRUCTURE_EYECLSF_FRAME_H_
