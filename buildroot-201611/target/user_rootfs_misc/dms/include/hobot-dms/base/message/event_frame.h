#ifndef HOBOT_DMS_DATA_STRUCTURE_EVENT_FRAME_H_
#define HOBOT_DMS_DATA_STRUCTURE_EVENT_FRAME_H_

#include <stdint.h>
#include <vector>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_typedef.h"

namespace HobotDMS {

typedef enum event_state_ {
  EVENT_ST_NONE = 0,
  EVENT_ST_DDW_L = 1,
  EVENT_ST_DDW_R = 2,
  EVENT_ST_DDW_U = 3,
  EVENT_ST_DDW_D = 4,
  EVENT_ST_DFW = 5,
  EVENT_ST_DYA = 6,
  EVENT_ST_DCA = 7,
  EVENT_ST_DSA = 8,
  EVENT_ST_LDR = 9,
  EVENT_ST_DAA = 10,
  EVENT_ST_CALIB = 11,
  EVENT_ST_SIZE = 12
} event_state;

class EventResult {
 public:
  EventResult() : state(EVENT_ST_NONE), duration(0), attach_img_path("") {}
  EventResult(event_state state_, TimeStamp time_,
              std::string attach_img_path_ = "")
      : state(state_), duration(time_) {}
  ~EventResult() = default;
  event_state state;
  TimeStamp duration;
  std::string attach_img_path;
  // anything else?
};

class EventFrame {
 public:
  EventFrame() { m_events.reserve(event_state::EVENT_ST_SIZE); };
  ~EventFrame() = default;
  EventFrame(const EventFrame &) = delete;
  EventFrame &operator=(const EventFrame &) = delete;
  void ClearData() { m_events.clear(); }
  // holding events filtered from single frame's warn state
  std::vector<EventResult> m_events;
};

}  // namespave Hobot

#endif  // HOBOT_DMS_DATA_STRUCTURE_EVENT_FRAME_H_
