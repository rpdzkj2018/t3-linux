//
// Copyright 2016 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_IMAGE_H_
#define HOBOT_DMS_DATA_STRUCTURE_IMAGE_H_

#include <stdint.h>
#include <string>
#include "hobot-dms/base/dms_defines.h"

namespace HobotDMS {

class ImageFrame {
 public:
  ImageFrame();
  ImageFrame(int img_width, int img_height, int img_color_mode);
  ~ImageFrame();
  ImageFrame(const ImageFrame &) = delete;
  ImageFrame &operator=(const ImageFrame &) = delete;
  void ClearData();
  void SetColorMode(int img_color_mode);
  // bitwise copy, no data deep copy
  void CopyShallow(const ImageFrame &);
  // copys image data
  void CopyDeep(const ImageFrame &);

  uint8_t *m_data;
  uint8_t *m_origin_data;
  bool m_inited;
  int m_width;
  int m_height;
  size_t m_capacity;
  size_t m_size;
  size_t m_origin_size;
  int m_step;
  int m_origin_step;
  int m_color_mode;
  int m_origin_color_mode;
  int32_t m_luma;
  int32_t m_img_id;
  TimeStamp m_timestamp;
};

}  // end of namespace HobotDMS

#endif  // HOBOT_DMS_DATA_STRUCTURE_IMAGE_H_
