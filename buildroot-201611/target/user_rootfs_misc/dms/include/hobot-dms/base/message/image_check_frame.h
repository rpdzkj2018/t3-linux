#ifndef HOBOT_DMS_DATA_STRUCTURE_IMAGE_CHECK_FRAME_H_
#define HOBOT_DMS_DATA_STRUCTURE_IMAGE_CHECK_FRAME_H_

#include <stdint.h>
#include <vector>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_typedef.h"

namespace HobotDMS {

enum class image_check_result {
  IMAGE_CHECK_NORMAL = 0,
  IMAGE_CHECK_BLURRED = 1,
  IMAGE_CHECK_SHELTER = 2,
  IMAGE_CHECK_FREEZED = 3,
  IMAGE_CHECK_SIZE = 4
};


class ImageCheckFrame {
 public:
  ImageCheckFrame()
      : m_frame_result(image_check_result::IMAGE_CHECK_NORMAL),
        m_check_result(image_check_result::IMAGE_CHECK_NORMAL),
        m_blurred_score(0.0),
        m_shelter_score(0.0),
        m_freezed_score(0.0) {}
  ~ImageCheckFrame() = default;
  ImageCheckFrame(const ImageCheckFrame &) = delete;
  ImageCheckFrame &operator=(const ImageCheckFrame &) = delete;
  void ClearData() {
    m_frame_result = image_check_result::IMAGE_CHECK_NORMAL;
    m_check_result = image_check_result::IMAGE_CHECK_NORMAL;
    m_blurred_score = 0.0;
    m_shelter_score = 0.0;
    m_freezed_score = 0.0;
  }
  // one frame result
  image_check_result m_frame_result;

  // one check result(30 frames)
  image_check_result m_check_result;
  float m_blurred_score;
  float m_shelter_score;
  float m_freezed_score;
};

}  // namespace Hobot

#endif  // HOBOT_DMS_DATA_STRUCTURE_IMAGE_CHECK_FRAME_H_
