//
// Copyright 2017 Horizon Robotics.
//
#ifndef HOBOT_DMS_DATA_STRUCTURE_FACE_FRAME_H_
#define HOBOT_DMS_DATA_STRUCTURE_FACE_FRAME_H_

// define faceframe
#include <deque>
#include <memory>
#include <ostream>
#include <vector>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_typedef.h"

namespace HobotDMS {
typedef enum face_st_ {
  FACE_ST_NOFACE = 0,
  FACE_ST_NORMAL = 1,
  FACE_ST_ABNORMAL = 2
} face_st;

// FaceInfo数据结构会被传递到WarnPolicyModule
class FaceInfo_Post {
 public:
  FaceInfo_Post()
      : mFaceState(face_st::FACE_ST_NOFACE),
        mAbSumFaceWidth(0.0),
        mAbAvrFaceWidth(0.0),
        mAbSumSqrFaceWidth(0.0),
        mSumFaceWidth(0.0),
        mAvrFaceWidth(0.0) {}

  ~FaceInfo_Post() = default;
  void clear() {
    mFaceState = face_st::FACE_ST_NOFACE;
    mAbSumFaceWidth = 0.0;
    mAbAvrFaceWidth = 0.0;
    mAbSumSqrFaceWidth = 0.0;
    mSumFaceWidth = 0.0;
    mAvrFaceWidth = 0.0;
  }


 public:
  face_st mFaceState;        // 脸部状态
  float mAbSumFaceWidth;     // 脸部宽度总和
  float mAbAvrFaceWidth;     // 脸部宽度均值
  float mAbSumSqrFaceWidth;  // 脸部宽度均值总和
  float mSumFaceWidth;       // 脸部宽度总和
  float mAvrFaceWidth;       // 脸部宽度均值
};

class FaceFrame {
 public:
  FaceFrame() = default;
  ~FaceFrame() = default;
  FaceFrame(const FaceFrame &) = delete;
  FaceFrame &operator=(const FaceFrame &) = delete;
  void ClearData() {
    m_origin_face_rois.clear();
    m_filtered_face_rois.clear();
    m_detect_roi.Clear();
    m_post_data.clear();
  }
  std::vector<FBox_f> m_origin_face_rois;
  std::vector<FBox_f> m_filtered_face_rois;
  Rect m_detect_roi;
  FaceInfo_Post m_post_data;
};
}
#endif  // HOBOT_DMS_DATA_STRUCTURE_FACE_FRAME_H_
