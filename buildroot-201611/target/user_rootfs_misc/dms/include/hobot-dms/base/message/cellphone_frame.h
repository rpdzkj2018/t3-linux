//
// Copyright 2017 Horizon Robotics.
//
#ifndef HOBOT_DMS_DATA_STRUCTURE_CELLPHONE_FRAME_H_
#define HOBOT_DMS_DATA_STRUCTURE_CELLPHONE_FRAME_H_

// define cellphoneframe
#include <ostream>
#include <vector>
#include "hobot-dms/base/dms_typedef.h"

namespace HobotDMS {
typedef enum phone_st_ {
  PHONE_ST_EXIST,
  PHONE_ST_NOEXIST,
  PHONE_ST_UNKNOW,
  PHONE_ST_SIZE
} phone_st;

template <typename Dtype>
class PhoneInfo_ {
 public:
  PhoneInfo_() { clear(); }
  PhoneInfo_(const Rect_<Dtype> &roi, float exist_score = -1,
             float noexist_score = -1, phone_st st = PHONE_ST_UNKNOW,
             float noexist_thld = -1) {
    this->m_roi = roi;
    this->m_exist_score = exist_score;
    this->m_noexist_score = noexist_score;
    if (noexist_thld < 0)
      this->m_status = st;
    else
      this->m_status =
          m_noexist_score > noexist_thld ? PHONE_ST_NOEXIST : PHONE_ST_EXIST;
  }
  ~PhoneInfo_() = default;
  PhoneInfo_(const PhoneInfo_<Dtype> &r_smoke_status) {
    this->m_status = r_smoke_status.m_status;
    this->m_roi = r_smoke_status.m_roi;
    this->m_exist_score = r_smoke_status.m_exist_score;
    this->m_noexist_score = r_smoke_status.m_noexist_score;
  }
  PhoneInfo_ &operator=(const PhoneInfo_<Dtype> &r_smoke_status) {
    this->m_status = r_smoke_status.m_status;
    this->m_roi = r_smoke_status.m_roi;
    this->m_exist_score = r_smoke_status.m_exist_score;
    this->m_noexist_score = r_smoke_status.m_noexist_score;
    return *this;
  }
  void clear() {
    m_status = PHONE_ST_UNKNOW;
    m_exist_score = -1;
    m_noexist_score = -1;
    m_roi.Clear();
  }
  const Rect_<Dtype> &roi() const { return m_roi; }
  Rect_<Dtype> &mutable_roi() { return m_roi; }
  float exist_score() const { return m_exist_score; }
  float noexist_score() const { return m_noexist_score; }
  phone_st status() const { return m_status; }
  void set_score(float exist_score, float noexist_score) {
    this->m_exist_score = exist_score;
    this->m_noexist_score = noexist_score;
    if (this->m_exist_score > this->m_noexist_score) {
      this->m_status = phone_st::PHONE_ST_EXIST;
    } else {
      this->m_status = phone_st::PHONE_ST_NOEXIST;
    }
  }

 private:
  phone_st m_status;
  Rect_<Dtype> m_roi;
  float m_exist_score;
  float m_noexist_score;
};
typedef PhoneInfo_<int> PhoneInfo;


class Cellphone_Post {
 public:
  Cellphone_Post()
      : mSumPhoneExistScore(0.0f),
        mSumPhoneNoExistScore(0.0f),
        mAvrPhoneExistScore(0.0f),
        mAvrPhoneNoExistScore(0.0f){};
  ~Cellphone_Post() = default;
  void clear() {
    mSumPhoneExistScore = 0.0f;
    mSumPhoneNoExistScore = 0.0f;
    mAvrPhoneExistScore = 0.0f;
    mAvrPhoneNoExistScore = 0.0f;
  };

 public:
  float mSumPhoneExistScore;
  float mSumPhoneNoExistScore;
  float mAvrPhoneExistScore;
  float mAvrPhoneNoExistScore;
};

class CellphoneFrame {
 public:
  CellphoneFrame() = default;
  ~CellphoneFrame() = default;
  CellphoneFrame(const CellphoneFrame &) = delete;
  CellphoneFrame &operator=(const CellphoneFrame &) = delete;
  void ClearData() {
    m_phone_info.clear();
    m_post_data.clear();
  }
  PhoneInfo m_phone_info;
  Cellphone_Post m_post_data;
};
}

#endif  // HOBOT_DMS_DATA_STRUCTURE_CELLPHONE_FRAME_H_
