#ifndef HOBOT_DMS_BASE_MESSAGE_LDMKFRAME_H_
#define HOBOT_DMS_BASE_MESSAGE_LDMKFRAME_H_

#include <array>
#include <ostream>
#include <vector>
#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_typedef.h"
#include "hobot-dms/base/utils/quaternion.h"
#include "vision_type/vision_type.hpp"

namespace HobotDMS {
template <typename Dtype, int N>
class LandmarkN_ {
 public:
  LandmarkN_() { clear(); }
  LandmarkN_(const LandmarkN_<Dtype, N> &r_ldmks) {
    this->m_coords = r_ldmks.m_coords;
    this->m_score = r_ldmks.m_score;
  }
  ~LandmarkN_() = default;
  LandmarkN_<Dtype, N> &operator=(const LandmarkN_<Dtype, N> &r_ldmks) {
    this->m_coords = r_ldmks.m_coords;
    this->m_score = r_ldmks.m_score;
    return *this;
  }
  const Point2D_<Dtype> &operator[](std::size_t n) const { return m_coords[n]; }
  Point2D_<Dtype> &operator[](std::size_t n) { return m_coords[n]; }
  void clear() {
    m_score = 0.0;
    m_coords.resize(N);
    for (size_t i = 0; i < m_coords.size(); i++) m_coords[i].Clear();
  }
  const float &score() const { return m_score; }
  float &mutable_score() { return m_score; }
  size_t size() const { return m_coords.size(); }
  void fromHVldmks(const hobot::vision::Landmarks_<Dtype> &r_hv_ldmks) {
    clear();
    if (r_hv_ldmks.coords.size() == N * 2) {
      for (size_t i = 0; i < m_coords.size(); i++) {
        m_coords[i].x = r_hv_ldmks.coords[2 * i];
        m_coords[i].y = r_hv_ldmks.coords[2 * i + 1];
      }
    }
  }
  inline friend std::ostream &operator<<(std::ostream &out,
                                         const LandmarkN_ &r_ldmks) {
    out << "[" << r_ldmks.size() << "]:";
    for (size_t i = 0; i < r_ldmks.size(); i++) {
      out << "  " << i << ": (" << r_ldmks[i].x << ", " << r_ldmks[i].y << ") ";
    }
    out << std::endl;
    return out;
  }

 private:
  std::vector<Point2D_<Dtype>> m_coords;
  float m_score;
};
typedef LandmarkN_<float, 21> Landmarks21_f;


class LDMKInfo_Post {
 public:
  LDMKInfo_Post()
      : deque_size(0),
        mCurSclMthH2FaceH(0.),
        mAvrSclMthH2FaceH(0.),
        m_CurSclMthH2MthW(0.) {}

  ~LDMKInfo_Post() = default;
  void clear() {
    deque_size = 0;
    mAvrSclMthH2FaceH = 0.;
    mCurSclMthH2FaceH = 0.;
    m_CurSclMthH2MthW = 0.;
    m_CalibRvec.Clear();
    m_CalibRPY.Clear();
    m_CalibTvec.Clear();
    m_AvrRvec.Clear();
    m_AvrRPY.Clear();
    m_AvrTvec.Clear();
    m_CurRvec.Clear();
    m_CurRPY.Clear();
    m_CurTvec.Clear();
  }


 public:
  int deque_size;
  Vector_3f m_CalibRvec;
  Vector_3f m_CalibRPY;
  Vector_3f m_CalibTvec;

  Vector_3f m_AvrRvec;
  Vector_3f m_AvrRPY;
  Vector_3f m_AvrTvec;

  Vector_3f m_CurRvec;
  Vector_3f m_CurRPY;
  Vector_3f m_CurTvec;

  float mAvrSclMthH2FaceH;
  float mCurSclMthH2FaceH;
  float m_CurSclMthH2MthW;
};


class LDMKFrame {
 public:
  LDMKFrame() = default;
  ~LDMKFrame() = default;
  LDMKFrame(const LDMKFrame &) = delete;
  LDMKFrame &operator=(const LDMKFrame &) = delete;
  void ClearData() {
    m_landmark.clear();
    m_post_data.clear();
  }
  Landmarks21_f m_landmark;
  LDMKInfo_Post m_post_data;
};
}
#endif  // HOBOT_DMS_BASE_MESSAGE_LDMKFRAME_H_
