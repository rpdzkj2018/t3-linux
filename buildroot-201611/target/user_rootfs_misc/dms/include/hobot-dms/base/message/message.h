//
// Copyright 2016 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_MESSAGE_H_
#define HOBOT_DMS_DATA_STRUCTURE_MESSAGE_H_

#include <string>
#include <vector>

#include <hobot/hobot.h>

#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/message/frame.h"
#include "hobot-dms/base/message/jniFrame.h"
#include "hobot-dms/base/message/output_frame.h"
#include "hobot-dms/base/message/video_position.h"
#include "opencv2/opencv.hpp"

#include "hal/can_info.h"
#include "hal/ins_info.h"
// struct DMSOutput_;

namespace HobotDMS {

template <typename T>
class TemplateMessage : public hobot::Message {
 public:
  explicit TemplateMessage(const T &msg) : msg_(msg) {}
  virtual ~TemplateMessage() {}
  T &Get() { return msg_; }

 private:
  T msg_;
};

template <typename T>
class TemplateTimestampMessage : public TemplateMessage<T> {
 public:
  explicit TemplateTimestampMessage(const T &msg, TimeStamp ts)
      : TemplateMessage<T>(msg), ts_(ts) {}
  explicit TemplateTimestampMessage(TimeStamp ts) : ts_(ts) {}
  ~TemplateTimestampMessage() {}
  TimeStamp GetTimeStamp() { return ts_; }

 private:
  TimeStamp ts_;
};

typedef TemplateMessage<int> IntMessage;
typedef TemplateMessage<float> FloatMessage;
typedef TemplateMessage<std::string> StringMessage;

// typedef TemplateMessage<struct DMSOutput_ *> DMSOutputMessage;

// frame message
typedef TemplateMessage<Frame *> FrameMessage;

// position message for userctrl
typedef TemplateMessage<VideoPosition> VideoPositionMessage;

// can info message
typedef TemplateMessage<CANInfo> CANInfoMessage;

// ins info message
typedef TemplateMessage<INSInfo> INSInfoMessage;

// jni info message
typedef TemplateMessage<JniFrame> JniInfoMessage;

// algo frame message
// typedef TemplateMessage<FaceFrame> FaceFrameMessage;
// typedef TemplateMessage<LDMKFrame> LDMKFrameMessage;
// typedef TemplateMessage<EyeClsfFrame> EyeClsfFrameMessage;

// for params dyn update
// typedef TemplateMessage<TrackingParam> ParamsMessage;

}  // end of namespace HobotDMS

#endif  // HOBOT_DMS_DATA_STRUCTURE_MESSAGE_H_
