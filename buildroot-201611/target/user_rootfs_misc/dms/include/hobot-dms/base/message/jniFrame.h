//
// Copyright 2018 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_JNI_FRAME_H
#define HOBOT_DMS_DATA_STRUCTURE_JNI_FRAME_H


#include <string>
#include <vector>

#include "hobot-dms/base/dms_defines.h"
#include "hobot-dms/base/dms_typedef.h"


namespace HobotDMS {
/**
 * @brief: JniFrame
 */
class JniFrame {
 public:
  JniFrame() = default;
  ~JniFrame() = default;
  // JniFrame(const JniFrame &) = delete;
  // JniFrame &operator =(const JniFrame &) = delete;

  bool do_cali;

  void clear() { do_cali = false; }
};
}  // namespace HobotDMS


#endif /* ifndef HOBOT_DMS_DATA_STRUCTURE_JNI_FRAME_H */
