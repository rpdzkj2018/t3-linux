//
// Copyright 2016 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_FRAME_HOLDER_H_
#define HOBOT_DMS_DATA_STRUCTURE_FRAME_HOLDER_H_

#include <utility>
#include <vector>
#include <mutex>
#include <list>

#include <hobot/hobot.h>
#include "hobot-dms/base/base.h"
#include "hobot-dms/base/permission.h"
#include "hobot-dms/base/message/sensor_frame.h"

// struct DMSOutput_;

namespace HobotDMS {

class Frame;
class OutputFrame;

class FrameHolder {
 public:
  FrameHolder();
  ~FrameHolder();

  bool InitFrameBuffers(int width, int height, int color_mode);
  bool IsFrameBufferInited() { return frame_buf_inited_; }

  void FiniFrameBuffers();

  // must call ReturnFrameBuffer() refCount times to release buffer
  Frame *GetFrameBuffer(int refCount = 1);
  bool ReturnFrameBuffer(Frame *frame);
  std::mutex frame_mutex_;
  std::vector<Frame *> frames_;
  std::list<std::pair<Frame *, int> > frame_used_;
  std::list<Frame *> frame_free_;

  OutputFrame *GetOutputBuffer(Frame *related_frame);
  bool ReturnOutputBuffer(OutputFrame *output);
  std::mutex output_mutex_;
  std::vector<OutputFrame *> outputs_;
  std::list<std::pair<OutputFrame *, Frame *>> output_used_;
  std::list<OutputFrame *> output_free_;

  SensorCANFrame *unique_caninfo_frame_;
  SensorINSFrame *unique_insinfo_frame_;

  bool frame_buf_inited_;
};

}  // end of namespace HobotDMS

#endif  // HOBOT_DMS_DATA_STRUCTURE_FRAME_HOLDER_H_
