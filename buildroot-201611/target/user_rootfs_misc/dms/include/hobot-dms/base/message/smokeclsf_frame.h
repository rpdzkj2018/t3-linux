#ifndef HOBOT_DMS_DATA_STRUCTURE_SMOKECLSF_FRAME_H_
#define HOBOT_DMS_DATA_STRUCTURE_SMOKECLSF_FRAME_H_
#include "hobot-dms/base/dms_typedef.h"

// define smokeclsfframe

namespace HobotDMS {
typedef enum smoke_st_ {
  SMOKE_ST_EXIST,
  SMOKE_ST_NOEXIST,
  SMOKE_ST_UNKNOW,
  SMOKE_ST_SIZE
} smoke_st;

class Smoke_Post {
 public:
  Smoke_Post()
      : mSumSmokeExistScore(0.0f),
        mSumSmokeNoExistScore(0.0f),
        mAvrSmokeExistScore(0.0f),
        mAvrSmokeNoExistScore(0.0f){};
  ~Smoke_Post() = default;
  void clear() {
    mSumSmokeExistScore = 0.0f;
    mSumSmokeNoExistScore = 0.0f;
    mAvrSmokeExistScore = 0.0f;
    mAvrSmokeNoExistScore = 0.0f;
  };

 public:
  float mSumSmokeExistScore;
  float mSumSmokeNoExistScore;
  float mAvrSmokeExistScore;
  float mAvrSmokeNoExistScore;
};


template <typename Dtype>
class SmokeInfo_ {
 public:
  SmokeInfo_() { clear(); }
  SmokeInfo_(const Rect_<Dtype> &roi, float exist_score = -1,
             float noexist_score = -1, smoke_st st = SMOKE_ST_UNKNOW,
             float noexist_thld = -1) {
    this->m_roi = roi;
    this->m_exist_score = exist_score;
    this->m_noexist_score = noexist_score;
    if (noexist_thld < 0)
      this->m_status = st;
    else
      this->m_status =
          m_noexist_score > noexist_thld ? SMOKE_ST_NOEXIST : SMOKE_ST_EXIST;
  }
  ~SmokeInfo_() = default;
  SmokeInfo_(const SmokeInfo_<Dtype> &r_smoke_status) {
    this->m_status = r_smoke_status.m_status;
    this->m_roi = r_smoke_status.m_roi;
    this->m_exist_score = r_smoke_status.m_exist_score;
    this->m_noexist_score = r_smoke_status.m_noexist_score;
  }
  SmokeInfo_ &operator=(const SmokeInfo_<Dtype> &r_smoke_status) {
    this->m_status = r_smoke_status.m_status;
    this->m_roi = r_smoke_status.m_roi;
    this->m_exist_score = r_smoke_status.m_exist_score;
    this->m_noexist_score = r_smoke_status.m_noexist_score;
    return *this;
  }
  void clear() {
    m_status = SMOKE_ST_UNKNOW;
    m_exist_score = -1;
    m_noexist_score = -1;
    m_roi.Clear();
  }
  const Rect_<Dtype> &roi() const { return m_roi; }
  Rect_<Dtype> &mutable_roi() { return m_roi; }
  float exist_score() const { return m_exist_score; }
  float noexist_score() const { return m_noexist_score; }
  smoke_st status() const { return m_status; }
  void set_score(float exist_score, float noexist_score) {
    this->m_exist_score = exist_score;
    this->m_noexist_score = noexist_score;
    if (this->m_exist_score > this->m_noexist_score) {
      this->m_status = smoke_st::SMOKE_ST_EXIST;
    } else {
      this->m_status = smoke_st::SMOKE_ST_NOEXIST;
    }
  }
  smoke_st &mutable_status() { return this->m_status; }

 private:
  smoke_st m_status;
  Rect_<Dtype> m_roi;
  float m_exist_score;
  float m_noexist_score;
};
typedef SmokeInfo_<int> SmokeInfo;

class SmokeClsfFrame {
 public:
  SmokeClsfFrame() = default;
  ~SmokeClsfFrame() = default;
  SmokeClsfFrame(const SmokeClsfFrame &) = delete;
  SmokeClsfFrame &operator=(const SmokeClsfFrame &) = delete;
  void ClearData() {
    m_smoke_info.clear();
    m_post_data.clear();
  }
  SmokeInfo m_smoke_info;
  Smoke_Post m_post_data;
};
}

#endif  // HOBOT_DMS_DATA_STRUCTURE_SMOKECLSF_FRAME_H_
