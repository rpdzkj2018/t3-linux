//
// Copyright 2016 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_DMS_MODULE_H_
#define HOBOT_DMS_DATA_STRUCTURE_DMS_MODULE_H_

#include <hobot/hobot.h>
#include <string>
namespace dms_sdk_api {
template <typename T>
class PFaceAttrBL;
typedef PFaceAttrBL<float> PFaceAttrBLF;

template <typename T>
class FaceBBox;

typedef FaceBBox<float> FaceBBoxF;

template <typename T>
class Landmark;
typedef Landmark<float> LandmarkF;
}
namespace cv {
class Mat;
}
#define DMS_DETECT_INIT(m_detect, DetectType, context)                       \
  {                                                                          \
    std::string confinePath = GetConfigPathName(context);                    \
    LOGD_T(MODULE_TAG) << "DMSModuleInit cfg_file:" << confinePath;          \
                                                                             \
    if (confinePath.length() <= 0) {                                         \
      LOGE_T(MODULE_TAG) << "confinePath null or not exist:" << confinePath; \
      return DMS_FAIL;                                                       \
    }                                                                        \
                                                                             \
    m_detect = new DetectType();                                             \
    if (!m_detect) {                                                         \
      LOGE_T(MODULE_TAG) << "Detect new faied";                              \
      return DMS_FAIL;                                                       \
    }                                                                        \
    int ret = m_detect->Init(confinePath);                                   \
    if (ret) {                                                               \
      LOGE_T(MODULE_TAG) << "m_phoneDetect Init failed.";                    \
      delete m_detect;                                                       \
      m_detect = nullptr;                                                    \
      return DMS_FAIL;                                                       \
    }                                                                        \
  }

#define DMS_DETECT_FINISH(m_detect) \
  {                                 \
    if (m_detect) {                 \
      m_detect->Finish();           \
      delete m_detect;              \
      m_detect = nullptr;           \
    }                               \
  }

namespace HobotDMS {
class Frame;
class DMSModule : public hobot::Module {
 public:
  explicit DMSModule(std::string class_name)
      : hobot::Module("Hobot-DMS", class_name) {}

  virtual ~DMSModule() {}

  // do override this method to your own module
  virtual int DMSModuleInit(hobot::RunContext *context) = 0;

  // do not override this method again
  int Init(hobot::RunContext *context) override;
};
class DMSSDKModule : public DMSModule {
 public:
  explicit DMSSDKModule(std::string class_name) : DMSModule(class_name) {}

  virtual ~DMSSDKModule() {}

 protected:
  std::string GetConfigPathName(hobot::RunContext *context);
  int SetFaceBBox(Frame *frame, dms_sdk_api::FaceBBoxF &facebbox);
  int SetFaceLdmk(Frame *frame, dms_sdk_api::LandmarkF &ldmk);
  void DoForward(const hobot::MessageLists &input, hobot::Workflow *workflow,
                 hobot::spRunContext &context, std::string MODULE_NAME);
  virtual int InitDetectHandle(hobot::RunContext *context) = 0;
  virtual int DoProcess(Frame *frame, cv::Mat &img_mat,
                        dms_sdk_api::PFaceAttrBLF &faceinfo) = 0;
  virtual void PostProcess(Frame *frame) = 0;
};
}  // end of namespace HobotDMS

#endif  // HOBOT_DMS_DATA_STRUCTURE_DMS_MODULE_H_
