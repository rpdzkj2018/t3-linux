//
// Copyright 2016 Horizon Robotics.
//

#ifndef HOBOT_DMS_DATA_STRUCTURE_PERMISSION_H_
#define HOBOT_DMS_DATA_STRUCTURE_PERMISSION_H_

#include <stdint.h>

#include "hobot-dms/base/message/frame_holder.h"
// #include "hobot-dms/base/sensor_frame.h"
// #include "hobot-dms/base/warning_frame.h"
// #include "hobot-dms/base/face_frame.h"
// #include "hobot-dms/base/ldmk_frame.h"
// #include "hobot-dms/base/eyeclsf_frame.h"

namespace HobotDMS {

//////////////////////////////////////////////////////////////////////////
// Permission Classes

template <typename T>
class PermissonPublic {
 public:
  explicit PermissonPublic(bool del) : data_(NULL), del_(del) {}

  PermissonPublic(T *data, bool del) : data_(data), del_(del) {}

  virtual ~PermissonPublic() {
    if (del_) {
      delete data_;
    }
  }

  const T *operator->() const { return data_; }

  T *operator->() { return data_; }

  const T *Get() const { return data_; }

  T *Get() { return data_; }

  // Warning: make sure T have a operator =
  void Set(const T *data) { *data_ = *data; }

 protected:
  T *data_;
  bool del_;
};

template <typename T>
class PermissonPrivate {
 public:
  explicit PermissonPrivate(bool del) : data_(NULL), del_(del) {}

  PermissonPrivate(T *data, bool del) : data_(data), del_(del) {}

  virtual ~PermissonPrivate() {
    if (del_) {
      delete data_;
    }
  }

 protected:
  const T *operator->() const { return data_; }

  T *operator->() { return data_; }

  const T *Get() const { return data_; }

  T *Get() { return data_; }

  // Warning: make sure T have a operator =
  void Set(const T *data) { *data_ = *data; }

  T *data_;
  bool del_;
};

//////////////////////////////////////////////////////////////////////////
// Permission Settings
// TODO
#if 0
class RestrictedSensorFrame : public PermissonPrivate<SensorFrame> {
  friend class CAN::CANParser;
  friend class Frame;
  friend class FrameHolder;
  friend class HobotDMS;
  friend class MetaSerializer;
  friend class MetaDeserializer;
  friend class CANParser;
  friend class WarnPolicy;

 public:
  RestrictedSensorFrame()
      : PermissonPrivate<SensorFrame>(new SensorFrame(), true) {}
};

class RestrictedFaceFrame : public PermissonPrivate<FaceFrame> {
  friend class Frame;
  friend class MetaSerializer;
  friend class MetaDeserializer;
  friend class WarnPolicy;

 public:
  RestrictedFaceFrame() : PermissonPrivate<FaceFrame>(new FaceFrame(), true) {}
};
class RestrictedLDMKFrame : public PermissonPrivate<LDMKFrame> {
  friend class Frame;
  friend class MetaSerializer;
  friend class MetaDeserializer;
  friend class WarnPolicy;
  friend class NetworkInputModule;

 public:
  RestrictedLDMKFrame() : PermissonPrivate<LDMKFrame>(new LDMKFrame(), true) {}
};
class RestrictedEyeClsfFrame : public PermissonPrivate<EyeClsfFrame> {
  friend class Frame;
  friend class MetaSerializer;
  friend class MetaDeserializer;
  friend class WarnPolicy;

 public:
  RestrictedEyeClsfFrame()
      : PermissonPrivate<EyeClsfFrame>(new EyeClsfFrame(), true) {}
};
class RestrictedWarningFrame : public PermissonPrivate<WarningFrame> {
  friend class Frame;
  friend class MetaSerializer;
  friend class MetaDeserializer;
  friend class WarnPolicy;

 public:
  RestrictedWarningFrame()
      : PermissonPrivate<WarningFrame>(new WarningFrame(), true) {}
};
#endif

}  // end of namespace HobotDMS

#endif  // HOBOT_DMS_DATA_STRUCTURE_PERMISSION_H_
