//
// Copyright 2017 Horizon Robotics.
//

#ifndef HOBOT_DMS_BASE_DMS_TYPEDEF_H_
#define HOBOT_DMS_BASE_DMS_TYPEDEF_H_

#include <math.h>
#include <ostream>
#include "vision_type/vision_type.hpp"

namespace HobotDMS {
template <typename T>
class Rect_;

//
//  Point2D
//
template <typename T>
class Point2D_ {
 public:
  T x, y;

  Point2D_() { Clear(); }
  Point2D_(T x, T y) {
    this->x = x;
    this->y = y;
  }

  Point2D_(const Point2D_<T> &pt) {
    this->x = pt.x;
    this->y = pt.y;
  }

  Point2D_<T> &operator=(const Point2D_<T> &pt) {
    this->x = pt.x;
    this->y = pt.y;
    return *this;
  }
  bool IsIn(const Rect_<T> &rect) const {
    return this->x >= rect.l && this->x <= rect.r && this->y >= rect.t &&
           this->y <= rect.b;
  }
  bool IsInited() const { return (x != -1) && (y != -1); }
  void Clear() { x = y = -1; }
};
typedef Point2D_<int> Point;
typedef Point2D_<float> Point_f;


template <typename T>
class Vector3D_ {
 public:
  T x, y, z;

  Vector3D_() { Clear(); }
  Vector3D_(T x, T y, T z) {
    this->x = x;
    this->y = y;
    this->z = z;
  }

  Vector3D_(const Vector3D_<T> &vec) {
    this->x = vec.x;
    this->y = vec.y;
    this->z = vec.z;
  }

  Vector3D_<T> &operator=(const Vector3D_<T> &vec) {
    this->x = vec.x;
    this->y = vec.y;
    this->z = vec.z;
    return *this;
  }

  Vector3D_<T> operator/(T div) {
    return Vector3D_<T>(x/div, y/div, z/div);
  }

  void Clear() { x = y = z = 0.; }
  Vector3D_<T> Add(Vector3D_<T> vec_) {
    x += vec_.x;
    y += vec_.y;
    z += vec_.z;
    return *this;
  }

  Vector3D_<T> Minus(Vector3D_<T> vec_) {
    x -= vec_.x;
    y -= vec_.y;
    z -= vec_.z;
    return *this;
  }

  Vector3D_<T> Add(Vector3D_<T> vec_, float gain) {
    float g0 = 1 - gain;
    x = x * g0 + gain * vec_.x;
    y = y * g0 + gain * vec_.y;
    z = z * g0 + gain * vec_.z;
    return *this;
  }

  T norm() { return sqrt(x * x + y * y + z * z); }
};
typedef Vector3D_<int> Vector_3i;
typedef Vector3D_<float> Vector_3f;


//
//  Rect
//
template <class T>
class Rect_ {
 public:
  T l, t, r, b;  // left, top, right and bottom

  T CalArea() const { return (r - l) * (b - t); }
  Rect_() { Clear(); }
  Rect_(T l, T t, T r, T b) {
    this->l = l;
    this->t = t;
    this->r = r;
    this->b = b;
  }

  Rect_(const Rect_<T> &rect) {
    this->l = rect.l;
    this->t = rect.t;
    this->r = rect.r;
    this->b = rect.b;
  }

  template <class D>
  Rect_<T> &operator=(const Rect_<D> &rect) {
    this->l = (D)rect.l;
    this->t = (D)rect.t;
    this->r = (D)rect.r;
    this->b = (D)rect.b;
    return *this;
  }

  Point2D_<T> GetCenter() const {
    return Point2D_<T>((l + r) * 0.5f, (t + b) * 0.5f);
  }
  Point2D_<T> GetLeftTop() const { return Point2D_<T>(l, t); }
  Point2D_<T> GetRightBottom() const { return Point2D_<T>(r, b); }
  T GetWidth() const { return r - l; }
  T GetHeight() const { return b - t; }
  T GetRadius() const { return T(((r - l) + (b - t)) * 0.25f); }

  void ReScale_wh(float ratio_x, float ratio_y, int max_w = 1280,
                  int max_h = 720) {
    T expand_w = this->GetWidth() * ratio_x;
    T expand_h = this->GetHeight() * ratio_y;
    T real_l = l - expand_w;
    T real_r = r + expand_w;
    T real_t = t - expand_h;
    T real_b = b + expand_h;
    l = real_l > 0 ? real_l : 0;
    r = real_r < max_w ? real_r : max_w - 1;
    t = real_t > 0 ? real_t : 0;
    b = real_b < max_h ? real_b : max_h - 1;
  }

  void ReScale(float ratio_x, float ratio_y) {
    float w = (r - l) / 2.0;
    float h = (b - t) / 2.0;
    float cx = (r + l) / 2.0;
    float cy = (b + t) / 2.0;
    l = cx - w * ratio_x;
    r = cx + w * ratio_x;
    t = cy - h * ratio_y;
    b = cy + h * ratio_y;
  }
  void ReScale(float ratio) { this->ReScale(ratio, ratio); }
  void Clear() {
    l = -1;
    t = -1;
    r = -1;
    b = -1;
  }

  bool IsIn(const Rect_<T> &rect) const {
    if (l >= rect.l && r <= rect.r && t >= rect.t && b <= rect.b) {
      return true;
    } else {
      return false;
    }
  }
  bool IsInited() const {
    return (l != -1) && (r != -1) && (t != -1) && (b != -1);
  }
};
typedef Rect_<int> Rect;
typedef Rect_<float> Rect_f;

template <typename Dtype>
class FBox_ {
 public:
  FBox_() { clear(); }
  FBox_(Dtype l, Dtype t, Dtype r, Dtype b, float s = 0.0)
      : m_rect(l, t, r, b), m_score(s) {}
  FBox_(const FBox_<Dtype> &r_fbox) {
    this->m_rect = r_fbox.m_rect;
    this->m_score = r_fbox.m_score;
  }
  ~FBox_() = default;
  FBox_<Dtype> &operator=(const FBox_<Dtype> &r_fbox) {
    this->m_rect = r_fbox.m_rect;
    this->m_score = r_fbox.m_score;
    return *this;
  }
  void clear() {
    m_rect.Clear();
    m_score = 0.0;
  }
  const float &score() const { return m_score; }
  float &mutable_score() { return m_score; }
  const Rect_<Dtype> &rect() const { return m_rect; }
  Rect_<Dtype> &mutable_rect() { return m_rect; }
  const float &attri() const { return m_attri; }
  float &mutable_attri() { return m_attri; }
  void toBBox(hobot::vision::BBox_<Dtype> &r_bbox) const {
    r_bbox.x1 = m_rect.l;
    r_bbox.y1 = m_rect.t;
    r_bbox.x2 = m_rect.r;
    r_bbox.y2 = m_rect.b;
    r_bbox.score = m_score;
    r_bbox.attribute = m_attri;
  }
  void fromeBBox(const hobot::vision::BBox_<Dtype> &r_bbox) {
    this->m_rect.l = r_bbox.x1;
    this->m_rect.t = r_bbox.y1;
    this->m_rect.r = r_bbox.x2;
    this->m_rect.b = r_bbox.y2;
    this->m_score = r_bbox.score;
    this->m_attri = r_bbox.attribute;
  }

 private:
  Rect_<Dtype> m_rect;
  float m_score;
  float m_attri;
};
// typedef FBox_<int> FBox_i;
typedef FBox_<float> FBox_f;
}

#endif  // HOBOT_DMS_BASE_DMS_TYPEDEF_H_
