//
// Copyright 2016 Horizon Robotics.
//

#ifndef DMS_DMS_DEFINES_H_
#define DMS_DMS_DEFINES_H_

#include <stdint.h>
#define DMS_VERSION_MAJOR(ver) (((ver) >> 24) & 0xF)
#define DMS_VERSION_MINOR(ver) (((ver) >> 16) & 0xF)
#define DMS_VERSION_PATCH(ver) (((ver) >> 8) & 0xF)
#define DMS_VERSION_ALPHA(ver) (((ver) >> 4) & 0x7)
#define DMS_VERSION_BETA(ver) (((ver) >> 0) & 0x7)

typedef enum DMSImageColorMode_ {
  DMS_IMAGE_COLOR_GRAYSCALE = 0,
  DMS_IMAGE_COLOR_YV12 = 1,
  DMS_IMAGE_COLOR_BGR = 2,
  DMS_IMAGE_COLOR_BGR565 = 3,
  DMS_IMAGE_COLOR_RGB = 4,
  DMS_IMAGE_COLOR_JPEG = 5
} DMSImageColorMode;

typedef enum DMSOutputImgMode_ {
  DMS_OUTPUT_IMAGE_NONE = 0,
  DMS_OUTPUT_IMAGE_RAW = 1,
  DMS_OUTPUT_IMAGE_JPG = 2
} DMSOutputImgMode;

typedef enum DMS_STATUS_ {
  DMS_OK = 0,
  DMS_ALREADY_INITED = 1,
  DMS_FAIL = -1,
  DMS_INVALID_CONFIG = -2,
  DMS_NOT_INITED = -3,
  DMS_INVALID_MODE = -4,
  DMS_INPUT_BUFFER_FULL = -5,
  DMS_INVALID_PARAM = -6,
  DMS_TIMEOUT = -7,
  DMS_MMC_ID_ERROR = -8,

  DMS_META_READ_ERROR = -100,
  DMS_VIDEO_READ_ERROR = -200,

  DMS_SECURE_UNKNOWN = -1000,
  DMS_SECURE_INIT_FAILED = -1001,
  DMS_SECURE_NETWORK_ERR = -1002,
  DMS_SECURE_SYSTIME_ERR = -1003,
  DMS_SECURE_GET_UUID_ERR = -1004,
  DMS_SECURE_TIME_OUT = -1005,
  DMS_SECURE_LICENSE_NOT_EXIST = -1006,
  DMS_SECURE_LICENSE_PATH_NOT_EXIST = -1007,
  DMS_SECURE_LICENSE_PATH_NOT_SPECIFIED = -1008,
  DMS_SECURE_LICENSE_PATH_ACCESS_DENIED = -1009,
  DMS_SECURE_LICENSE_INVALID = -1010,
  DMS_SECURE_LICENSE_EXPIRED = -1011,
  DMS_SECURE_LICENSE_EXCEED_LIMIT = -1012,
  DMS_SECURE_CACERT_NOT_EXIST = -1013,
  DMS_SECURE_CACERT_NOT_SPECIFIED = -1014,
  DMS_SECURE_ACTIVATE_INVALID = -1015,
  DMS_SECURE_MODULE_UNAUTHORIZED = -1016,
  DMS_SECURE_IC_INVALID = -1017,
  DMS_SECURE_THREAD_NOTUP = -1018,
} DMS_STATUS;

typedef enum DMS_CALLBACK_ {
  DMS_CALLBACK_END_OF_FILE = 0x01,
  DMS_CALLBACK_IMG_OUT = 0x02,
  DMS_CALLBACK_STATUS = 0x04,
  DMS_CALLBACK_EVENT_UPLOAD = 0x08,
  DMS_CALLBACK_DATA_OUT = 0x10,
  DMS_CALLBACK_ALL = 0x1f
} DMS_CALLBACK;

typedef int64_t TimeStamp;  // unit: ms

// structs for data output
typedef enum DMS_WARNSTATE_ {
  STATE_NORMAL = 0,
  STATE_FORWARD = 1,
  STATE_FATIGUE = 2,
  STATE_DANGER = 3,
} DMS_WARNSTATE;

// structs for feed data
typedef struct DMSImage_ {
  int width;
  int height;
  int channel;
  int step;
  int color_mode;
  uint8_t *data;
  int size;
  TimeStamp timestamp;
} DMSImage;

// vehicle info
typedef struct DMSCANInfo_ {
  int32_t Gears;
  int32_t Speed;
  int32_t Direction;
  int32_t Angle;
  TimeStamp timestamp;
} DMSCANInfo;

typedef struct DMSINSInfo_ {
  float Yawrate;
  int32_t Speed;
  TimeStamp timestamp;
} DMSINSInfo;

typedef struct DMSJNIInfo_ {
  bool do_cali;
}DMSJNIInfo;

// struct for output data
typedef struct DMSOutputData_ {
  DMSImage image;
  int32_t image_id;
  DMS_WARNSTATE st;
} DMSOutputData;

typedef struct DMSModuleStatus_ {
  char name[32];
  int code;
  TimeStamp timestamp;
} DMSModuleStatus;

typedef struct DMSEventAttach_ {
  char type[32];
  char path[256];
} DMSEventAttach;

typedef struct DMSUploadEvent_ {
  char type[16];
  int attach_mode;
  DMSEventAttach *attached;
  int attached_size;
  TimeStamp ts;
  TimeStamp slice_start_ms;
  TimeStamp slice_end_ms;
} DMSUploadEvent;

// struct for output event
typedef struct DMSSDKOutput_ {
  // serialize of DMSSDKOutput.proto
  uint8_t *meta;
  int meta_size;
} DMSSDKOutput;

typedef struct DMSUVCParams_ {
  int fd;           // usb fd
  int vid;          // usb vid
  int pid;          // usb pid
  int busnum;       // usb busnum
  int devaddr;      // usb devaddr
  char usbfs[128];  // usb fs dir
} DMSUVCParams;

#ifdef __cplusplus
#include <functional>
typedef std::function<int(uint32_t, void *)> DMSCallBack;
#else
typedef int (*DMSCallBack)(uint32_t, void *);
#endif

#endif  // DMS_DMS_DEFINES_H_
