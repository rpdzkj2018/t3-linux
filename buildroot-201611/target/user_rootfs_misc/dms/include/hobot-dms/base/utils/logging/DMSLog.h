#ifndef __DMS_DMSLOG_H__
#define __DMS_DMSLOG_H__

#include <string>
#include "hobotlog/hobotlog.hpp"

#ifndef MODULE_TAG
#define MODULE_TAG "HobotDMS"
#endif

// #ifdef LOG_TAG
// #undef LOG_TAG
// #define LOG_TAG(sev, tag)        \
  // LOG_SEVERITY_PRECONDITION(sev) \
  // hobotlog::LogMessage(NULL, __LINE__, sev, tag).stream()
// #endif

#ifdef LOGV_T
#undef LOGV_T
#define LOGV_T(tag) \
  LOG_TAG(hobotlog::HOBOT_LOG_VERBOSE, std::string("[DMS] ") + tag)

#endif
#ifdef LOGD_T
#undef LOGD_T
#define LOGD_T(tag) \
  LOG_TAG(hobotlog::HOBOT_LOG_DEBUG, std::string("[DMS] ") + tag)
#endif

#ifdef LOGI_T
#undef LOGI_T
#define LOGI_T(tag) \
  LOG_TAG(hobotlog::HOBOT_LOG_INFO, std::string("[DMS] ") + tag)
#endif

#ifdef LOGW_T
#undef LOGW_T
#define LOGW_T(tag) \
  LOG_TAG(hobotlog::HOBOT_LOG_WARN, std::string("[DMS] ") + tag)
#endif

#ifdef LOGE_T
#undef LOGE_T
#define LOGE_T(tag) \
  LOG_TAG(hobotlog::HOBOT_LOG_ERROR, std::string("[DMS] ") + tag)
#endif


#define FUN_IN(modulename)                             \
  do {                                                 \
    LOGV_T(modulename) << "(" << __func__ << ") ++in"; \
  } while (0);
#define FUN_OUT(modulename)                             \
  do {                                                  \
    LOGV_T(modulename) << "(" << __func__ << ") --out"; \
  } while (0);

// Added by yepanl
#ifdef PROC_NAME_ENABLE
#include <sys/prctl.h>
#define SET_PROC_NAME(name)                         \
  do {                                              \
    static int flag = 0;                            \
    if (!flag) {                                    \
      prctl(PR_SET_NAME, name, NULL, NULL, NULL);   \
      flag = 1;                                     \
    }                                               \
  } while (0);
#else
#define SET_PROC_NAME(name)
#endif

#endif  //__DMS_DMSLOG_H__
