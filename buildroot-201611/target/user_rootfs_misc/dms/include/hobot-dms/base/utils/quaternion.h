//
// Copyright 2017 Horizon Robotics.
//

#ifndef HOBOT_UTILS_QUATERNION_H_
#define HOBOT_UTILS_QUATERNION_H_

namespace HobotDMS {

template <class T>
struct EulerAngle {
  T roll, pitch, yaw;

  EulerAngle() {
    roll = pitch = yaw = 0;
  }

  EulerAngle(T roll, T pitch, T yaw) {
    this->roll = roll;
    this->pitch = pitch;
    this->yaw = yaw;
  }

  EulerAngle<T>& operator = (const EulerAngle<T> &ea) {
    this->roll = ea.roll;
    this->pitch = ea.pitch;
    this->yaw = ea.yaw;
    return *this;
  }
};
typedef EulerAngle<float> EulerAngleF;

class Quaternion {
 public:
  Quaternion();

  /* construct normalized quaternion by independent elements */
  explicit Quaternion(float q0, float q1, float q2, float q3);

  /* construct normalized quaternion by euler angles */
  explicit Quaternion(const EulerAngleF &rpy);
  explicit Quaternion(float roll, float pitch, float yaw);

  /* reset quaternion to (1, 0, 0, 0) */
  void reset();

  /* set elements and normalize */
  Quaternion & set(float q0, float q1, float q2, float q3);

  /* get elements */
  void get(float &q0, float &q1, float &q2, float &q3) const;

  float q0() {return q0_;};
  float q1() {return q1_;};
  float q2() {return q2_;};
  float q3() {return q3_;};
  /* norm
  * |q| = sqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3)
  */
  float norm() const;

  /* conjugate
  * q* = (q0, -q1, -q2, -q3)
  */
  Quaternion conj() const;

  /* inversion
  * q.inv() = q* / (|q|^2)
  */
  Quaternion inv() const;

  /* addition
  * p + q = (p0 + q0, p1 + q1, p2 + q2, p3 + q3)
  * return normalized quaternion
  */
  Quaternion operator + (const Quaternion &q) const;
  Quaternion & operator += (const Quaternion &q);

  /* subtraction
  * p - q = (p0 - q0, p1 - q1, p2 - q2, p3 - q3)
  * return normalized quaternion
  */
  Quaternion operator - (const Quaternion &q) const;
  Quaternion & operator -= (const Quaternion &q);

  /* multiplication
  * p * q = (p0q0 - p1q1 - p2q2 - p3q3, p0q1 + p1q0 + p2q3 - p3q2,
  *          p0q2 - p1q3 + p2q0 + p3q1, p0q3 + p1q2 - p2q1 + p3q0)
  * return normalized quaternion
  *
  * The orientation quaternion after a sequence of rotations can be
  * easily found by quaternion multiplication. For example, given
  * three frames A, B and C, and given the quaternion q(B->A)
  * orientation of frame A expressed with respect to frame B and
  * given the quaternion q(C->B) orientation of frame B expressed
  * with respect to frame C, the orientation of frame A with respect
  * to frame C is characterized by: q(C->A) = q(C->B) * q(B->A)
  * ATTENTION: p * q != q * p
  */
  Quaternion operator * (const Quaternion &q) const;
  Quaternion & operator *= (const Quaternion &q);

  /* division
  * p / q = q.inv() * p
  * return normalized quaternion
  *
  * Contrary to multiplication, 'division' represents the difference
  * between two quaternions. Thus, given q(C->B) and q(C->A),
  * the orientation of frame A with respect to frame B can be
  * characterized by: q(B->A) = q(C->A) / q(C->B)
  */
  Quaternion operator / (const Quaternion &q) const;
  Quaternion & operator /= (const Quaternion &q);

  /* Spherical Linear Interpolation
  * slerp(p,q,gain) =
  *  ((sin((1-gain)theta))/sin(theta))p + (sin((gain)theta)/sin(theta))q
  * where theta = acos(p.dot(q))
  * return normalized quaternion
  */
  Quaternion slerp(float gain, const Quaternion &q) const;

  /* rotate vector
  * Given arbitrary vector vb(xb, yb, zb) of frame B, and the
  * quaternion q(A->B) orientation of frame B with respect to
  * frame A, then the representation of vector vb in frame A
  * can be characterized by: va = (xa, ya, za), where
  * xa = (q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3) * xb +
  *      2 * (q1 * q2 - q0 * q3) * yb +
  *      2 * (q1 * q3 + q0 * q2) * zb
  * ya = 2 * (q1 * q2 + q0 * q3) * xb +
  *      (q0 * q0 - q1 * q1 + q2 * q2 - q3 * q3) * yb +
  *      2 * (q2 * q3 - q0 * q1) * zb
  * za = 2 * (q1 * q3 - q0 * q2) * xb +
  *      2 * (q2 * q3 + q0 * q1) * yb +
  *      (q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3) * zb
  */
  void rotate(float xb, float yb, float zb,
    float &xa, float &ya, float &za) const;

  /* accumulate quaternion q(Earth->Body) orientation with euler angle */
  Quaternion & inc(const EulerAngleF &rpy);

  /* accumulate quaternion q(Earth->Body) orientation with angular velocity */
  Quaternion & inc(float wx, float wy, float wz, float t);

  /* convert euler angle to quaternion */
  Quaternion & fromRPY(const EulerAngleF &rpy);
  Quaternion & fromRPY(float roll, float pitch, float yaw);

  /* convert quaternion to euler angle */
  void getRPY(EulerAngleF &rpy) const;
  void getRPY(float &roll, float &pitch, float &yaw) const;
  EulerAngleF toRPY() const;

 private:
  /* scalar product */
  Quaternion multi(float d) const;

  /* dot product
  * p.dot(q) = (p0q0 + p1q1 + p2q2 + p3q3)
  */
  float dot(const Quaternion &q) const;

  /* |q|^2 */
  float norm_square() const;

  /* set elements without normalize */
  Quaternion & set_origin(float q0, float q1, float q2, float q3);

  /* normalize */
  Quaternion & normalize();

 private:
  float q0_, q1_, q2_, q3_;
};

}  // end of namespace HobotDMS
#endif  // HOBOT_UTILS_QUATERNION_H_
