//
// Copyright 2017 Horizon Robotics.
//

#ifndef SRC_BASE_UTILS_BUFFERT_H_
#define SRC_BASE_UTILS_BUFFERT_H_
#include <stddef.h>
#include <memory>

template <typename T, size_t num = 100>
class BufferT {
 public:
  explicit BufferT(size_t max = num) : buf_(NULL), m_wpos(0), m_maxnum(max), m_empty(1) {
    reset();
  }

  ~BufferT() {
    if (buf_) {
      delete[] buf_;
      buf_ = NULL;
    }
  }

  void reset() {
    m_wpos = 0;
    m_empty = 1;
    if (buf_ == NULL) {
      buf_ = new T[m_maxnum];
    }
    memset(buf_, 0, sizeof(T) * m_maxnum);
  }

  /* push a sample to ring buffer, and return the index position of pushed
   * sample */
  int push(const T *value) {
    int writeIdx = m_wpos;
    buf_[m_wpos] = *value;
    m_wpos = nextIdx(m_wpos);
    m_empty = 0;
    return writeIdx;
  }

  void push(const T *src, size_t count) {
    int remain = m_maxnum - m_wpos;
    if (remain < count) {
      memcpy(&buf_[m_wpos], src, (remain) * sizeof(T));
      memcpy(&buf_[0], &src[remain], (count - remain) * sizeof(T));
      m_wpos = count - remain;
    } else {
      memcpy(&buf_[m_wpos], src, count * sizeof(T));
      if (m_wpos + count >= m_maxnum)
        m_wpos = 0;
      else
        m_wpos = m_wpos + count;
    }
    m_empty = 0;
  }

  /* [] operator to access sample of given index */
  T &operator[](int idx) { return buf_[idx]; }

  /* return the current position to be write, previous is last write valid
   * data */
  int curIdx() { return preIdx(m_wpos); }

  /* return the last valid data
   * @return
   *     NULL - if no sample valid in buffer
   *     T* - the sample pointer for the latest valid data */
  T *curPos() {
    if (m_empty)
      return NULL;
    T *dataptr = buf_;
    int head = preIdx(m_wpos);
    return dataptr + head;
  }

  /* @seekbytime : seek to the sample right (sample.ts >= time) after the given
   * time stamp
   * @param time - the time position seek to
   * @return
   *     NULL - if no sample after the give time stamp position
   *     T* - the sample pointer right after the given time stamp position */
  T *seekbytime(int64_t time) {
    if (m_empty)
      return NULL;
    T *dataptr = buf_;
    int head = preIdx(m_wpos);

    // LOGW("bufferT seek head t: %lld, last t: %lld", dataptr[head].ts,
    // dataptr[next].ts);
    if (time > dataptr[head].ts)
      return NULL;

    int k = head;
    while (dataptr[k].ts >= time) {
      k = preIdx(k);
      if (k == head)
        break;
    }

    // if the seek time is the past of current, then we always have a sample
    // to return.
    k = nextIdx(k);
    return dataptr + k;
  }

  /* return pointer points to the next sample of the given sample */
  T *next(T *prev_address) {
    if (m_empty)
      return NULL;
    T *next = prev_address + 1;
    if (next >= buf_ + m_maxnum) {  // overflow
      next = buf_;
    }
    if (next != buf_ + m_wpos)
      return next;
    else
      return NULL;
  }

  /* return the next index of ring buffer */
  inline int nextIdx(int idx) {
    int next = idx + 1;
    if (next >= m_maxnum)
      next = 0;
    return next;
  }

  /* return the previous index of the ring buffer */
  inline int preIdx(int idx) {
    int pre = idx - 1;
    if (pre < 0)
      pre = m_maxnum - 1;
    return pre;
  }

  /* @revertTo revert the write position to the sample right before the given
   * time stamp
   * @param time - given timestamp position
   * @return
   *     -1 - if no history data right before the given timestamp
   *     idx - index of the sample right before the given timestamp
   */
  int revertTo(int64_t time) {
    T *p = seekbytime(time);
    if (p == NULL)
      return -1;

    int idx = ((int)p - (int)buf_) / sizeof(T);
    m_wpos = idx;
    return preIdx(idx);
  }

 private:
  T *buf_;
  size_t m_wpos;
  int m_maxnum;
  int m_empty;
};

#endif  // SRC_BASE_UTILS_BUFFERT_H_
