//
// Copyright 2016 Horizon Robotics.
//

#ifndef HOBOT_DMS_UTILS_MEMORY_MEMORY_H_
#define HOBOT_DMS_UTILS_MEMORY_MEMORY_H_

#include <stddef.h>

namespace HobotDMS {

const int ALIGNMENT_SIZE_BIT = 4;
const int ALIGNMENT_SIZE = 1 << ALIGNMENT_SIZE_BIT;

#define ALIGNMENT_ROUND_UP(x) \
  (((x)+ALIGNMENT_SIZE - 1) >> ALIGNMENT_SIZE_BIT << ALIGNMENT_SIZE_BIT)
#define ALIGNMENT_ROUND_UP_BYBIT(x, y) (((x) + (1 << (y)) - 1) >> (y) << (y))

void *MallocAlignedMemory(size_t memory_size);

void FreeAlignedMemory(void *&memory);

}  // end of namespace HobotDMS

#endif  // HOBOT_DMS_UTILS_MEMORY_MEMORY_H_
