//
// Copyright 2016 Horizon Robotics.
//
#ifndef SRC_HOBOT_DMS_UTILS_SERIALIZE_SERIALIZER_H_
#define SRC_HOBOT_DMS_UTILS_SERIALIZE_SERIALIZER_H_

#include <stdint.h>
#include <fstream>
#include <iostream>
#include <memory>
#include <vector>
#include "hobot-dms/base/message/frame.h"


namespace DMSFrameProtocol {
class DMSFrame;
class ImageInfo;
class VehicleInfo;
class EventResult;
class PolicyResult;
class DSAPolicyResult;
class DCAPolicyResult;
class DYAPolicyResult;
class DDWPolicyResult;
class DFWPolicyResult;
class DAAPolicyResult;
class AlgoResult;
class SmokeClsfPost;
class SmokeClsfResult;
class PhoneClsfResult;
class PhoneClsfPost;
class EyesPost;
class Eyes;
class LandMark21;
class LandmarkPost;
class FaceDetectPost;
class FaceDetectResult;
class ImageCheckFrameInfo;
class ImageCheckResult;
class FaceCalibResult;
class FaceDirectionResult;
}

namespace DMSOutputProtocol {
class DMSSDKOutput;
class EventResult;
class FaceROIResult;
class FaceDirectionResult;
class FaceFeaturePoint;
class EyeStateResult;
class SmokeStateResult;
class PhoneStateResult;
class EventResult;
class StateResult;
class ImageCheckResult;
class FaceCalibResult;
}

namespace HobotDMS {
class Frame;
class HobotDMSContext;

/* class MetaSerializer */
class MetaSerializer {
 public:
  MetaSerializer();
  ~MetaSerializer();

  bool Init(HobotDMSContext *context);
  void Fini();

  size_t Frame2Proto(const HobotDMSContext *context, Frame *frame);
  bool Serialize(uint8_t *buf, size_t buf_size);
  bool Serialize(std::ofstream &ofs, size_t buf_size);

 private:
  std::unique_ptr<DMSFrameProtocol::DMSFrame> mp_proto_serialize;

  void WriteFrameIDPb(const Frame *const frame);
  void WriteImageInfoPb(const ImageFrame &imageframe,
                        DMSFrameProtocol::ImageInfo *const image);
  void WriteAlgoResultPb(const Frame *const frame,
                         DMSFrameProtocol::AlgoResult *const algo_result);
  void WritePolicyReusltPb(
      const WarningFrame &warning_frame,
      DMSFrameProtocol::PolicyResult *const p_policy_result);
  void WriteEventResultPb(const EventResult &event_result,
                          DMSFrameProtocol::EventResult *const p_event_result);
  void WriteVehicleInfoPb(const VehicleInfoFrame &vehinf_frame,
                          DMSFrameProtocol::VehicleInfo *const p_veh_info);
  void WriteImageCheckFrameInfoPb(
      const ImageCheckFrame &imgcheck_frame,
      DMSFrameProtocol::ImageCheckFrameInfo *const p_img_check_info);
  void WriteImageCheckResultPb(
      const ImageCheckFrame &imgcheck_frame,
      DMSFrameProtocol::ImageCheckResult *const p_img_check_result);
  void WriteFaceCalibResultPb(
      const FaceCalibFrame &facecalib_frame,
      DMSFrameProtocol::FaceCalibResult *const p_face_calib_result);
  void WriteFaceDirResultPb(
      const Frame &r_frame,
      DMSFrameProtocol::FaceDirectionResult *const p_face_dir_result);
  void WriteAlgoResultPb_FaceDetectResult(
      const FaceFrame &r_face_frame, DMSFrameProtocol::FaceDetectResult *const);
  void WriteAlgoResultPb_FaceDetectPost(
      const FaceInfo_Post &r_face_post,
      DMSFrameProtocol::FaceDetectPost *const);

  void WriteAlgoResultPb_PhoneClsfResult(
      const CellphoneFrame &, DMSFrameProtocol::PhoneClsfResult *const);
  void WriteAlgoResultPb_PhoneClsfPost(const Cellphone_Post &,
                                       DMSFrameProtocol::PhoneClsfPost *const);

  void WriteAlgoResultPb_LandMark(const LDMKFrame &,
                                  DMSFrameProtocol::LandMark21 *const);
  void WriteAlgoResultPb_LandMarkPost(const LDMKInfo_Post &,
                                      DMSFrameProtocol::LandmarkPost *const);
  void WriteAlgoResultPb_Eyes(const EyeClsfFrame &,
                              DMSFrameProtocol::Eyes *const);
  void WriteAlgoResultPb_EyesPost(const EyeInfo_Post &,
                                  DMSFrameProtocol::EyesPost *const);
  void WriteAlgoResultPb_SmokeClsResult(
      const SmokeClsfFrame &, DMSFrameProtocol::SmokeClsfResult *const);
  void WriteAlgoResultPb_SmokeClsPost(const Smoke_Post &,
                                      DMSFrameProtocol::SmokeClsfPost *const);

  void WritePolicyReusltPb_DDWPolicyResult(
      const DDW_result &, DMSFrameProtocol::DDWPolicyResult *const);
  void WritePolicyReusltPb_DFWPolicyResult(
      const DFW_result &, DMSFrameProtocol::DFWPolicyResult *const);
  void WritePolicyReusltPb_DYAPolicyResult(
      const DYA_result &, DMSFrameProtocol::DYAPolicyResult *const);
  void WritePolicyReusltPb_DCAPolicyResult(
      const DCA_result &, DMSFrameProtocol::DCAPolicyResult *const);
  void WritePolicyReusltPb_DSAPolicyResult(
      const DSA_result &, DMSFrameProtocol::DSAPolicyResult *const);
  void WritePolicyReusltPb_DAAPolicyResult(
      const DAA_result &, DMSFrameProtocol::DAAPolicyResult *const);
};


/* class MetaDeserializer */
class MetaDeserializer {
 public:
  MetaDeserializer();
  ~MetaDeserializer();

  bool Init(HobotDMSContext *context);
  void Fini();

  bool Deserialize(std::ifstream &ifs, size_t pos, size_t meta_len);
  bool Deserialize(uint8_t *buf, size_t buf_size);
  bool Proto2Frame(HobotDMSContext *context, Frame *frame,
                   bool image_info = true, bool algo_info = true,
                   bool policy_info = true, bool event_info = true,
                   bool veh_info = true, bool check_info = true);
  std::unique_ptr<DMSFrameProtocol::DMSFrame> mp_proto_deserialize;

 private:
  void WriteFrameIDFrame(Frame *const frame);
  void WriteImageInfoFrame(const DMSFrameProtocol::ImageInfo &image,
                           ImageFrame &imageframe);
  void WriteAlgoResultFrame(const DMSFrameProtocol::AlgoResult &algo_result,
                            Frame *const frame);
  void WritePolicyReusltFrame(
      const DMSFrameProtocol::PolicyResult &policy_result,
      WarningFrame &warning_frame);
  void WriteEventResultFrame(const DMSFrameProtocol::EventResult &event_result,
                             EventResult &event_frame);
  void WriteVehicleInfoFrame(const DMSFrameProtocol::VehicleInfo &veh_info,
                             VehicleInfoFrame &vehinf_frame);
  void WriteImageCheckInfoFrame(
      const DMSFrameProtocol::ImageCheckFrameInfo &img_check_info,
      ImageCheckFrame &imgcheck_frame);
  void WriteImageCheckResultFrame(
      const DMSFrameProtocol::ImageCheckResult &img_check_result,
      ImageCheckFrame &imgcheck_frame);
  void WriteFaceCalibResultFrame(
      const DMSFrameProtocol::FaceCalibResult &face_calib_result,
      FaceCalibFrame &facecalib_frame);
  void WriteFaceDirResultFrame(
      const DMSFrameProtocol::FaceDirectionResult &dir_info, Frame &r_frame);
  void WriteAlgoResultFrame_FaceDetectResult(
      const DMSFrameProtocol::FaceDetectResult &, FaceFrame &face_frame);
  void WriteAlgoResultFrame_FaceDetectPost(
      const DMSFrameProtocol::FaceDetectPost &, FaceInfo_Post &face_frame);

  void WriteAlgoResultFrame_PhoneClsfResult(
      const DMSFrameProtocol::PhoneClsfResult &, CellphoneFrame &);
  void WriteAlgoResultFrame_PhoneClsfPost(
      const DMSFrameProtocol::PhoneClsfPost &, Cellphone_Post &);

  void WriteAlgoResultFrame_LandMark(const DMSFrameProtocol::LandMark21 &,
                                     LDMKFrame &);
  void WriteAlgoResultFrame_LandMarkPost(const DMSFrameProtocol::LandmarkPost &,
                                         LDMKInfo_Post &);
  void WriteAlgoResultFrame_Eyes(const DMSFrameProtocol::Eyes &,
                                 EyeClsfFrame &);
  void WriteAlgoResultFrame_EyesPost(const DMSFrameProtocol::EyesPost &,
                                     EyeInfo_Post &);
  void WriteAlgoResultFrame_SmokeClsResult(
      const DMSFrameProtocol::SmokeClsfResult &, SmokeClsfFrame &);
  void WriteAlgoResultFrame_SmokeClsPost(
      const DMSFrameProtocol::SmokeClsfPost &, Smoke_Post &);

  void WritePolicyReusltFrame_DDWPolicyResult(
      const DMSFrameProtocol::DDWPolicyResult &, DDW_result &);
  void WritePolicyReusltFrame_DFWPolicyResult(
      const DMSFrameProtocol::DFWPolicyResult &, DFW_result &);
  void WritePolicyReusltFrame_DYAPolicyResult(
      const DMSFrameProtocol::DYAPolicyResult &, DYA_result &);
  void WritePolicyReusltFrame_DCAPolicyResult(
      const DMSFrameProtocol::DCAPolicyResult &, DCA_result &);
  void WritePolicyReusltFrame_DSAPolicyResult(
      const DMSFrameProtocol::DSAPolicyResult &, DSA_result &);
  void WritePolicyReusltFrame_DAAPolicyResult(
      const DMSFrameProtocol::DAAPolicyResult &, DAA_result &);
};

/* class SDKOutMetaSerializer */
class SDKOutMetaSerializer {
 public:
  SDKOutMetaSerializer();
  ~SDKOutMetaSerializer();

  bool Init(HobotDMSContext *context);
  void Fini();

  size_t Frame2Proto(Frame *frame);
  bool Serialize(uint8_t *buf, size_t buf_size);
  bool Serialize(std::ofstream &ofs, size_t buf_size);

 private:
  void WriteFaceROIResult(
      const FBox_f &r_face_roi,
      DMSOutputProtocol::FaceROIResult *const p_face_roi_result);
  void WriteFaceDirResult(
      const Frame &r_frame,
      DMSOutputProtocol::FaceDirectionResult *const p_face_dir_result);
  void WriteFeceFeatureResult(
      const LDMKFrame &r_ldmk,
      DMSOutputProtocol::FaceFeaturePoint *const p_face_feature_result);
  void WriteEyeStateResult(
      const EyeClsfFrame &r_eye_clsf,
      DMSOutputProtocol::EyeStateResult *const p_eye_state_result);
  void WriteSmokeStateResult(
      const SmokeClsfFrame &r_smoke_clsf,
      DMSOutputProtocol::SmokeStateResult *const p_smoke_state_result);
  void WritePhoneStateResult(
      const CellphoneFrame &r_phone,
      DMSOutputProtocol::PhoneStateResult *const p_phone_state_result);
  void WriteImageCheckResult(
      const ImageCheckFrame &r_img_check_frame,
      DMSOutputProtocol::ImageCheckResult *const p_image_check_result);
  void WriteFaceCalibResult(
      const FaceCalibFrame &face_calib_frame,
      DMSOutputProtocol::FaceCalibResult *const p_face_calib_result);
  void WriteStateResult(const WarningFrame &state,
                        DMSOutputProtocol::DMSSDKOutput *const p_state_result);
  void WriteEventResult(const EventResult &event,
                        DMSOutputProtocol::EventResult *const p_event_result);


 private:  // private value
  std::unique_ptr<DMSOutputProtocol::DMSSDKOutput> mp_proto_serialize;
};


}  // end of namespace HobotDMS

#endif  // SRC_HOBOT_DMS_UTILS_SERIALIZE_SERIALIZER_H_
