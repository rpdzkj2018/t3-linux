//
// Copyright 2016 Horizon Robotics.
//

#ifndef SRC_HOBOT_DMS_HOBOT_DMS_H_
#define SRC_HOBOT_DMS_HOBOT_DMS_H_

#include <cstring>

#include <list>
#include <memory>
#include <mutex>
#include <utility>
#include <vector>

#include <hobot/hobot.h>
#include "hobot-dms/base/base.h"

namespace HobotDMS {

#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_PATCH 0
#define VERSION_ALPHA 0
#define VERSION_BETA 0

class HobotDMSContext;
class Frame;

typedef std::vector<std::pair<hobot::Module *, int>> ObserverBinding;

class HobotDMS {
 public:
  static int GetVersion() {
    int nVersion = (VERSION_MAJOR << 24) | (VERSION_MINOR << 16) |
                   (VERSION_PATCH << 8) | (VERSION_ALPHA << 4) | (VERSION_BETA);
    return nVersion;
  };

  HobotDMS();
  virtual ~HobotDMS();

  HobotDMS(const HobotDMS &);
  HobotDMS &operator=(const HobotDMS &);

  int Init(const char *config_file, const DMSUVCParams *uvc_params = NULL);
  void Fini();
  bool IsInited() { return inited_; }

  int GetInitedModules(std::vector<DMSModuleStatus> &module_list);

  // callbacks
  void SetCallback(uint32_t type, DMSCallBack func);

  // input api mode
  int FeedImage(const DMSImage *image);
  int FeedImei(const char *imei, int length);
  int FeedCANInfo(const DMSCANInfo *can_info);
  int FeedINSInfo(const DMSINSInfo *ins_info);
  int FeedJniInfo(const DMSJNIInfo *jni_info);

  // output api mode
  int GetOutput(DMSOutputData *output, bool with_image);

  // set out img color mode
  int SetOutImgMode(int mode);

  // manually start image check
  int RunImageCheck(void);

  virtual void ConstructWorkflow(hobot::Module *input,
                                 hobot::Module *post_process) {
    workflow_main_->From(input)->To(post_process);

    /* condition expression settings example
    // condition start
    int slot_idx = 0;
    hobot::Expression *expr =
    hobot::Expression::Require(0, hobot::ReqAll);

    // add a module link
    workflow_main_->From(input)->To(post_process);
    expr = expr->And(hobot::Expression::Require(slot_idx++, 1));

    // add more links exactly like above
    // ...

    // set module's forward_0's condition
    workflow_main_->SetCondition(post_process, expr, 0);
    conditioins_.push_back(expr);
    */
  }

 protected:
  bool inited_;

  hobot::Engine *engine_;
  hobot::Workflow *workflow_main_;
  hobot::spRunContext workflow_main_rt_ctx_;
  // trigger_module_ is an input module which just transmit one message to start
  // the workflow
  hobot::InputModule *trigger_module_;
  std::vector<hobot::Module *> modules_;
  std::vector<hobot::Expression *> conditioins_;
  hobot::RunObserver *observer_;

  // modules for api feed input
  hobot::Module *ext_img_input_;
  hobot::Module *ext_can_input_;
  hobot::Module *ext_ins_input_;
  hobot::Module *ext_jni_input_;
  uint32_t ext_img_counter_;

  hobot::Module *ext_check_input_;
  HobotDMSContext *dms_context_;
};

}  // end of namespace HobotDMS

#endif  // SRC_HOBOT_DMS_HOBOT_DMS_H_
