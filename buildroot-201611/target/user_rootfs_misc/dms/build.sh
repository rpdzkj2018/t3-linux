source ../../../../misc_config
LICHEE_TOOL=out/${MISC_CHIP}/linux/common/buildroot/host/opt/ext-toolchain/bin
LICHEE_OUT=out/${MISC_CHIP}/linux/common/buildroot
if [ $MISC_CHIP = "sun8iw11p1" ];then
	if [ $MISC_GNUEABI = "gnueabi" ];then
		cp -f doc/T3_CMakeLists.txt  CMakeLists.txt
	else
		cp -f doc/T3_hf_CMakeLists.txt  CMakeLists.txt
	fi
else
	if [ $MISC_GNUEABI = "gnueabi" ];then
		cp -f doc/T7_CMakeLists.txt  CMakeLists.txt
	else
		cp -f doc/T7_hf_CMakeLists.txt  CMakeLists.txt
	fi
fi

rm -rf allwinner
rm -rf build
mkdir build
cd build
pwdpath=$(pwd)
sdk_path=$pwdpath/../../../../..
$sdk_path/${LICHEE_OUT}/host/usr/bin/cmake -DCMAKE_CXX_COMPILER=$sdk_path/${LICHEE_TOOL}/arm-linux-${MISC_GNUEABI}-g++ -DCMAKE_C_COMPILER=$sdk_path/${LICHEE_TOOL}/arm-linux-${MISC_GNUEABI}-gcc ..

make install
cd ..
#cp -rf allwinner/lib/* ../sdk_lib/libs/dms/
cp -rf allwinner/* ../../../../${LICHEE_OUT}/target/