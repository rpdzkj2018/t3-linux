//
// Copyright 2017 Horizon Robotics.
//

#include "DMSSDKOutput.pb.h"
#include "hobot_dms_sdk.h"
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <pthread.h>
#include <string>
#include <sys/time.h>
#include <thread>
#include <unistd.h>
#include <opencv2/opencv.hpp>

const char *config_file = NULL;
int input_mode = 0;
int input_thread_running = 1;

#define MODULE_TAG "DMS_SAMPLE"

void usage() {
  std::cout << "Usage: ./test_dms_sdk config_path input_mode" << std::endl;
  std::cout
      << "       config_path: config file path, typically ./config/global.json"
      << std::endl;
  std::cout << "       input_mode: 1 for hardware, 2 for api" << std::endl;
}

void frameRateDump() {
  static long startTime = 0;
  static long count = 0;

  struct timeval tv = {0, 0};
  long curTime = 0;

  gettimeofday(&tv, NULL);
  curTime = tv.tv_sec * 1000 + tv.tv_usec / 1000;

  if (startTime == 0) {
    startTime = curTime;
  } else {
    count++;
    if (curTime > startTime + 1000) {
      std::cout << "+++++++++++++++++ frameRate="
                << count * 1000 / (curTime - startTime) << std::endl;
      startTime = 0;
      count = 0;
    }
  }
}

int ResultCallback(uint32_t type, void *data) {
  if (type == DMS_CALLBACK_DATA_OUT) {
    frameRateDump();
    std::cout << ">>>>>>>>>>>>>>>>>>>> data out begin <<<<<<<<<<<<<<<<<<<<<<<"
              << std::endl;
    DMSSDKOutput *protobuf_data = static_cast<DMSSDKOutput *>(data);
    DMSOutputProtocol::DMSSDKOutput *p = new DMSOutputProtocol::DMSSDKOutput();
    p->ParseFromArray(protobuf_data->meta, protobuf_data->meta_size);

    if (p->has_frame_id()) {
      int32_t frame_id = p->frame_id();
      std::cout << MODULE_TAG << " frame_id = " << frame_id << std::endl;
    }

    if (p->has_face_roi_result()) {
      std::cout << ">>>>>>>>>>>>>>>>>>>> has face roi <<<<<<<<<<<<<<<<<<<<<<<"
                << std::endl;
      int face_roi_left = p->face_roi_result().face_roi().left();
      int face_roi_right = p->face_roi_result().face_roi().right();
      int face_roi_top = p->face_roi_result().face_roi().top();
      int face_roi_bottom = p->face_roi_result().face_roi().bottom();
      std::cout << MODULE_TAG << " face_roi_left = " << face_roi_left
                << std::endl;
      std::cout << MODULE_TAG << " face_roi_right = " << face_roi_right
                << std::endl;
      std::cout << MODULE_TAG << " face_roi_top = " << face_roi_top
                << std::endl;
      std::cout << MODULE_TAG << " face_roi_bottom = " << face_roi_bottom
                << std::endl;
    }

    // if (p->has_face_direction_result()) {
      // std::cout
          // << ">>>>>>>>>>>>>>>>>>>> has face direction <<<<<<<<<<<<<<<<<<<<<<<"
          // << std::endl;
      // DMSOutputProtocol::FaceDirEnum face_dir =
          // p->face_direction_result().face_dir();
      // std::cout << MODULE_TAG << " face_dir = " << face_dir << std::endl;

      // if (p->face_direction_result().has_angle_rpy()) {
        // std::cout
            // << ">>>>>>>>>>>>>>>>>>>> has angle rpy <<<<<<<<<<<<<<<<<<<<<<<"
            // << std::endl;
        // float roll = p->face_direction_result().angle_rpy().roll();
        // std::cout << MODULE_TAG << " roll = " << roll << std::endl;
      // }
    // }

    if (p->has_face_feature_result()) {
      std::cout
          << ">>>>>>>>>>>>>>>>>>>> has face feature <<<<<<<<<<<<<<<<<<<<<<<"
          << std::endl;
      size_t p_sz = p->face_feature_result().feature_point_size();

      for (size_t i = 0; i < p_sz; ++i) {
        std::cout << MODULE_TAG << " i = " << i << ": ("
                  << p->face_feature_result().feature_point(i).x() << ", "
                  << p->face_feature_result().feature_point(i).y() << ")"
                  << std::endl;
      }
    }

    if (p->has_eye_result()) {
      std::cout << ">>>>>>>>>>>>>>>>>>>> has eye result <<<<<<<<<<<<<<<<<<<<<<<"
                << std::endl;
      DMSOutputProtocol::EyeStateEnum left_eye_state = p->eye_result().left();
      DMSOutputProtocol::EyeStateEnum right_eye_state = p->eye_result().right();
      std::cout << MODULE_TAG << " left_eye_state=" << left_eye_state
                << ", right_eye_state=" << right_eye_state << std::endl;
    }

    size_t e_sz = p->event_result_size();
    std::cout << MODULE_TAG << " e_sz= " << e_sz << std::endl;
    for (size_t i = 0; i < e_sz; ++i) {
        DMSOutputProtocol::EventResult event_item = p->event_result(i);
        DMSOutputProtocol::EventEnum enum_item = event_item.event();
      std::cout << MODULE_TAG << " i = " << i << " event = " << enum_item << std::endl;
    }

    size_t s_sz = p->state_result_size();
    std::cout << MODULE_TAG << " s_sz= " << s_sz << std::endl;
    for (size_t i = 0; i < s_sz; ++i) {
      std::cout << MODULE_TAG << " i = " << i
                << " state = " << p->state_result(i).state() << std::endl;
    }
    
    delete p;
    std::cout << ">>>>>>>>>>>>>>>>>>>> data out end <<<<<<<<<<<<<<<<<<<<<<<"
              << std::endl;
  }

  std::cout << std::endl << std::endl << std::endl;
  return 0;
}

void *feedImage(void *args) {
  DMSImage img;
  cv::Mat image = cv::imread("../data/100.png", CV_8UC1);
  img.data = image.data;
  //img.data = new uint8_t[1280 * 720];
  //memset(img.data, 0x80, 1280 * 720);
  img.width = 1280;
  img.height = 720;
  img.step = 1280;
  img.channel = 1;
  img.color_mode = DMS_IMAGE_COLOR_GRAYSCALE;

  std::cout << "start to feed Image, 10fps" << std::endl;
  while (input_thread_running) {
    img.timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(
                        std::chrono::system_clock::now().time_since_epoch())
                        .count();
    DMS_FeedImage(&img);
    std::cout << "Feed img, timestamp:" << img.timestamp << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }

  delete[] img.data;
  return NULL;
}

int main(int argc, char **argv) {
  if (argc != 3) {
    usage();
    return 0;
  }

  config_file = argv[1];
  input_mode = atoi(argv[2]);

  DMS_SetCallBack(DMS_CALLBACK_DATA_OUT, ResultCallback);
  if (DMS_Init(config_file, NULL) != DMS_OK) {
    std::cout << "DMS_Init Failed" << std::endl;
    return 0;
  }

  std::cout << "start process" << std::endl;
  if (input_mode == 2) {
    // api mode, need to feed image manually
    pthread_t tid;
    if (pthread_create(&tid, NULL, feedImage, NULL) < 0) {
      std::cout << "pthread_create failed, errno=" << errno << std::endl;
      return 0;
    }

    while (true) {
#if 0
      char ch;
      std::cin >> ch;
      if (ch == 'q') {
        break;
      } else {
  	    usleep(100000);
      }
#else
      usleep(100000);
#endif
    }

    // quit
    input_thread_running = 0;
    pthread_join(tid, NULL);
  } else if (input_mode == 1) {
    // hardware mode, feed image by sdk
    while (true) {
#if 0
      char ch;
      std::cin >> ch;
      if (ch == 'q') {
        break;
      } else {
  	    usleep(100000);
      }
#else
      usleep(100000);
#endif
    }
  }

  DMS_Fini();
  std::cout << "stop process" << std::endl;
  return 0;
}
