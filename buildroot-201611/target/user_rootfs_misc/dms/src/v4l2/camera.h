//
// Copyright 2017 Horizon Robotics.
//

#ifndef HAL_CAMERA_H_
#define HAL_CAMERA_H_

#include <cstddef>
#include <stdint.h>
#include <string>

class CameraImpl;

namespace HobotDMS {
namespace HAL {

// base params
struct camera_params_base {
  int frame_width;   // frame width
  int frame_height;  // frame height
  int fmt;   // required format
  virtual ~camera_params_base() {}
};

// for v4l2 camera
struct camera_params_v4l2 : public camera_params_base {
  std::string video_dev;  // v4l2 device node
};

class Camera {
 public:
  enum PropertyID { PROP_TIMESTAMP, PROP_AVR_LUMA, PROP_FRAME_COUNT };

  Camera();

  ~Camera();

  int init(camera_params_base *param);

  void fini();

  int open();

  void close();

  int getFrame(uint8_t *buf_y, size_t buf_y_size, uint8_t *buf_uv = NULL,
               size_t buf_uv_size = 0);

  int getProperty(int propID, void *p_val);

 private:
  CameraImpl *mp_impl;
};

}  // end of namespace HAL
}  // end of namespace HobotDMS

#endif  // HAL_CAMERA_H_
