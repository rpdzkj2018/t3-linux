#ifndef __V4L2_CAM_H__
#define __V4L2_CAM_H__
#include <condition_variable>
#include <cstring>
#include <list>
#include <memory>
#include <mutex>
#include <vector>
#include <poll.h>

#include "camera.h"

#define V4L2_IN_BUF_CNT 3

typedef struct in_buf_
{
    void *p_buf;
    int len;
} in_buf;

class CameraImpl {
 public:
  static CameraImpl *mpInstance;

  CameraImpl();

  ~CameraImpl();

  int Init(HobotDMS::HAL::camera_params_v4l2 *p_ctx);

  void Fini(void);

  int Open();

  int getFrame(uint8_t *buf_y, size_t buf_y_size, uint8_t *buf_uv = NULL,
               size_t buf_uv_size = 0);

  int getProperty(int propID, void *p_val);

  int Close();

 private:
  int m_fd;
  char m_video_dev[32];
  in_buf m_buffers[V4L2_IN_BUF_CNT];
  struct pollfd m_pollfds[1];
  uint64_t m_sync_ts;
};

#endif  //  __V4L2_CAM_H__

