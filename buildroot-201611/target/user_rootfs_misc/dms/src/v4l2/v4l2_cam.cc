#include <dlfcn.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <poll.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <errno.h>

#include "v4l2_cam.h"

#if 0
#define ERROR(fmt, ...)                                             \
  do {                                                              \
    fprintf(stderr, "ERROR:%s:%d: " fmt "\r\n", __func__, __LINE__, \
            ##__VA_ARGS__);                                         \
  } while (0)
#else
  #define ERROR printf
#endif


CameraImpl *CameraImpl::mpInstance = NULL;

CameraImpl::CameraImpl() : m_fd(-1), m_sync_ts(0) {
  m_video_dev[0] = '\0';
  printf("CLH CameraImpl");
}

CameraImpl::~CameraImpl() { this->Fini(); }

int CameraImpl::Init(HobotDMS::HAL::camera_params_v4l2 *p_ctx) {
  printf("CLH CameraImpl::Init +");
  if (p_ctx == nullptr || p_ctx->video_dev.empty()) {
    ERROR("V4L2 Init() parameter failed!\n");
    return -1;
  }

  strncpy(m_video_dev, p_ctx->video_dev.c_str(), 32);

  if (access(m_video_dev, F_OK) != 0) {
    ERROR("V4L2 video dev not exist: %s\n", m_video_dev);
    return -1;
  }

  printf("CLH CameraImpl::Init -");
  return 0;
};

void CameraImpl::Fini(void) { m_video_dev[0] = '\0'; };

int CameraImpl::Open()  // start
{
  printf("CLH CameraImpl::Open +");
  int ret = -1;

  if ((m_fd = open(m_video_dev, O_RDWR)) < 0) {
    ERROR("V4L2 open dev(%s) failed errno=%d!\n", m_video_dev, errno);
    return -1;
  }

  // enum the available format for reference
  struct v4l2_fmtdesc fmtdesc;
  fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  fmtdesc.index = 0;
  while ((ret = ioctl(m_fd, VIDIOC_ENUM_FMT, &fmtdesc)) != -1) {
    char *fmt = (char *)&fmtdesc.pixelformat;
    printf("desc:%s,fmt:%c%c%c%c\n", fmtdesc.description, fmt[0], fmt[1],
           fmt[2], fmt[3]);
    fmtdesc.index++;
  }

  // set format
  struct v4l2_format format;
  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  format.fmt.pix.width = 720;
  format.fmt.pix.height = 480;
  format.fmt.pix.field = V4L2_FIELD_NONE;
  format.fmt.pix.pixelformat = V4L2_PIX_FMT_NV61;
  ret = ioctl(m_fd, VIDIOC_S_FMT, &format);
  if (ret < 0) {
    ERROR("set format error, errno=%d\n", errno);
    this->Close();
    return -1;
  }

  // get format to confirm the set is correct
  if ((ret = ioctl(m_fd, VIDIOC_G_FMT, &format)) != -1) {
    char *fmt = (char *)&format.fmt.pix.pixelformat;
    printf("current size:%ux%u,fmt:%c%c%c%c\n", format.fmt.pix.width,
           format.fmt.pix.height, fmt[0], fmt[1], fmt[2], fmt[3]);
    printf("current field:%d\n", format.fmt.pix.field);
  } else {
    ERROR("set fmt failed, errno=%d\n", errno);
    this->Close();
    return -1;
  }

  // check the crop info
  struct v4l2_cropcap cropcap;
  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  ioctl(m_fd, VIDIOC_CROPCAP, &cropcap);
  printf("%d %d %d %d\n", cropcap.bounds.left, cropcap.bounds.top,
         cropcap.bounds.width, cropcap.bounds.height);
  printf("%d %d %d %d\n", cropcap.defrect.left, cropcap.defrect.top,
         cropcap.defrect.width, cropcap.defrect.height);

  struct v4l2_crop crop;
  crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  ioctl(m_fd, VIDIOC_G_CROP, &crop);
  printf("current crop:%d %d %d %d\n", crop.c.left, crop.c.top, crop.c.width,
         crop.c.height);

  struct v4l2_queryctrl qctrl;
  for (unsigned int id = V4L2_CID_BASE; id < V4L2_CID_LASTP1; id++) {
    qctrl.id = id;
    if (ioctl(m_fd, VIDIOC_QUERYCTRL, &qctrl) != 0)
      continue;

    printf("name:%s,def:%d,max:%d,min:%d,step:%d\n", qctrl.name,
           qctrl.default_value, qctrl.maximum, qctrl.minimum, qctrl.step);

    if (qctrl.type == V4L2_CTRL_TYPE_MENU) {
      struct v4l2_querymenu menu;
      menu.id = id;
      for (int idx = qctrl.minimum; idx < qctrl.maximum; idx++) {
        menu.index = idx;
        if (ioctl(m_fd, VIDIOC_QUERYMENU, &menu) == 0) {
          printf("  menuname:%s\n", menu.name);
        }
      }
    }
  }

  struct v4l2_requestbuffers req;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.count = V4L2_IN_BUF_CNT;
  req.memory = V4L2_MEMORY_MMAP;
  ret = ioctl(m_fd, VIDIOC_REQBUFS, &req);

  struct v4l2_buffer buf;
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;
  int i;
  for (i = 0; i < V4L2_IN_BUF_CNT; i++) {
    buf.index = i;
    ret = ioctl(m_fd, VIDIOC_QUERYBUF, &buf);
    m_buffers[i].p_buf = mmap(NULL, buf.length, PROT_READ | PROT_WRITE,
                              MAP_SHARED, m_fd, buf.m.offset);
    m_buffers[i].len = buf.length;
    printf("length:%d\n", buf.length);

    ret = ioctl(m_fd, VIDIOC_QBUF, &buf, &buf.type);
    if (ret < 0) {
      ERROR("VIDIOC_QBUF failed!\n");
      this->Close();
      return -1;
    }
  }

  int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  ioctl(m_fd, VIDIOC_STREAMON, &type);

  m_pollfds[0].fd = m_fd;
  m_pollfds[0].events = POLLIN | POLLERR;

  printf("V4L2 Open() successful\n");
  return 0;
};

int CameraImpl::getProperty(int propID, void *p_val) {
  switch (propID) {
    case HobotDMS::HAL::Camera::PROP_TIMESTAMP:
      *(int64_t *)p_val = m_sync_ts;
      return 0;
    case HobotDMS::HAL::Camera::PROP_AVR_LUMA:
      *(int *)p_val = 2050;
      return 0;
    default:
      return -1;
  }
};

int CameraImpl::Close() {
  if (m_fd >= 0) {
    close(m_fd);
    m_fd = -1;
  }

  int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  ioctl(m_fd, VIDIOC_STREAMOFF, &type);
  for (int i = 0; i < 3; i++) {
    munmap(m_buffers[i].p_buf, m_buffers[i].len);
  }
};

void RGB565ToGray(uint8_t *pInbuf, uint8_t *pOutbuf, uint32_t lines,
                  uint32_t line_len, uint32_t step, uint32_t line_offset) {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
  uint16_t *pixel;
  pixel = (uint16_t *)pInbuf;

  int i = 0, j = 0;
  int pad_line_end = step - line_offset - line_len;
  for (i = 0; i < lines; i++) {
    pOutbuf += line_offset;
    for (j = 0; j < line_len; j++) {
      red = (uint8_t)((*pixel & 0xF800) >> 11);
      green = (uint8_t)((*pixel & 0x07E0) >> 5);
      blue = (uint8_t)(*pixel & 0x001F);
      *pOutbuf = (uint8_t)((red * 299 + green * 587 + blue * 114) / 1000);
      pixel++;
      pOutbuf++;
    }
    pOutbuf += pad_line_end;
  }
}

int CameraImpl::getFrame(uint8_t *buf_y, size_t buf_y_size, uint8_t *buf_uv,
                         size_t buf_uv_size) {
  int ret = poll(m_pollfds, 1, 1000);
  if (ret < 0) {
    ERROR("poll err\n");
    return -1;
  }
  if (ret == 0) {
    printf("poll timeout\n");
    return -1;
  }
  if (buf_y_size < 640 * 480) {
    ERROR("Y buffer size too small: %d\n", buf_y_size);
    return -1;
  }
  if (m_pollfds[0].revents & POLLIN) {
    m_sync_ts = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::system_clock::now().time_since_epoch())
                    .count();


    struct v4l2_buffer buf;
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    ioctl(m_fd, VIDIOC_DQBUF, &buf);

    struct timeval old;
    gettimeofday(&old, NULL);
    RGB565ToGray((unsigned char *)m_buffers[buf.index].p_buf,
                 buf_y + 120 * 1280, 480, 640, 1280, 320);
    struct timeval now;
    gettimeofday(&now, NULL);
    printf(
        "RGB565 To GRAY time = %ld ms\n",
        (now.tv_sec - old.tv_sec) * 1000 + (now.tv_usec - old.tv_usec) / 1000);

    ioctl(m_fd, VIDIOC_QBUF, &buf);
  }
};
