#include <iostream>

#include "camera.h"
#include "v4l2_cam.h"

namespace HobotDMS {
namespace HAL {

Camera::Camera() { mp_impl = new CameraImpl(); }

Camera::~Camera() {
  if (!mp_impl)
    delete mp_impl;
}

int Camera::init(camera_params_base *param) {
  camera_params_v4l2 *params_raw_v4l2 =
      dynamic_cast<camera_params_v4l2 *>(param);
  if (!params_raw_v4l2)
    return -1;
  return mp_impl->Init(params_raw_v4l2);
}

void Camera::fini() { mp_impl->Fini(); }

int Camera::open() { return mp_impl->Open(); }

void Camera::close() {
  if (!mp_impl)
    return;
  mp_impl->Close();
}

int Camera::getFrame(uint8_t *buf_y, size_t buf_y_size, uint8_t *buf_uv,
                     size_t buf_uv_size) {
  return mp_impl->getFrame(buf_y, buf_y_size, buf_uv, buf_uv_size);
}

int Camera::getProperty(int propID, void *p_val) {
  return mp_impl->getProperty(propID, p_val);
}

}  // end of namespace HAL
}  // end of namespace HobotDMS
