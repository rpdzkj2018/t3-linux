#!/bin/sh
#set -e

if [ -d ./CameraUI ];then
	cd ./CameraUI
	make distclean 
	./makeCameraUI
	echo "=====build CameraUI success!!!======"
	cd ../
fi

if [ -d ./Launcher ];then
	cd ./Launcher
	make distclean 
	./makeLauncher
	echo "=====build Launcher success!!!======"
	cd ../
fi

if [ -d ./MediaUI ];then
	cd ./MediaUI
	make distclean 
	./makeMediaUI
	echo "=====build MediaUI success!!!======"
	cd ../
fi

echo "rebuildAll done!"
