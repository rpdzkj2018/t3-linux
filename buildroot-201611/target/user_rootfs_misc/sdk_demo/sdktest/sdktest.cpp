//SDKLIB_TEST have already defined by Makefile
#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <linux/fb.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <sdklog.h>
#include <time.h>
#include <cutils/log.h>
#include "aw_ini_parser.h"

#include "CameraDebug.h"
#include "hwdisp2.h"

#include "V4L2CameraDevice2.h"
#include "CallbackNotifier.h"
#include "PreviewWindow.h"
#include "CameraHardware2.h"

#include "audio_hal.h"

#ifndef CDX_V27
#include "log.h"
#endif

#include "vencoder.h"
#include "CdxMuxer.h"
#include "Rtc.h"
#include "StorageManager.h"
#include "DvrFactory.h"
#include "CameraFileCfg.h"
#include <sys/socket.h>
#include <sys/un.h>
#include "Fat.h"
#include "DebugLevelThread.h"
#include "DvrRecordManager.h"

#include "moudle/AutoMount.h"
#include "moudle/Display.h"
#include "moudle/Audio.h"

#include "sdktest.h"
#include <mcheck.h> 
#include "AutoMount.h"
#include <ls_ctrl.h>
#include "G2dApi.h"


//#define EVE_TEST
//#define DMS_TEST 1

#ifdef DMS_TEST

#include "WarnHandle.h"

uint8_t DmsDectectFlag=0;

uint8_t DmsPreviewDectect = 0;
uint8_t DmsPreviewData[1280*720];

#include "DMSSDKOutput.pb.h"
#include "hobot_dms_sdk.h"
#include "hobot-dms/base/dms_defines.h"

#include <chrono>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <pthread.h>
#include <string>
#include <sys/time.h>
#include <thread>
#include <unistd.h>
#include <opencv2/opencv.hpp>

#define MODULE_TAG "DMS_SAMPLE"
//#define IMG_PREVIEW_OUT 1 

#ifdef IMG_PREVIEW_OUT
#define DMS_OUT_TPYE  DMS_CALLBACK_IMG_OUT
#else
#define DMS_OUT_TPYE  DMS_CALLBACK_DATA_OUT
#endif


typedef enum EventEnum {
  EVENT_NONE = 0,
  EVENT_DDW_L = 1,
  EVENT_DDW_R = 2,
  EVENT_DDW_U = 3,
  EVENT_DDW_D = 4,
  EVENT_DFW = 5,
  EVENT_DYA = 6,
  EVENT_DCA = 7,
  EVENT_DSA = 8,
  EVENT_LDR = 9,
  EVENT_SIZE = 10
}EventEnum;

typedef enum ImageCheckEnum {
  IMAGE_CHECK_NORMAL = 0,
  IMAGE_CHECK_BLURRED = 1,
  IMAGE_CHECK_SHELTER = 2,
  IMAGE_CHECK_FREEZED = 3,
  IMAGE_CHECK_SIZE = 4
}ImageCheckEnum;

typedef enum FaceDirEnum {
  FACE_DIR_NONE = 0,
  FACE_DIR_FRONT = 1,
  FACE_DIR_LEFT = 2,
  FACE_DIR_RIGHT = 4,
  FACE_DIR_UP = 8,
  FACE_DIR_DOWN = 16
} FaceDirEnum;


sp < WarnHandle > pWarnHandleThread;


#endif

using namespace android;

#define TEST_CAMERA_ID 0


#ifdef EVE_TEST
#include "EveHandle.h"
sp < EveHandle > pEveHandleThread;
#endif

void testStop(int signo)
{
    ALOGD("oops! stop!!! %d\n",signo);
    _exit(0);
}

/*
mode 0:
	cameraId:	0,1,2,3,4,5,6,7,8,360
	if cameraId is 8,dont open /dev/video4~7    4of cvbsin
	if cameraId is 360,dont open /dev/video0~3 
mode 1:cameraId:	0,1,2,3,4,5,6,7,
arg1:camera number x
arg2->arg(x+1):cameraid1  cameraidx
*/
#define MAX_TEST_CAMERA 14
dvr_factory *dvrs[MAX_TEST_CAMERA];

typedef struct {
	int record;
	int preview;
}dvrstat_t;

dvrstat_t test_dvrstat[MAX_TEST_CAMERA];

/**************************************
Function:usr_datacb
Description:dataPtr has encoded data such as H264 or jpeg data
***************************************/
static FILE *pfileH264 = NULL;
status_t usr_datacb(int32_t msgType, char *dataPtr, int dalen, void *user)
{
	dvr_factory *p_dvr = (dvr_factory *) user;
#if 0
	if(pfileH264 == NULL){
		char bufferb[60]= {0};
		sprintf(bufferb,"/tmp/dvr.h264");
		pfileH264 = fopen(bufferb,"a");
	}
	
	if(pfileH264){		
		fwrite((void *)dataPtr,1,dalen,pfileH264);				
		fflush(pfileH264);
		//fclose(pfileH264);
	}
#endif
	
	return 0;
}
/**************************************
Function:
Description:
***************************************/
void usernotifyCallback(int32_t msgType, int32_t ext1, int32_t ext2, void *user)
{
	LOGE("msgType =%x-----data=%p-----%d)", msgType, user);
	RecordCamera *p_dvr = (RecordCamera*) user;

	if ((msgType & CAMERA_MSG_ERROR) == CAMERA_MSG_ERROR) {
		LOGE("(msgType =CAMERA_MSG_ERROR  code=%d)",ext1);

	}
	if ((msgType & CAMERA_MSG_DVR_NEW_FILE) == CAMERA_MSG_DVR_NEW_FILE) {
		//dvr_factory *p_dvr = (dvr_factory *) user;
		LOGE("(msgType =CAMERA_MSG_DVR_NEW_FILE camera %d idx =%d)", p_dvr->mCameraId, ext1);
	}

	if ((msgType & CAMERA_MSG_DVR_STORE_ERR) == CAMERA_MSG_DVR_STORE_ERR) {
		LOGE("msgType =CAMERA_MSG_DVR_STORE_ERR)");
		//dvr_factory *p_dvr = (dvr_factory *) user;
		if(p_dvr){
			p_dvr->storage_state = 0;
			test_dvrstat[p_dvr->mCameraId%30].record =0;
			dvrs[p_dvr->mCameraId%30]->stopRecord();
		}
	}
}

#ifdef EVE_TEST
HwDisplay* gEveDisp;
static void EveResultDisplayEn(int addr_phy, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
	//printf("dispResult [%d %d %d %d]\n",x, y, w, h);
	
    int screen_id=0;
	int ch_id = 1;
	int lyr_id = 0;

	if(gEveDisp == NULL){
		ALOGE("gEveDisp is NULL");
		return ;
	}
	
	struct view_info frame={1024-w,600-h,w,h};
	int video_layer0=gEveDisp->aut_hwd_layer_request(&frame,screen_id,ch_id,lyr_id);
	gEveDisp->aut_hwd_layer_sufaceview(video_layer0,&frame);

	struct src_info src={1280, 720, DISP_FORMAT_YUV420_SP_VUVU};
	gEveDisp->aut_hwd_layer_set_src(video_layer0,&src, (int)&addr_phy);
	
	struct view_info crop = {x,y,w,h};
	gEveDisp->aut_hwd_layer_set_rect(video_layer0,&crop);
	
	gEveDisp->aut_hwd_layer_set_zorder(video_layer0,6);
	gEveDisp->aut_hwd_layer_open(video_layer0);
	
}

static void EveResultDisplayDisable(int addr_phy, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
    int screen_id=0;
	int ch_id = 1;
	int lyr_id = 0;
	
	if(gEveDisp == NULL){
		ALOGE("gEveDisp is NULL");
		return ;
	}
	
	struct view_info frame={1024-w, 600-h ,w ,h};

	int video_layer0=gEveDisp->aut_hwd_layer_request(&frame,screen_id,ch_id,lyr_id);

	gEveDisp->aut_hwd_layer_close(video_layer0);
}
#endif

#ifdef DMS_TEST

#define  EVE_MAX(a,b)              (((a) > (b)) ? (a) : (b))
#define  EVE_MIN(a,b)              (((a) < (b)) ? (a) : (b))

void DrawRect(char* pNV21_y, char* pNV21_vu, int width, int height, int x, int y, int w, int h, int len_w)
{
    int linewidth = len_w;
    int sx = x;
    int sy = y;
    int ex = x + w;
    int ey = y + h;
    
	int i, j;
	unsigned char *pY  = (unsigned char *)pNV21_y;
	unsigned char *pVU = (unsigned char *)pNV21_vu;
	unsigned char *pSrc, *pSrc1;
	int halfline = (linewidth >> 1);
	int sty, endy, stx, endx;
    
    int colorR = 0, colorG = 0, colorB = 0;
    int colory = 255;   //GETY(colorR, colorG, colorB);
    int coloru = 0;     //GETU(colorR, colorG, colorB);
    int colorv = 255;   //GETV(colorR, colorG, colorB);
	
	//top line
	sty  = EVE_MAX(sy - halfline, 0);
	endy = EVE_MIN(sy + halfline, height - 1);
	for(i = sty; i <= endy; i++)
	{
		pSrc = (unsigned char *)(pVU + (i / 2)  * width);
		pSrc1 = (unsigned char *)(pY + i * width);
		for(j = sx; j <= ex; j ++)
		{
			pSrc1[j] = colory;
			pSrc[2*(j >> 1) + 0] = colorv;
			pSrc[2*(j >> 1) + 1] = coloru;
		}
	}

	//bottom line
	sty  = EVE_MAX(ey - halfline, 0);
	endy = EVE_MIN(ey + halfline, height - 1);
	for(i = sty; i <= endy; i++)
	{
		pSrc = (unsigned char *)(pVU + (i / 2)  * width);
		pSrc1 = (unsigned char *)(pY + i * width);
		for(j = sx; j <= ex; j ++)
		{
			pSrc1[j] = colory;
			pSrc[2*(j >> 1) + 0] = colorv;
			pSrc[2*(j >> 1) + 1] = coloru;
		}
	}

	//left line
	stx  = EVE_MAX(sx - halfline, 0);
	endx = EVE_MIN(sx + halfline, width - 1);
	sty  = EVE_MAX(sy - halfline, 0);
	endy = EVE_MIN(ey + halfline, height - 1);
	for(i = sty; i <= endy; i++)
	{
		pSrc = (unsigned char *)(pVU + (i / 2)  * width);
		pSrc1 = (unsigned char *)(pY + i * width);
		for(j = stx; j <= endx; j ++)
		{
			pSrc1[j] = colory;
			pSrc[2*(j >> 1) + 0] = colorv;
			pSrc[2*(j >> 1) + 1] = coloru;
		}
	}

	//right line
	stx  = EVE_MAX(ex - halfline, 0);
	endx = EVE_MIN(ex + halfline, width - 1);

	for(i = sty; i <= endy; i++)
	{
		pSrc = (unsigned char *)(pVU + (i / 2)  * width);
		pSrc1 = (unsigned char *)(pY + i * width);
		for(j = stx; j <= endx; j ++)
		{
			pSrc1[j] = colory;
			pSrc[2*(j >> 1) + 0] = colorv;
			pSrc[2*(j >> 1) + 1] = coloru;
		}
	}
}

typedef struct dmsflag{
	int faceflag;
	int pointflag;
	
}DmsflagStru;

struct FacePoint{
	int x;
	int y;
};

#define POINT_MAX 30
#define DETECT_LEVEL 1

int face_x,face_y,face_w,face_h;
FacePoint facep[POINT_MAX];
int facep_max=0;

int smoke_cnt=0,phone_cnt=0,eye_cnt;

DmsflagStru Dmsflag;



int ResultCallback(uint32_t type, void *data) {


	if (type == DMS_CALLBACK_DATA_OUT) {
		
		//printf(">>> ResultCallback >>> \n");
		
		DMSSDKOutput *protobuf_data = static_cast<DMSSDKOutput *>(data);
		DMSOutputProtocol::DMSSDKOutput *p = new DMSOutputProtocol::DMSSDKOutput();
		p->ParseFromArray(protobuf_data->meta, protobuf_data->meta_size);

		if (p->has_face_roi_result()) {
			Dmsflag.faceflag=1;
			face_x = p->face_roi_result().face_roi().left();
			face_y = p->face_roi_result().face_roi().right();
			face_w = p->face_roi_result().face_roi().top();
			face_h = p->face_roi_result().face_roi().bottom();
		}else{
			Dmsflag.faceflag=0;
		}

		if(p->has_face_feature_result()){
			
			facep_max=p->face_feature_result().feature_point_size();
			Dmsflag.pointflag=1;
			for(int i=0;i<facep_max;i++){
			 	facep[i].x=p->face_feature_result().feature_point(i).x();
			 	facep[i].y=p->face_feature_result().feature_point(i).y();
			
			}
		}else{
			Dmsflag.pointflag=0;
		}

		#if 1
		if (p->has_smoke_result()) {
			DMSOutputProtocol::SmokeStateEnum smoke_state = p->smoke_result().smoke_enum_state();
			//std::cout << MODULE_TAG << " smoke_state = " << smoke_state << std::endl;
			if(smoke_state == 0){
				if(smoke_cnt<DETECT_LEVEL){
					smoke_cnt++;
				}else{
					printf(">>>>> smoke  >>> \n");
					pWarnHandleThread->extPlayWarnSound(1);
				}
			}else{
				smoke_cnt=0;
			}
		}

		if (p->has_phone_result()) 
		{
			DMSOutputProtocol::PhoneStateEnum phone_state = p->phone_result().phone_enum_state();
			//std::cout << MODULE_TAG << " phone_state= " << phone_state << std::endl;
			if(phone_state == 0){	
				if(phone_cnt<DETECT_LEVEL){
					phone_cnt++;
				}else{
					printf(">>>>> phone  >>> \n");
					pWarnHandleThread->extPlayWarnSound(0);
				}
			}else{
				phone_cnt=0;
			}
		}


		/*
		if (p->has_eye_result()) 
		{
			DMSOutputProtocol::EyeStateEnum left_eye_state = p->eye_result().left();
     			 DMSOutputProtocol::EyeStateEnum right_eye_state = p->eye_result().right();
			//std::cout << MODULE_TAG << " phone_state= " << phone_state << std::endl;
			if(( left_eye_state == 1) || (right_eye_state == 1)){	
				if(eye_cnt<DETECT_LEVEL){
					eye_cnt++;
				}else{
					printf(">>>>> eye  >>> \n");
					pWarnHandleThread->extPlayWarnSound(0);
				}
			}else{
				eye_cnt=0;
			}
		}
		*/


		#else
		
		
		
		size_t e_sz = p->event_result_size();

		for (size_t i = 0; i < e_sz; ++i)
		{
			printf(">>> %d,%d >>>> \n",i,p->event_result(i).event());
			switch(p->event_result(i).event())
			{
				case EVENT_DDW_L:
				case EVENT_DDW_R:
				case EVENT_DDW_U:
				case EVENT_DDW_D:
					pWarnHandleThread->extPlayWarnSound(0);
				break;
				case EVENT_DFW:
					pWarnHandleThread->extPlayWarnSound(0);
				break;
				case EVENT_DYA:
					pWarnHandleThread->extPlayWarnSound(0);
				break;

				case EVENT_DCA:
					pWarnHandleThread->extPlayWarnSound(1);	
				break;

				case EVENT_DSA:
					pWarnHandleThread->extPlayWarnSound(1);
				break;

				case EVENT_LDR:
					pWarnHandleThread->extPlayWarnSound(0);
				break;

				default:
					break;
			}
		}
		#endif
		
		delete p;
	}

	
	if (type == DMS_CALLBACK_IMG_OUT) 
	{
		DmsPreviewDectect=1;
		DMSImage *dmspreview = (DMSImage *)data;
		memcpy(DmsPreviewData, dmspreview->data, 1280*720);
	}
  	return 0;
}

#endif



/**************************************
Function:
Description:
***************************************/
void userdataCallback(int32_t msgType, char *dataPtr, camera_frame_metadata_t * metadata,void *user)
{
	int i;
	
	dvr_factory *p_dvr = (dvr_factory *) user;
	V4L2BUF_t *pv4l2_buf = (V4L2BUF_t *) dataPtr;
	
	if(CAMERA_MSG_PREVIEW_FRAME == msgType){

    	/////////////////////////////////////////eve start//////////////////////////////////////
    		#ifdef EVE_TEST
    	
    		static int s_NoEveResultCounter = 0;
		if(0 == p_dvr->mCameraId){   //only use eve in camera0
			dma_mem_des_t* pEveMem = pEveHandleThread->getEveAllocMem();
			if(pEveMem->phy){
				pEveHandleThread->EveG2dScale((unsigned char *) pv4l2_buf->addrPhyY,pv4l2_buf->width, pv4l2_buf->height,
							(unsigned char *) pEveMem->phy, 640, 360);
				
				pEveHandleThread->loadPhyAddr(pEveMem->phy);
			}

			if(pEveHandleThread->mrltNum > 0){
				s_NoEveResultCounter = 0;
				EveResultDisplayEn(pv4l2_buf->addrPhyY,
						   pEveHandleThread->mEveResult[0].x, pEveHandleThread->mEveResult[0].y,
						   pEveHandleThread->mEveResult[0].w, pEveHandleThread->mEveResult[0].h);
			}else{
				s_NoEveResultCounter++;
				if(s_NoEveResultCounter > 10){   //detect is already disappear
					EveResultDisplayDisable(pv4l2_buf->addrPhyY,
						   pEveHandleThread->mEveResult[0].x, pEveHandleThread->mEveResult[0].y,
						   pEveHandleThread->mEveResult[0].w, pEveHandleThread->mEveResult[0].h);
				}
			}
			
			pEveHandleThread->DrawDetectRect((char*)pv4l2_buf->addrVirY, (char*)(pv4l2_buf->addrVirY+1280*720), 1280, 720);
			
			pEveHandleThread->CleanDetectRect();
			pthread_mutex_lock(&pEveHandleThread->mEvehandleMutex);
			pthread_cond_signal(&pEveHandleThread->mEvehandleCond);
			pthread_mutex_unlock(&pEveHandleThread->mEvehandleMutex);
			
		}
    		#endif
    	/////////////////////////////////////////eve end//////////////////////////////////////
    	
    	
    	/////////////////////////////////////////dms start//////////////////////////////////////
    		#ifdef DMS_TEST	

		if(0 == p_dvr->mCameraId && DmsDectectFlag==1)
		{

			//printf(">>>>> img input  >>> \n");

			static int framecount;
			
			//only use eve in camera0
			if(framecount < 2)
				framecount++;
			else
			{
				framecount=0;
				
				
				DMSImage img;
				img.data = (uint8_t*)pv4l2_buf->addrVirY;
				//img.data = new uint8_t[1280 * 720];
				//memset(img.data, 0x80, 1280 * 720);
				img.width = 1280;
				img.height = 720;
				img.step = 1280;
				img.channel = 1;
				img.color_mode = DMS_IMAGE_COLOR_GRAYSCALE;	
				img.timestamp = (pv4l2_buf->timeStamp)/10000;

				//printf(">>>>>  DMS_FeedImage %d >>> \n",img.timestamp);

				DMS_FeedImage(&img);
			}

		
			if(Dmsflag.faceflag==1){
				DrawRect((char*)pv4l2_buf->addrVirY, (char*)(pv4l2_buf->addrVirY+1280*720), 1280, 720, face_x, face_w, face_y-face_x, face_h-face_w, 1);
			}
			
			if(Dmsflag.pointflag==1){
				for(i=0;i<facep_max;i++){
					DrawRect((char*)pv4l2_buf->addrVirY, (char*)(pv4l2_buf->addrVirY+1280*720), 1280, 720, facep[i].x, facep[i].y, 4, 4, 1);
				}
			}
			
			if(DmsPreviewDectect==1){
				memcpy((uint8_t*)pv4l2_buf->addrVirY, DmsPreviewData, 1280*720);
				memset((uint8_t*)pv4l2_buf->addrVirY+1280*720,0x80,1280*360);
			}
			
		}
		#endif
		/////////////////////////////////////////dms end//////////////////////////////////////
	
	}
}

/**************************************
Function:
Description:data is from V4l2 captureThread,it's NV21 format YUV
***************************************/
void userdataCallbackTimestamp(nsecs_t timestamp, int32_t msgType, char *dataPtr, void *user)
{
	V4L2BUF_t *pbuf = (V4L2BUF_t *) (dataPtr );
}

int  RecordInit(dvr_factory *pdvr)
{
	int rt=0;

	rt = pdvr->recordInit();
	if (0 != rt) {
		ALOGE("pdvr->recordInit error\n");
		return -1;
	}
	else {
		ALOGD("pdvr->recordInit success\n");
	}	
	
	pdvr->startRecord();

	return 0;
}

void argsAnalysis(int argc, char *argv[], int *cameraId)
{
//======Analysis argv=========
	printf("argc = %d \r\n", argc);
	int cameranums;
	int i = 0;
	if (argc > 2) {

		cameranums = atoi(argv[1]);
		if((cameranums > MAX_TEST_CAMERA)||((cameranums < 1)))
		{
			printf("can not support %d,max support camera numbers %d \r\n",cameranums,MAX_TEST_CAMERA);
			exit(-1);
		}
		if(argc<cameranums+2)
		{
			printf("bad parm num %d can not support %d,max support camera numbers %d \r\n",argc,cameranums,MAX_TEST_CAMERA);
			exit(-1);
		}
		for(i=0;i<cameranums;i++){
			cameraId[i]  = atoi(argv[2+i]);
		}
		int j;
		for(i=0;i<cameranums;i++){
			if(cameraId[i]==360)
			{
				for(j=0;j<cameranums;j++){
					if((cameraId[j]==0)||(cameraId[j]==1)||(cameraId[j]==2)||(cameraId[j]==3))
					{
						printf("when use camera 360,can not use csi camera %d \r\n",cameraId[j]);
						exit(-1);
					}
				}
			}
			
			if(cameraId[i]==8)
			{
				for(j=0;j<cameranums;j++){
					if((cameraId[j]==4)||(cameraId[j]==5)||(cameraId[j]==6)||(cameraId[j]==7))
					{
						printf("when use camera 8,can not use cvbs camera %d \r\n",cameraId[j]);
						exit(-1);
					}
					
				}
			}
		}			

		
		printf("argv[1] =%s \r\n", argv[1]);
	}else
	{
char *shelp=" ---------------sdktest-------------------------\r\n \
cameraId:	0,1,2,3,4,5,6,7,8,360 \r\n \
if cameraId is 8,dont open /dev/video4~7    4of cvbsin \r\n \
if cameraId is 360,dont open /dev/video0~3 \r\n \
arg1:camera number x \r\n \
arg2->arg(x+1):cameraid1 ... cameraidx \r\n \
for example: \
\r\n";
			printf("%s",shelp);
			printf("/sdktest 1	0    	//for test video 0\r\n");
			printf("/sdktest 3  0 1 2 	//for test video 0,1,2 \r\n");
			printf("/sdktest 2  0  2 	//for test video 0,2 \r\n");
			printf("/sdktest 1	360  	//for test video 360(4 csi in)\r\n");
			printf("/sdktest 1	8    	//for test video 8(4 cvbs in)\r\n");
			printf(" ---------------sdktest-------------------------\r\n");
			exit(-1);
	}
}

#define HAVAAUDIO 1
#define RECORD_TEST 1
#define RECORD_new_delete_TEST 1


//#define AUDIO_CAPTURE_TEST 1
//#define BACKLIGHT_TEST 1

void testaudiocb(int32_t msgType,nsecs_t timestamp,int card,int device,
                                   char *dataPtr,int dsize,
                                   void* user)
{
	ALOGD("testaudiocb date=%p ,len=%d ,usrp=%p",dataPtr,dsize,user);
}
void testaudiohal()
{
	sp < AudioCapThread > audiocap = new AudioCapThread();
	sleep(1);
	int hdl= audiocap->reqAudioSource(testaudiocb,NULL);
	audiocap->startCapture(hdl);
	sleep(2);
	audiocap->stopCapture(hdl);
	audiocap->releaseAudioSource(hdl);
	sleep(2);
	audiocap->stopAudio();
	sleep(2);

}
int main(int argc, char *argv[])
{
	//setenv("MALLOC_TRACE", "/tmp/output", 1);
	//mtrace(); 
	sigset_t set1;
	sigemptyset(&set1);
	sigaddset(&set1 , SIGIO);   //block SIGIO signal in main thread,SIGIO will wakeup sleep
	pthread_sigmask(SIG_BLOCK , &set1 , NULL);
	sigpending(&set1);
	
	signal(SIGINT, testStop);
	int rt=0;
	
#ifdef AUDIO_CAPTURE_TEST
	while(1){
		printf("test audio hal \r\n");
		ALOGD("test audio hal \r\n");
		testaudiohal();
		sleep(2);
	}
#endif
	sdk_log_setlevel(6);
	int mode;
	int cameranums;
	//======moudle init===========
	int cameraId[MAX_TEST_CAMERA] = {0};
	int i;
	for(i=0;i<MAX_TEST_CAMERA;i++){
		cameraId[i] = -1;
	}
#ifdef BACKLIGHT_TEST
	//test bl ctrl
	lcd_blk_ctrl_init();
	for(int i=0;i<20;i++){
			lcd_blk_ctrl(i);
			sleep(1);
	}
	lcd_blk_ctrl_exit();
#endif

	/////////////////////dms start//////////////////////////
	#ifdef DMS_TEST
	
	pWarnHandleThread = new WarnHandle();
	
	DMS_SetCallBack(DMS_OUT_TPYE, ResultCallback);
	if (DMS_Init("/etc/global.json", NULL) != DMS_OK) {
		printf("DMS init ERROR \n");
	}
	DmsDectectFlag=1;
	#endif
	/////////////////////dms end//////////////////////////
	
	argsAnalysis(argc, argv, cameraId);
	cameranums = atoi(argv[1]);

	//start audio capture thread
	#ifdef HAVAAUDIO
	sp < AudioCapThread > audiocap = new AudioCapThread();
	#endif
	int hdl;
	int ret;

    #ifdef EVE_TEST
	pEveHandleThread = new EveHandle(1280,720);
	
	pEveHandleThread->EveInit(640,360);
	
	pEveHandleThread->EveG2dInit();
	
	pEveHandleThread->EveAllocMem(640*360*3/2);
	
	gEveDisp=HwDisplay::getInstance();
	#endif
    
	DvrRecordManagerInit();
	int testcnt=0;
retry:

	dvr_factory *tmp_dvr;
	for(i=0;i<cameranums;i++){
		int cur_camno = cameraId[i]%30;
		test_dvrstat[cur_camno].record =0;
		test_dvrstat[cur_camno].preview =0;
		
		dvrs[cur_camno] = new dvr_factory(cameraId[i]);
		tmp_dvr = dvrs[cur_camno];
#ifdef HAVAAUDIO		
		hdl= audiocap->reqAudioSource(tmp_dvr->__dvr_audio_data_cb_timestamp,(void *)tmp_dvr);
		tmp_dvr->setAudioOps(audiocap, hdl);
#endif
		tmp_dvr->SetDataCB(usr_datacb, tmp_dvr);
		tmp_dvr->setCallbacks(usernotifyCallback, userdataCallback, userdataCallbackTimestamp, tmp_dvr);
			int rt=0;
#ifdef 	RECORD_TEST

		rt = tmp_dvr->recordInit();
		if (0 != rt) {
			ALOGE("pdvr->recordInit error\n");
			return -1;
		}
		else {
			ALOGD("pdvr->recordInit success\n");
		}	
		tmp_dvr->startRecord();
		test_dvrstat[cur_camno].record = 1;
#endif
	}
   	//ppDvr = dvrs;

#ifdef 	RECORD_TEST
#ifndef RECORD_new_delete_TEST
	 AutoMountInit(dvrs[ cameraId[0]]);// only can regisister "ONLY ONE"  function   
#endif
#endif

	#if !defined(DISP_LIB_V20)
        DisplayInit(DISP_GET_CONFIG,dvrs[cameraId[0]%30]->mCameraId); 
	
	#endif

	for(i=0;i<cameranums;i++){
		int cur_camno = cameraId[i]%30;
		rt = dvrs[cur_camno]->start();
		if (rt < 0) {
			ALOGE("dvrs[%d]->start() error\n",i);
			return -1;
		}
	}

	//dvrs[0]->takePicture();   
	
	//======preview===========
	struct view_info vv = { 0, 0, 1024, 600 };
	int recordcnt=0;
#ifdef	RECORD_new_delete_TEST
	recordcnt=20;
#endif
	#ifdef 	RECORD_TEST
	while (1) 
	#endif
	{
		for(i=0;i<cameranums;i++){
			int cur_camno = cameraId[i]%30;
			if(cameranums == 1){
				ret = dvrs[cur_camno]->startPriview(vv);
				
				if(ret != 0){
					ALOGE("dvr startPreview error,maybe cam isn't online");
					return -1;
				}else
					test_dvrstat[cur_camno].preview =1;
				int slpret = sleep(5);
				dvrs[cur_camno]->stopPriview();
				test_dvrstat[cur_camno].preview =0;
				slpret = sleep(3);
				int type = dvrs[cur_camno]->getTVINSystemType();
				printf("sleep ret =%d,errno=%d tvin type =%d \n",slpret,errno,type);
			}else{
				ret = dvrs[cur_camno]->startPriview(vv);
				if(ret != 0){
					ALOGE("dvr startPreview error,maybe cam isn't inline");
					return -1;
				}
				ALOGV("preview camera %d for 7 sec",cameraId[cur_camno]);
				sleep(7);
				dvrs[cur_camno]->stopPriview();
				sleep(1);
			}
			
		}
		
		#ifdef	RECORD_new_delete_TEST
		sleep(1);
		recordcnt--;
		if(recordcnt<0){
			ALOGW("record test done break--------");
            #ifdef 	RECORD_TEST
			break;
            #endif
		}
		#endif
	}
	
	for(i=0;i<cameranums;i++){
		int cur_camno = cameraId[i]%30;
		if(test_dvrstat[cur_camno].preview ==1){
			rt = dvrs[cur_camno]->stopPriview();
			test_dvrstat[cur_camno].preview =0;
		}
		
		if(test_dvrstat[cur_camno].record == 1){
			test_dvrstat[cur_camno].record =0;
			dvrs[cur_camno]->stopRecord();
		}
		sleep(2);
		rt = dvrs[cur_camno]->stop();
		#ifdef HAVAAUDIO
		dvrs[cur_camno]->mAudioCap->releaseAudioSource(dvrs[cur_camno]->mAudioHdl);
		#endif
	}
	
	#ifdef EVE_TEST
	pEveHandleThread->EveUnInit();
	pEveHandleThread->EveG2dUnInit();
	pEveHandleThread->EveFreeMem();
    	#endif

	for(i=0;i<cameranums;i++){
		int cur_camno = cameraId[i]%30;
		delete dvrs[cur_camno];
		dvrs[cur_camno] = NULL;
	}

	
	#if !defined(DISP_LIB_V20)
		DisplayExit();
	#endif
	
	sleep(2);
	if(pfileH264){
		fclose(pfileH264);
	}
	if(testcnt >40)
	{
		testcnt =0;
	}else{
		testcnt++;
		printf("\n\n\n\n\n\n\n\n----------test---%d end \n\n\n\n\n\n\n\n",testcnt);
		ALOGV("\n\n\n\n\n\n\n\n----------test---%d end \n\n\n\n\n\n\n\n",testcnt);
		goto retry;
	}
#ifdef HAVAAUDIO
	audiocap->stopAudio();
#endif
	while(1){
		LOGE("sleep forever");
		sleep(1000);
	}
	return 0;
}
