
videoserv: $(BIN)
	echo -------------------
	$(CPP) -c ./background_video_player.cpp -o ./background_video_player.o -g $(CPPFLAGS) $(DEFINES)  $(CEDAR_MACORS) -DBGVIDEO_SER -I$(INC) -I$(SDK_PATH)/../shm/ -I$(SDK_PATH)/../dbusmedia/
	$(CPP) -c ./bg_video_serv_warper.cpp    -o ./bg_video_serv_warper.o    -g $(CPPFLAGS) $(DEFINES)  $(CEDAR_MACORS)  -DBGVIDEO_SER -I$(INC) -I$(SDK_PATH)/../shm/ -I$(SDK_PATH)/../dbusmedia/
	$(CPP)  -o ./video_serv  ./background_video_player.o ./bg_video_serv_warper.o -DBGVIDEO_SER -L./cedarx/$(CEDARX_LIB)/ -Wl,--no-undefined $(CEDAR_LINK_NEW) -lrt -lpthread -lz -lcrypto -lssl $(LIBS) -L$(SDK_PATH)  -lsdk  $(TESTLDFLAGS) -ltinyalsa -L$(SDK_PATH)/libs   -lawshm -lawcommediavideo
