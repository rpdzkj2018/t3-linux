#!/bin/sh
set -e

#if [ -d ./carbit ];then
#	cd ./carbit
#	make clean
#	make
#	cd ../
#fi

if [ -d ./AwH264VDecoder ];then
	echo compileing  AwH264VDecoder 
	cd ./AwH264VDecoder
	make clean
	make
	echo "=====build AwH264VDecoder success!!!======"
	cd ../
fi

if [ -d ./G2dDemo ];then
	cd ./G2dDemo
	make clean
	make
	echo "=====build G2dDemo success!!!======"
	cd ../
fi

if [ -d ./recordTest ];then
	cd ./recordTest
	make clean
	make
	echo "=====build recordTest success!!!======"
	cd ../
fi

if [ -d ./sdktest ];then
	cd ./sdktest
	make clean
	make
	echo "=====build sdktest success!!!======"
	cd ../
fi

if [ -d ./t7arecord ];then
	cd ./t7arecord
	make clean
	make
	echo "=====build t7arecord success!!!======"
	cd ../
fi

if [ -d ./videoservice ];then
	cd ./videoservice
	make clean
	make
	echo "=====build videoservice success!!!======"
	cd ../
fi

if [ -d ./videoproxy ];then
	cd ./videoproxy
	make clean
	make
	echo "=====build videoproxy success!!!======"
	cd ../
fi


if [ -d ./xplayerdemo ];then
	cd ./xplayerdemo
	make clean
	make
	echo "=====build xplayerdemo success!!!======"
	cd ../
fi

if [ -d ./xplayerdemoH264 ];then
	cd ./xplayerdemoH264
	make clean
	make
	echo "=====build xplayerdemoH264 success!!!======"
	cd ../
fi

echo "rebuildAll done!"