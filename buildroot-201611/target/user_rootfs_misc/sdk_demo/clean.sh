#!/bin/sh
set -e

#if [ -d ./carbit ];then
#	cd ./carbit
#	make clean
#	make
#	cd ../
#fi

if [ -d ./AwH264VDecoder ];then
	cd ./AwH264VDecoder
	make clean
	cd ../
fi

if [ -d ./G2dDemo ];then
	cd ./G2dDemo
	make clean
	cd ../
fi

if [ -d ./recordTest ];then
	cd ./recordTest
	make clean
	cd ../
fi

if [ -d ./sdktest ];then
	cd ./sdktest
	make clean
	cd ../
fi

if [ -d ./t7arecord ];then
	cd ./t7arecord
	make clean
	cd ../
fi

if [ -d ./videoservice ];then
	cd ./videoservice
	make clean
	cd ../
fi

if [ -d ./videoproxy ];then
	cd ./videoproxy
	make clean
	cd ../
fi


if [ -d ./xplayerdemo ];then
	cd ./xplayerdemo
	make clean
	cd ../
fi

if [ -d ./xplayerdemoH264 ];then
	cd ./xplayerdemoH264
	make clean
	cd ../
fi

echo "cleanall done!"