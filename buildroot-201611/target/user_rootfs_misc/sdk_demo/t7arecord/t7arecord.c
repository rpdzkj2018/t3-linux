#include <malloc.h>
#include <ctype.h>
#include <errno.h>
#include <alsa/asoundlib.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "aconfig.h"
#include "formats.h"

//  /t7arecord -D plug:mic-dvr -r 16000 -f S16_LE -c 2 /tmp/dvr.wav
//  /t7arecord /tmp/dvr.wav

typedef  struct _InputHWparams{
	snd_pcm_format_t format;
	unsigned int channels;
	unsigned int rate;
}InputHWparams;

static snd_pcm_uframes_t gChunkSize = 0;
static size_t gBitsPerFrame;

static void pcm_exit(snd_pcm_t *handle,int code) 
{
	if (handle)
		snd_pcm_close(handle);
}

/*
 *  当读写音频节点出现问题时，用此函数来恢复
 */
static int asound_recovery(snd_pcm_t *handle, int err)  
{  
   if (err == -EPIPE) {    /* under-run */ 
      err = snd_pcm_prepare(handle);  
 
   if (err < 0)  
      printf("Can't recovery from underrun, prepare failed: %s\n",  
         snd_strerror(err));  
      return 0;  
   } else if (err == -ESTRPIPE) {  
      while ((err = snd_pcm_resume(handle)) == -EAGAIN)  
         sleep(1);       /* wait until the suspend flag is released */ 
         if (err < 0) {  
            err = snd_pcm_prepare(handle);  
         if (err < 0)  
            printf("Can't recovery from suspend, prepare failed: %s\n",  
              snd_strerror(err));  
      }  
      return 0;  
   }else{
       printf("asound_recovery snd_pcm_prepare() return %s\n",snd_strerror(err));  
   }
   return err;  
}  

/*
 *  read function
 *  从音频节点读chunk_size大小的数据，放到以data为开始的缓存中
 *  返回值为成功读出的字节大小
 *  中途会等待100
 */
static ssize_t asound_pcm_read(snd_pcm_t *handle, char *data, size_t chunk_size)
{
	ssize_t r;
	size_t result = 0;
	size_t count = chunk_size;

	if (count != gChunkSize) {
		count = gChunkSize;
	}

	while (count > 0) {
		r = snd_pcm_readi(handle, data, count);
		if (r == -EAGAIN || (r >= 0 && (size_t)r < count)) { //haven't finish or r is in normal zone
			snd_pcm_wait(handle, 100);
		} else if (r < 0) {
			printf(("read error: %s"), snd_strerror(r));
			asound_recovery(handle, r);
            break;
		}
		if (r > 0) {
			result += r;
			count -= r;
			data += r * gBitsPerFrame / 8;
		}
	}
	return result;
}

/* write a WAVE-header */
static void begin_wave(int fd, size_t cnt,snd_pcm_t *handle, InputHWparams *pInputParam)
{
	WaveHeader h;
	WaveFmtBody f;
	WaveChunkHeader cf, cd;
	int bits;
	u_int tmp;
	u_short tmp2;

	/* WAVE cannot gPcmHandle greater than 32bit (signed?) int */
	if (cnt == (size_t)-2)
		cnt = 0x7fffff00;

	bits = 8;
	switch ((unsigned long) pInputParam->format) {
	case SND_PCM_FORMAT_U8:
		bits = 8;
		break;
	case SND_PCM_FORMAT_S16_LE:
		bits = 16;
		break;
	case SND_PCM_FORMAT_S32_LE:
        case SND_PCM_FORMAT_FLOAT_LE:
		bits = 32;
		break;
	case SND_PCM_FORMAT_S24_LE:
	case SND_PCM_FORMAT_S24_3LE:
		bits = 24;
		break;
	default:
		printf(("Wave doesn't support %s format..."), snd_pcm_format_name(pInputParam->format));
		pcm_exit(handle,EXIT_FAILURE);
	}
	h.magic = WAV_RIFF;
	tmp = cnt + sizeof(WaveHeader) + sizeof(WaveChunkHeader) + sizeof(WaveFmtBody) + sizeof(WaveChunkHeader) - 8;
	h.length = LE_INT(tmp);
	h.type = WAV_WAVE;

	cf.type = WAV_FMT;
	cf.length = LE_INT(16);

        if (pInputParam->format == SND_PCM_FORMAT_FLOAT_LE)
                f.format = LE_SHORT(WAV_FMT_IEEE_FLOAT);
        else
                f.format = LE_SHORT(WAV_FMT_PCM);
	f.channels = LE_SHORT(pInputParam->channels);
	f.sample_fq = LE_INT(pInputParam->rate);

	tmp2 = pInputParam->channels * snd_pcm_format_physical_width(pInputParam->format) / 8;
	f.byte_p_spl = LE_SHORT(tmp2);
	tmp = (u_int) tmp2 * pInputParam->rate;

	f.byte_p_sec = LE_INT(tmp);
	f.bit_p_spl = LE_SHORT(bits);

	cd.type = WAV_DATA;
	cd.length = LE_INT(cnt);

	if (write(fd, &h, sizeof(WaveHeader)) != sizeof(WaveHeader) ||
	    write(fd, &cf, sizeof(WaveChunkHeader)) != sizeof(WaveChunkHeader) ||
	    write(fd, &f, sizeof(WaveFmtBody)) != sizeof(WaveFmtBody) ||
	    write(fd, &cd, sizeof(WaveChunkHeader)) != sizeof(WaveChunkHeader)) {
		printf("write error\n");
		pcm_exit(handle,EXIT_FAILURE);
	}
}

static void end_wave(int fd,off64_t totalBuffCount)
{				/* only close output */
	WaveChunkHeader cd;
	off64_t length_seek;
	off64_t filelen;
	u_int rifflen;
	
	length_seek = sizeof(WaveHeader) +
		      sizeof(WaveChunkHeader) +
		      sizeof(WaveFmtBody);
	cd.type = WAV_DATA;
	cd.length = totalBuffCount > 0x7fffffff ? LE_INT(0x7fffffff) : LE_INT(totalBuffCount);
	filelen = totalBuffCount + 2*sizeof(WaveChunkHeader) + sizeof(WaveFmtBody) + 4;
	rifflen = filelen > 0x7fffffff ? LE_INT(0x7fffffff) : LE_INT(filelen);
	if (lseek64(fd, 4, SEEK_SET) == 4)
		write(fd, &rifflen, 4);
	if (lseek64(fd, length_seek, SEEK_SET) == length_seek)
		write(fd, &cd, sizeof(WaveChunkHeader));
	if (fd != 1)
		close(fd);
}


void initHwParams(snd_pcm_t *handle, snd_pcm_hw_params_t *params,InputHWparams *pInputParam)
{
    int err = snd_pcm_hw_params_any(handle, params);
	if (err < 0) {
		printf("Broken configuration for this PCM: no configurations available\n");
		pcm_exit(handle,EXIT_FAILURE);
	}

    err = snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED); //SND_PCM_ACCESS_RW_NONINTERLEAVED
	if (err < 0) {
		printf("Access type not available\n");
		pcm_exit(handle,EXIT_FAILURE);
	}
    
	err = snd_pcm_hw_params_set_format(handle, params, pInputParam->format);
	if (err < 0) {
		printf("Sample format non available\n");
		pcm_exit(handle,EXIT_FAILURE);
	}
    
	err = snd_pcm_hw_params_set_channels(handle, params, pInputParam->channels);
	if (err < 0) {
		printf("Channels count non available\n");
		pcm_exit(handle,EXIT_FAILURE);
	}

	err = snd_pcm_hw_params_set_rate_near(handle, params, &pInputParam->rate, 0);
	if (err < 0) {
		printf("snd_pcm_hw_params_set_rate_near error\n");
		pcm_exit(handle,EXIT_FAILURE);
	}

	err = snd_pcm_hw_params(handle, params);   //start to set hardware acturly
	if (err < 0) {
		printf("Unable to install hw params:\n");
		pcm_exit(handle,EXIT_FAILURE);
	}
}

snd_pcm_t *gPcmHandle;
static void signal_handler(int sig)
{
	printf("Aborted by signal %s...\n", strsignal(sig));
	if (gPcmHandle)
		snd_pcm_abort(gPcmHandle);
	if (sig == SIGABRT) {
		/* do not call snd_pcm_close() and abort immediately */
		gPcmHandle = NULL;
		pcm_exit(gPcmHandle,EXIT_FAILURE);
	}
	signal(sig, SIG_DFL);
}

int main(int argc, char *argv[])
{
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);
    signal(SIGABRT, signal_handler);
    
    //pcm_name="plug:mic-dvr";
    char *pcm_name = argv[1];
    char *SaveFilePath = argv[2];
	int err;
    
    InputHWparams paramIn;
    paramIn.channels=2;
    paramIn.format = SND_PCM_FORMAT_S16_LE;
    paramIn.rate = 16000;

    //printf("pcm_name=%s\n",pcm_name);
	err = snd_pcm_open(&gPcmHandle, pcm_name, SND_PCM_STREAM_CAPTURE, 0);
	if (err < 0) {
		printf("audio open error: %s", snd_strerror(err));
		return -1;
	}

    /* setup sound hardware */
    snd_pcm_hw_params_t *params;
	snd_pcm_hw_params_alloca(&params);  //how to release it???
    initHwParams(gPcmHandle, params, &paramIn); //init hardware param

    size_t chunkBytes;
	snd_pcm_hw_params_get_period_size(params, &gChunkSize, 0);
    size_t gBitsPerSample = snd_pcm_format_physical_width(paramIn.format);
	gBitsPerFrame = gBitsPerSample * paramIn.channels;
	chunkBytes = gChunkSize * gBitsPerFrame / 8;
    //printf("chunkBytes=%d gChunkSize=%d gBitsPerFrame=%d\n",chunkBytes, gChunkSize, gBitsPerFrame);
    //chunkBytes=8000 gChunkSize=2000 gBitsPerFrame=32
    char *audioBuff = (char *)malloc(chunkBytes);
	if (audioBuff == NULL) {
		printf("not enough memory\n");
		pcm_exit(gPcmHandle,EXIT_FAILURE);
	}
    //-------------------asound init finish----------------------
    
    char *name = SaveFilePath;	/* current filename */
    if(name != NULL){
        printf("save data to %s\n",name);
    }

    /* open a file to write */
    struct stat statbuf;
    if (!lstat(name, &statbuf)) {
        if (S_ISREG(statbuf.st_mode))  //if this is a dir,remove it
            remove(name);
    }
    
    int fd = open(name, O_WRONLY | O_CREAT, 0644);
	if (fd < 0) {
        printf("can not open save file\n");
        pcm_exit(gPcmHandle,EXIT_FAILURE);
	}
    
    off64_t rest;		/* number of bytes to capture */
    rest = 2147483648LL;
    begin_wave(fd, rest, gPcmHandle, &paramIn);  //setup sample header
    
    off64_t PcmSavedDataCount = 0;
    size_t read_ret;
    size_t write_ret;
    while(1){
        read_ret = asound_pcm_read(gPcmHandle, audioBuff, chunkBytes);
        write_ret = write(fd, audioBuff, read_ret);
        //printf("read_ret write_ret chunkBytes %d %d %d\n",read_ret,write_ret,chunkBytes);
        PcmSavedDataCount +=read_ret;  ////chunkBytes=8000 gChunkSize=2000 gBitsPerFrame=32
    }
    
    end_wave(fd,PcmSavedDataCount);    /* finish sample container */
    snd_pcm_hw_params_free(params);  
	snd_pcm_close(gPcmHandle);
	gPcmHandle = NULL;
	free(audioBuff);
	snd_config_update_free_global();
	pcm_exit(gPcmHandle,EXIT_SUCCESS);
	/* avoid warning */
	return EXIT_SUCCESS;
}
