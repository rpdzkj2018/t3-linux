#ifndef _DMAION_H_
#define _DMAION_H_
#ifdef __cplusplus
extern "C" {
#endif
typedef struct ION_BUFFER_NODE {
	int phy;		//phisical address
	int vir;		//virtual address
	int size;		//buffer size
	int dmabuf_fd;	//dma_buffer fd
	void* handle;		//alloc data handle
   /* struct aw_mem_list_head i_list;
    unsigned long phy;        //phisical address
    unsigned long vir;        //virtual address
    unsigned int size;        //buffer size
    unsigned int tee;      //
    unsigned long user_virt;//
    ion_fd_data_t fd_data;
    unsigned char alloc_by_ve;
    struct user_iommu_param iommu_buffer;
    */
} ion_buffer_node;

int dma_ion_open(int *ion_fd);
int dma_ion_close(int ion_fd);
int dma_ion_sync(int, int);
ion_buffer_node dma_ion_alloc(int ion_fd, int size);
void dma_ion_free(ion_buffer_node * ionbuf,int ionfd);
#ifdef __cplusplus
}
#endif
#endif
