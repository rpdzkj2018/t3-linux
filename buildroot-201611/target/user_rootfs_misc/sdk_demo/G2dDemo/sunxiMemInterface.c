#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#include "sunxiMemInterface.h"

int allocGetOpsParam(unsigned int memType, paramStruct_t * param_in, void * param_out)
{
	if(NULL == param_in){
		printf("allocGetOpsParam failed,param_in is null\n");
		return -1;
	}
	
	paramStruct_t* p =  param_in;
	
	switch(memType){
	case MEM_TYPE_DMA:
		break;	
#if 0	
	case MEM_TYPE_CDX_NEW:
		p->ops = MemAdapterGetOpsS();
		if(p->ops == NULL)
			return -1;
		break;
#endif

	default:
		printf("allocGetOpsParam: can't find memType=%d\n",memType);
		break;
	}

	return SUCCESS;
}

int allocOpen(unsigned int memType, paramStruct_t * param_in, void * param_out)
{
	int ret = FAIL;
	if(NULL == param_in){
		printf("allocOpen failed,param_in is null\n");
		return -1;
	}
	
	paramStruct_t* p =  param_in;
	switch(memType){
	case MEM_TYPE_DMA:
		ret = dma_ion_open(&p->Ionfd);
		break;	
#if 0		
	case MEM_TYPE_CDX_NEW:
		if(NULL != p->ops){
			ret = CdcMemOpen((struct ScMemOpsS *)p->ops);
		}else{
			printf("allocOpen failed,p->ops null\n");
		}
		break;
#endif		
	default:
		printf("allocOpen: can't find memType=%d\n",memType);
		break;
	}

	return ret;
}

int allocClose(unsigned int memType, paramStruct_t * param_in, void * param_out)
{
	int ret = FAIL;
	if(NULL == param_in){
		printf("allocClose failed,param_in is null\n");
		return -1;
	}
	
	paramStruct_t* p =  param_in;
	switch(memType){
	case MEM_TYPE_DMA:
		ret = dma_ion_close(p->Ionfd);
		break;	

#if 0		
	case MEM_TYPE_CDX_NEW:
		if(NULL != p->ops){
			CdcMemClose((struct ScMemOpsS *)p->ops);
			p->ops = NULL;
			ret = SUCCESS;
		}else{
			printf("allocClose failed,p->ops null\n");
		}
		break;
#endif		
	default:
		printf("allocClose: can't find memType=%d\n",memType);
		break;
	}
	return ret;
}

int allocAlloc(unsigned int memType, paramStruct_t * param_in, void * param_out)
{
	int ret = FAIL;
	if(NULL == param_in){
		printf("allocAlloc failed,input param is null\n");
		return ret;
	}
	
	paramStruct_t* p = param_in;
	switch(memType){
	case MEM_TYPE_DMA:
		p->ion_buffer = dma_ion_alloc(p->Ionfd, p->size);
		p->vir = p->ion_buffer.vir;
		p->phy = p->ion_buffer.phy;
		ret = 0;
		break;	
#if 0		
	case MEM_TYPE_CDX_NEW:
		p->vir = (int)CdcMemPalloc((struct ScMemOpsS *)p->ops, p->size, NULL, NULL);
		p->phy = (int)CdcMemGetPhysicAddressCpu((struct ScMemOpsS *)p->ops, (void*)p->vir);
		ret = 0;
		break;
#endif

	default:
		printf("allocAlloc: can't find memType=%d\n",memType);
		break;
	}

	return ret;
}

void allocFree(unsigned int memType, paramStruct_t * param_in, void * param_out)
{
	if(NULL == param_in){
		printf("allocFree failed,input param is null\n");
		return;
	}
	paramStruct_t* p = param_in;
	switch(memType){
	case MEM_TYPE_DMA:
		dma_ion_free(&p->ion_buffer,p->Ionfd);
		memset(&p->ion_buffer,0,sizeof(p->ion_buffer));
		p->Ionfd = -1;
		break;	

#if 0		
	case MEM_TYPE_CDX_NEW:
		CdcMemPfree((struct ScMemOpsS *)p->ops, (void*)p->vir, NULL, NULL);
		break;
#endif

	default:
		printf("allocFree: can't find memType=%d\n",memType);
		break;
	}
}

int allocVir2phy(unsigned int memType, paramStruct_t * param_in, void * param_out)
{
	int ret = FAIL;
	if(NULL == param_in){
		printf("allocVir2phy failed,input param is null\n");
		return -1;
	}
	
	paramStruct_t* p = param_in;
	switch(memType){
	case MEM_TYPE_DMA:
		break;	

#if 0		
	case MEM_TYPE_CDX_NEW:
		p->phy = (int)CdcMemGetPhysicAddressCpu((struct ScMemOpsS *)p->ops, (void*)p->vir);
		break;
#endif        

	default:
		printf("allocVir2phy: can't find memType=%d\n",memType);
		break;
	}
	
	return ret;
}

//warning !!!below function no test
int allocPhy2vir(unsigned int memType, paramStruct_t * param_in, void * param_out)
{
	int ret = FAIL;
	if(NULL == param_in){
		printf("allocPhy2vir failed,input param is null\n");
		return -1;
	}
	
	paramStruct_t* p = param_in;
	switch(memType){
	case MEM_TYPE_DMA:
		break;	

#if 0		
	case MEM_TYPE_CDX_NEW:
		p->vir = (int)CdcMemGetVirtualAddressCpu((struct ScMemOpsS *)p->ops, (void*)p->phy);
		break;
#endif

	default:
		printf("allocPhy2vir: can't find memType=%d\n",memType);
		break;
	}

	return ret;
}

void flushCache(unsigned int memType, paramStruct_t * param_in, void * param_out)
{
	if(NULL == param_in){
		printf("flushCache failed,input param is null\n");
		return;
	}
	paramStruct_t* p = param_in;
	switch(memType){
	case MEM_TYPE_DMA:
		dma_ion_sync(p->Ionfd, p->ion_buffer.dmabuf_fd);
		break;	

#if 0		
	case MEM_TYPE_CDX_NEW:
		CdcMemFlushCache((struct ScMemOpsS *)p->ops, (void*)p->vir, p->size);
		break;
#endif

	default:
		printf("flushCache: can't find memType=%d\n",memType);
		break;
	}
}
