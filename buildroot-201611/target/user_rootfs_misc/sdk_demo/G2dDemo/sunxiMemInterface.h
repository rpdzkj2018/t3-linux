#ifndef __SUNXI_MEM_INTERFACE_H
#define __SUNXI_MEM_INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DmaIon.h"

#define FAIL -1
#define SUCCESS 0

/* This struct is for many platform interface
 *
*/

#define MEM_TYPE_DMA 1

typedef struct paramStruct {
	void* ops;
	unsigned int memType;
	unsigned int phy;
	unsigned int vir;
	unsigned int size;
	int Ionfd;
	ion_buffer_node ion_buffer;
} paramStruct_t;

int allocGetOpsParam(unsigned int memType, paramStruct_t * param_in,void * param_out);
int allocOpen(unsigned int memType, paramStruct_t * param_in,void * param_out);
int allocClose(unsigned int memType, paramStruct_t * param_in,void * param_out);
int allocAlloc(unsigned int memType, paramStruct_t * param_in,void * param_out);
void allocFree(unsigned int memType, paramStruct_t * param_in, void * param_out);
int allocVir2phy(unsigned int memType, paramStruct_t * param_in, void * param_out);
int allocPhy2vir(unsigned int memType, paramStruct_t * param_in, void * param_out);
void flushCache(unsigned int memType, paramStruct_t * param_in, void * param_out);
#ifdef __cplusplus
}
#endif

#endif