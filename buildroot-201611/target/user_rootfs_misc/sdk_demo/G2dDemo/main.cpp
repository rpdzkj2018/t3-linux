/***************************************
FileName:
Copyright:
Author:
Description:
***************************************/
//#include "ContentDisplay.h"
//#include "CsiCameraMplane.h"
#include "hwdisp2.h"
#include "sunxiMemInterface.h"
#include <sys/time.h>

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "G2dApi.h"

//是否退出程序标志位
bool g_bQuitCapPro = false;

//ctrl+c消息捕获函数
void CtrlcMsgCb(int s)
{
    printf("===Caught Signal %d\n", s);
    g_bQuitCapPro = true;
}

int InitSignalCtrlC()
{
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = CtrlcMsgCb;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
}

/************Macro*********************/

/************Variable******************/

/************Function******************/
paramStruct_t m_DispMemOps;
paramStruct_t m_DispMemOps_half0;
paramStruct_t m_DispMemOps_half1;


paramStruct_t m_DispMemOps0;
paramStruct_t m_DispMemOps1;
paramStruct_t m_DispMemOps2;
paramStruct_t m_DispMemOps3;



int m_iVideoLayer0;
int ihalfWidth=2560 ,ihalfHeight=720 ;
int iWidth=2560 ,iHeight=1440 ;

int iSubWidth=1280 ,iSubHeight=720 ;

int ReadPicFileContent(char *pPicPath,paramStruct_t*pops,int w,int h)
{
    int iRet = 0;

    iRet = allocOpen(MEM_TYPE_DMA, pops, NULL);
    if (iRet < 0) {
        printf("ion_alloc_open failed\r\n");
        return iRet;
    }
    pops->size = w * h * 3 / 2;
    iRet = allocAlloc(MEM_TYPE_DMA, pops, NULL);
    if(iRet < 0) {
        printf("allocAlloc failed\r\n");
        return iRet;
    }

    FILE *fpff = fopen(pPicPath, "rb");
    if(NULL == fpff) {
        fpff = fopen(pPicPath, "rb");
        if(NULL == fpff) {
            printf("fopen %s ERR \r\n", pPicPath);
            allocFree(MEM_TYPE_DMA, pops, NULL);
            return -1;
        } else {
            printf("fopen %s OK \r\n", pPicPath);
            fread((void *)pops->vir, 1, w * h * 3 / 2, fpff);
            fclose(fpff);
        }
    } else {
        printf("fopen %s OK \r\n", pPicPath);
        fread((void *)pops->vir, 1, w * h * 3 / 2, fpff);
        fclose(fpff);
    }
	flushCache(MEM_TYPE_DMA,pops, NULL);

    return 0;
}
int WritePicFileContent(char *pPicPath,paramStruct_t*pops,int w,int h)

{
    int iRet = 0;
	printf("WritePicFileContent w=%d h=%d size=%d \r\n",w,h,w * h * 3 / 2);
	flushCache(MEM_TYPE_DMA,pops, NULL);

    FILE *fpff = fopen(pPicPath, "wb");
    if(NULL == fpff) {
        fpff = fopen(pPicPath, "wb");
        if(NULL == fpff) {
            printf("fopen %s ERR \r\n", pPicPath);
            allocFree(MEM_TYPE_DMA, pops, NULL);
            return -1;
        } else {
            printf("fopen %s OK \r\n", pPicPath);
            fwrite((void *)pops->vir, 1, w * h * 3 / 2, fpff);
            fclose(fpff);
        }
    } else {
        printf("fopen %s OK \r\n", pPicPath);
        fwrite((void *)pops->vir, 1, w * h * 3 / 2, fpff);
        fclose(fpff);
    }
	

    return 0;
}


int allocPicMem(paramStruct_t*pops,int size)
{
    int iRet = 0;

    iRet = allocOpen(MEM_TYPE_DMA, pops, NULL);
    if (iRet < 0) {
        printf("ion_alloc_open failed\r\n");
        return iRet;
    }
    pops->size =size;
    iRet = allocAlloc(MEM_TYPE_DMA, pops, NULL);
    if(iRet < 0) {
        printf("allocAlloc failed\r\n");
        return iRet;
    }

    return 0;
}
int freePicMem(paramStruct_t*pops)
{
    int iRet = 0;

	allocFree(MEM_TYPE_DMA, pops, NULL);

    return 0;
}

char *pPicPath0="/tmp/video0.yuv";
char *pPicPath1="/tmp/video1.yuv";
char *pPicPath2="/tmp/video2.yuv";
char *pPicPath3="/tmp/video3.yuv";

char *pcomph0PicPath0="/tmp/chvideo0.yuv";
char *pcomph1PicPath1="/tmp/chvideo1.yuv";

char *pcompPicPath0="/tmp/cvideo.yuv";


char *pdPicPath0="/tmp/dvideo0.yuv";
char *pdPicPath1="/tmp/dvideo1.yuv";
char *pdPicPath2="/tmp/dvideo2.yuv";
char *pdPicPath3="/tmp/dvideo3.yuv";
char *pdcompPicPath0="/tmp/dcvideo.yuv";








int main(int argc, char *argv[])
{
	if(argc <2)
	{
		printf("arc=%d \r\n",argc);
		exit(-1);
	}
    InitSignalCtrlC();
	
	printf("arc=%d  testid=%s\r\n",argc,argv[1]);

	int testid=atoi(argv[1]);
	
	struct timeval t1,t2;


		if(5 == testid) {//4in 1 compose
		//read two file to sunximem
		ReadPicFileContent(pPicPath0,&m_DispMemOps0,iSubWidth,iSubHeight);

		//reroll back
		WritePicFileContent("/tmp/ck.yuv",&m_DispMemOps0,iSubWidth,iSubHeight);
		
		//alloc compse buff
		allocPicMem(&m_DispMemOps,iWidth*iHeight*3/2);

		//compose
		int g2dfd=g2dInit();
		
		t1.tv_sec = t1.tv_usec = 0;
	    t2.tv_sec = t2.tv_usec = 0;
		printf("start rotate 180 \r\n");
		gettimeofday(&t1, NULL);
		
		//return nsecs_t(t.tv_sec)*1000000000LL + nsecs_t(t.tv_usec)*1000LL;
		
		//g2dRotate(int g2dHandle,g2dRotateAngle angle, unsigned char *src, int src_width, int src_height, unsigned char *dst, int dst_width, int dst_height)
		g2dRotate(g2dfd,G2D_ROTATE180, (unsigned char *)m_DispMemOps0.phy, iSubWidth, iSubHeight, (unsigned char *)m_DispMemOps.phy,iSubWidth, iSubHeight);

		gettimeofday(&t2, NULL);
		printf("end trans start rotate 180 time =%lld \r\n",int64_t(t2.tv_sec)*1000000000LL + int64_t(t2.tv_usec)*1000LL-(int64_t(t1.tv_sec)*1000000000LL + int64_t(t1.tv_usec)*1000LL));
		
		//save compose buffer
		WritePicFileContent(pcompPicPath0,&m_DispMemOps,iSubWidth,iSubHeight);
		
		g2dUnit(g2dfd);
		
		freePicMem(&m_DispMemOps);
		freePicMem(&m_DispMemOps0);

		
    }else if(3 == testid) {//4in 1 compose
		//read two file to sunximem
		ReadPicFileContent(pPicPath0,&m_DispMemOps0,iSubWidth,iSubHeight);
		ReadPicFileContent(pPicPath1,&m_DispMemOps1,iSubWidth,iSubHeight);
		ReadPicFileContent(pPicPath2,&m_DispMemOps2,iSubWidth,iSubHeight);
		ReadPicFileContent(pPicPath3,&m_DispMemOps3,iSubWidth,iSubHeight);


		//reroll back
		WritePicFileContent("/tmp/ck.yuv",&m_DispMemOps0,iSubWidth,iSubHeight);
		
		//alloc compse buff
		allocPicMem(&m_DispMemOps,iWidth*iHeight*3/2);

		//compose
		int g2dfd=g2dInit();
		
		t1.tv_sec = t1.tv_usec = 0;
	    t2.tv_sec = t2.tv_usec = 0;
		printf("start trans 4x 720p to 1x 2560x1440 compose \r\n");
		gettimeofday(&t1, NULL);
		
		//return nsecs_t(t.tv_sec)*1000000000LL + nsecs_t(t.tv_usec)*1000LL;
		
		//int g2dClip(int g2dHandle,void* psrc, int src_w, int src_h, int src_x, int src_y, int width, int height, void* pdst, int dst_w, int dst_h, int dst_x, int dst_y)
		
		g2dClip(g2dfd,(void *)m_DispMemOps0.phy, iSubWidth, iSubHeight, 0, 0, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps.phy, iWidth, iHeight, 0, 0);
		g2dClip(g2dfd,(void *)m_DispMemOps0.phy, iSubWidth, iSubHeight, 0, 0, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps.phy, iWidth, iHeight, iSubWidth, 0);
		g2dClip(g2dfd,(void *)m_DispMemOps0.phy, iSubWidth, iSubHeight, 0, 0, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps.phy, iWidth, iHeight, 0, iSubHeight);
		g2dClip(g2dfd,(void *)m_DispMemOps0.phy, iSubWidth, iSubHeight, 0, 0, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps.phy, iWidth, iHeight, iSubWidth, iSubHeight);

		gettimeofday(&t2, NULL);
		printf("end trans 4x 720p to 1x 2560x1440 compose time =%lld \r\n",int64_t(t2.tv_sec)*1000000000LL + int64_t(t2.tv_usec)*1000LL-(int64_t(t1.tv_sec)*1000000000LL + int64_t(t1.tv_usec)*1000LL));
		//return nsecs_t(t.tv_sec)*1000000000LL + nsecs_t(t.tv_usec)*1000LL;
		
		//save compose buffer
		WritePicFileContent(pcompPicPath0,&m_DispMemOps,iWidth,iHeight);
		
		g2dUnit(g2dfd);
		
		freePicMem(&m_DispMemOps);
		freePicMem(&m_DispMemOps0);
		freePicMem(&m_DispMemOps1);
		freePicMem(&m_DispMemOps2);
		freePicMem(&m_DispMemOps3);

		
    }else 
    if(4 == testid) { //create 2560x720
		//read two file to sunximem
		ReadPicFileContent(pPicPath0,&m_DispMemOps0,iSubWidth,iSubHeight);
		ReadPicFileContent(pPicPath1,&m_DispMemOps1,iSubWidth,iSubHeight);

		//reroll back
		WritePicFileContent("/tmp/ck.yuv",&m_DispMemOps0,iSubWidth,iSubHeight);
		
		//alloc compse buff
		allocPicMem(&m_DispMemOps,ihalfWidth*ihalfHeight*3/2);

		//compose
		int g2dfd=g2dInit();
		
		//int g2dClip(int g2dHandle,void* psrc, int src_w, int src_h, int src_x, int src_y, int width, int height, void* pdst, int dst_w, int dst_h, int dst_x, int dst_y)
		
		t1.tv_sec = t1.tv_usec = 0;
	    t2.tv_sec = t2.tv_usec = 0;
		printf("start trans 2x 1280x720  to 2560x720 compose \r\n");
		gettimeofday(&t1, NULL);
		g2dClip(g2dfd,(void *)m_DispMemOps0.phy, iSubWidth, iSubHeight, 0, 0, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps.phy, ihalfWidth, ihalfHeight, 0, 0);
		g2dClip(g2dfd,(void *)m_DispMemOps0.phy, iSubWidth, iSubHeight, 0, 0, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps.phy, ihalfWidth, ihalfHeight, iSubWidth, 0);
		gettimeofday(&t2, NULL);
		printf("end trans 2x 1280x720  to 2560x720  time =%lld \r\n",int64_t(t2.tv_sec)*1000000000LL + int64_t(t2.tv_usec)*1000LL-(int64_t(t1.tv_sec)*1000000000LL + int64_t(t1.tv_usec)*1000LL));
		
		
		//save compose buffer
		WritePicFileContent(pcompPicPath0,&m_DispMemOps,ihalfWidth,ihalfHeight);
		
		g2dUnit(g2dfd);
		
		freePicMem(&m_DispMemOps);
		freePicMem(&m_DispMemOps0);
		freePicMem(&m_DispMemOps1);
		
    }else 
    if(1 == testid) {
		//read two file to sunximem
		ReadPicFileContent(pcomph0PicPath0,&m_DispMemOps_half0,ihalfWidth,ihalfHeight);
		ReadPicFileContent(pcomph1PicPath1,&m_DispMemOps_half1,ihalfWidth,ihalfHeight);

		//reroll back
		WritePicFileContent("/tmp/ck.yuv",&m_DispMemOps_half0,ihalfWidth,ihalfHeight);
		
		//alloc compse buff
		allocPicMem(&m_DispMemOps,iWidth*iHeight*3/2);

		//compose
		int g2dfd=g2dInit();
		
		//int g2dClip(int g2dHandle,void* psrc, int src_w, int src_h, int src_x, int src_y, int width, int height, void* pdst, int dst_w, int dst_h, int dst_x, int dst_y)
		t1.tv_sec = t1.tv_usec = 0;
	    t2.tv_sec = t2.tv_usec = 0;
		printf("start trans 2x 2560x720 to 1x 2560x1440 compose \r\n");
		gettimeofday(&t1, NULL);
		//below cannot support
		//g2dClip(g2dfd,(void *)m_DispMemOps_half0.phy, ihalfWidth, ihalfHeight, 0, 0, ihalfWidth, ihalfHeight, \
		//				(void *)m_DispMemOps.phy, iWidth, iHeight, 0, 0);
		//g2dClip(g2dfd,(void *)m_DispMemOps_half1.phy, ihalfWidth, ihalfHeight, 0, 0, ihalfWidth, ihalfHeight, \
		//						(void *)m_DispMemOps.phy, iWidth, iHeight, 0, ihalfHeight/* offset h */);

		g2dClip(g2dfd,(void *)m_DispMemOps_half0.phy, ihalfWidth, ihalfHeight, 0, 0, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps.phy, iWidth, iHeight, 0, 0);
		g2dClip(g2dfd,(void *)m_DispMemOps_half0.phy, ihalfWidth, ihalfHeight, iSubWidth, 0, iSubWidth, iSubHeight, \
								(void *)m_DispMemOps.phy, iWidth, iHeight, iSubWidth, 0/* offset h */);
		g2dClip(g2dfd,(void *)m_DispMemOps_half1.phy, ihalfWidth, ihalfHeight, 0, 0, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps.phy, iWidth, iHeight, 0, iSubHeight);
		g2dClip(g2dfd,(void *)m_DispMemOps_half1.phy, ihalfWidth, ihalfHeight, iSubWidth, 0, iSubWidth, iSubHeight, \
								(void *)m_DispMemOps.phy, iWidth, iHeight, iSubWidth, iSubHeight/* offset h */);		
		gettimeofday(&t2, NULL);
		printf("end trans 2x 2560x720 to 1x 2560x1440 compose  time =%lld \r\n",int64_t(t2.tv_sec)*1000000000LL + int64_t(t2.tv_usec)*1000LL-(int64_t(t1.tv_sec)*1000000000LL + int64_t(t1.tv_usec)*1000LL));
		
		
		//save compose buffer
		WritePicFileContent(pcompPicPath0,&m_DispMemOps,iWidth,iHeight);
		
		g2dUnit(g2dfd);
		
		freePicMem(&m_DispMemOps);
		freePicMem(&m_DispMemOps_half0);
		freePicMem(&m_DispMemOps_half1);

		
    }else if(2 == testid){
		//reada BIG  file to sunximem
		ReadPicFileContent(pdcompPicPath0,&m_DispMemOps,iWidth,iHeight);

		//alloc de-compse buff
		allocPicMem(&m_DispMemOps0,iSubWidth*iSubHeight*3/2);
		allocPicMem(&m_DispMemOps1,iSubWidth*iSubHeight*3/2);
		allocPicMem(&m_DispMemOps2,iSubWidth*iSubHeight*3/2);
		allocPicMem(&m_DispMemOps3,iSubWidth*iSubHeight*3/2);

		//compose
		int g2dfd=g2dInit();
		
		//int g2dClip(int g2dHandle,void* psrc, int src_w, int src_h, int src_x, int src_y, int width, int height, void* pdst, int dst_w, int dst_h, int dst_x, int dst_y)
		t1.tv_sec = t1.tv_usec = 0;
	    t2.tv_sec = t2.tv_usec = 0;
		printf("start trans 1x 2560x1440  to 4x 1280x720 to decompose \r\n");
		gettimeofday(&t1, NULL);

		
		g2dClip(g2dfd,(void *)m_DispMemOps.phy, iWidth, iHeight, 0, 0, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps0.phy, iSubWidth, iSubHeight, 0, 0);

		g2dClip(g2dfd,(void *)m_DispMemOps.phy, iWidth, iHeight, iSubWidth, 0, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps1.phy, iSubWidth, iSubHeight, 0, 0);
		
		g2dClip(g2dfd,(void *)m_DispMemOps.phy, iWidth, iHeight, 0, iSubHeight, iSubWidth, iSubHeight, \
						(void *)m_DispMemOps2.phy, iSubWidth, iSubHeight, 0, 0);
		
		g2dClip(g2dfd,(void *)m_DispMemOps.phy, iWidth, iHeight, iSubWidth, iSubHeight, iSubWidth, iSubWidth, \
						(void *)m_DispMemOps3.phy, iSubWidth, iSubHeight, 0, 0);
		gettimeofday(&t2, NULL);
		printf("end trans 1x 2560x1440  to 4x 1280x720   time =%lld \r\n",int64_t(t2.tv_sec)*1000000000LL + int64_t(t2.tv_usec)*1000LL-(int64_t(t1.tv_sec)*1000000000LL + int64_t(t1.tv_usec)*1000LL));

		g2dUnit(g2dfd);
		
		//save de-compose buffer
		WritePicFileContent(pdPicPath0,&m_DispMemOps0,iSubWidth, iSubHeight);
		WritePicFileContent(pdPicPath1,&m_DispMemOps1,iSubWidth, iSubHeight);
		WritePicFileContent(pdPicPath2,&m_DispMemOps2,iSubWidth, iSubHeight);
		WritePicFileContent(pdPicPath3,&m_DispMemOps3,iSubWidth, iSubHeight);

		//free mem
		freePicMem(&m_DispMemOps);
		freePicMem(&m_DispMemOps0);
		freePicMem(&m_DispMemOps1);
		freePicMem(&m_DispMemOps2);
		freePicMem(&m_DispMemOps2);
    	}
	else {
        printf("Invid Param\n");
    }
    return 0;
}

/**************************************
end
***************************************/

