#!/bin/sh
set -e

#if [ -d ./carbit ];then
#	echo compileing  carbit 
#	cd ./carbit
#	make
#	cd ../
#fi

if [ -d ./AwH264VDecoder ];then
	echo compileing  AwH264VDecoder 
	cd ./AwH264VDecoder
	make
	cd ../
fi


if [ -d ./G2dDemo ];then
	echo compileing  G2dDemo 
	cd ./G2dDemo
	make
	cd ../
fi

if [ -d ./recordTest ];then
	echo compileing  recordTest 
	cd ./recordTest
	make
	cd ../
fi

if [ -d ./sdktest ];then
	echo compileing  sdktest 
	cd ./sdktest
	make
	cd ../
fi

if [ -d ./t7arecord ];then
	echo compileing  t7arecord 
	cd ./t7arecord

	make

	cd ../
fi

if [ -d ./videoservice ];then
	echo compileing  videoservice 
	cd ./videoservice

	make

	cd ../
fi

if [ -d ./videoproxy ];then
	echo compileing  videoproxy 
	cd ./videoproxy

	make

	cd ../
fi


if [ -d ./xplayerdemo ];then
	echo compileing  xplayerdemo 
	cd ./xplayerdemo
	make
	cd ../
fi

if [ -d ./xplayerdemoH264 ];then
	echo compileing  xplayerdemoH264 
	cd ./xplayerdemoH264
	make
	cd ../
fi

echo "build done!"