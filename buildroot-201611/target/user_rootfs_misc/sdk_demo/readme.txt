使用方法：
手动执行本目录下的build.sh,或者进入单个demo目录执行make都可以把对应的demo编译出来
rebuildAll.sh会进入每个目录，然后执行make clean后再执行make

rebuildAll.sh和build.sh不一样的地方是：rebuildAll.sh会在执行make前先执行make clean,把原来工程的依赖文件以及所有.o文件都删除。

├── carbit				 	 手机互联
├── recordTest				 录像demo
├── sdktest 				 摄像头相关API测试demo
├── xplayerdemo			     播放器demo
└── xplayerdemoH264			 与xplayerdemo类似，但是xplayerdemo是基于文件的，xplayerdemoH264是基于流的，专用于手机互联场景

