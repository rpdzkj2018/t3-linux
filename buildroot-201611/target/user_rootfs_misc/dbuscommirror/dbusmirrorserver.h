#ifndef __SERVER_H__
#define __SERVER_H__

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

typedef int (* fn_set_cmd_handler)(int type,int val0,int val1,int val2,void* pdat);

#ifdef  __cplusplus
extern "C" {
#endif

int sMirrorServRegSetCmdHandl(void* cb);
int sMirrorEmitSignal(int i_arg1,int i_arg2);
int sMirrorServerInit(void);

#ifdef  __cplusplus
}
#endif

#endif
