#ifndef __LIB_CLIENT__
#define __LIB_CLIENT__

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

typedef void (* fn_signal_handler)(int type,int val,char *arg,void* pdat);

#ifdef  __cplusplus
extern "C" {
#endif

int cMirrorClientSetCmd( int in_arg1, int in_arg2, int in_arg3, int in_arg4,  int *out_arg2);

int cMirrorClientInit(void *p);
int cMirrorClientRegSingalHandl(void* cb);

#ifdef  __cplusplus
}
#endif

#endif
