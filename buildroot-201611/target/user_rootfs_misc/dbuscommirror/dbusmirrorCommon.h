#ifndef FORYOU_GDBUS_EXAMPLE_H
#define FORYOU_GDBUS_EXAMPLE_H

#include <gio/gio.h> /* Bus define */

#define AW_GDBUS_MIRROR_BUS_NAME    "com.aw.Gdbus.Mirror"
#define AW_GDBUS_MIRROR_OBJECT_PATH "/com/aw/Gdbus/Mirror"
typedef int (*method_cb)(int msgType, char *dataPtr,int dalen,void *user);

#endif