#ifndef AW_VIDEO_ENCORDER_H
#define AW_VIDEO_ENCORDER_H
/***********************************************************
    Copyright (c) 2018 by Allwinnertech Co., Ltd.

            http://www.allwinnertech.com

************************************************************
File Name           :
Version             :   release 0.1
Author              :   yuanguochao

Description         :   This file is depend on Tinyalsa with cerdaX
                        for AllWinner Socs system.
                        plz read Makefile for more information.


History Information Description
Date                    Author                  Modification
2018.10.31             yuanguochao               Created file
************************************************************/

namespace awvideoencoder
{
typedef enum CodecType_
{
    CODEC_H264,
    CODEC_JPEG,
    CODEC_H264_VER2,
    CODEC_H265,
    CODEC_VP8,
} CodecType;

typedef enum PixelFmt_
{
    PIXEL_YUV420SP,//0 nv12
    PIXEL_YVU420SP,//1 nv21
    PIXEL_YUV420P, //2 i420
    PIXEL_YVU420P, //3 yv12
} PixelFmt;

typedef enum FrameCount_
{
    ONLY_ONE_FRAME = 1,
    MULTI_FRAMES,
} FrameCount;

typedef enum RotationAngle_
{
    Angle_0 = 0,
    Angle_90 = 90,
    Angle_180 = 180,
    Angle_270 = 270,
} RotationAngle;

typedef struct encode_param_t_
{

    PixelFmt    pixelFormat;
    FrameCount  frameCount;     //one for a picture,MULTI for a stream.
    CodecType   codecType;

    unsigned int srcW;          //the src size.
    unsigned int srcH;
    unsigned int dstW;          //the dst size.
    unsigned int dstH;
    RotationAngle rotation;     //Clockwise,invalid for jpeg.

    int bitRate;
    int frameRate;              //fps
    int maxKeyFrame;            //The max intarval of key frame,for h264 and h265.
} EncodeParam;

class AWVideoEncoder
{
public:
    static AWVideoEncoder* create(EncodeParam* param);
    static void destroy(AWVideoEncoder* encoder);

    /***********************************************************
     Name:        getSpsHeader
     Description: get h264 and h265 pps & sps data.
     Return:      Actual length of Header.
    ************************************************************/
    virtual int getHeader(void* header) = 0;

    /***********************************************************
     Name:        encode
     Description: encode the input data and copy to outputBuf.
     Return:      >0:Actual length of output data.
                  <0:encode failed!
    ************************************************************/
    virtual int encode(const void* inputBuf, unsigned int inputLen,
                       void* outputBuf, unsigned int outputLen) = 0;

    virtual ~AWVideoEncoder() {};
};

}

#endif
