#!/bin/sh

. ${LICHEE_BR_DIR}/../misc_config

USR_TARGET_DIR=${LICHEE_BR_OUT}/target
USER_MISC=${LICHEE_BR_DIR}/target/user_rootfs_misc
USER_EXTRA=${LICHEE_BR_DIR}/target/user_rootfs_extra
echo "run user rootfs build script------------genrootfs = "${LICHEE_GEN_ROOTFS}

if [ x${LICHEE_GEN_ROOTFS} = x ];then
	echo "not genrootfs DO NOT compile sdk lib"
	exit
fi


#WARN!!! user_rootfs_extra dir directly force copy to rootfs 
cp -prf ${LICHEE_BR_DIR}/target/user_rootfs_extra/*  ${LICHEE_BR_OUT}/target/

rm -rf ${LICHEE_BR_OUT}/target/bin_hf/
rm -rf ${LICHEE_BR_OUT}/target/lib_hf/

if [ "x${MISC_GNUEABI}" = "xgnueabihf" ] ; then
	cp -prf ${LICHEE_BR_DIR}/target/user_rootfs_extra/bin_hf/*  ${LICHEE_BR_OUT}/target/bin/
	cp -prf ${LICHEE_BR_DIR}/target/user_rootfs_extra/lib_hf/*  ${LICHEE_BR_OUT}/target/lib/
fi

#for mdev
if [ ! -d "${LICHEE_BR_OUT}/target/etc/mdev" ]; then
    mkdir -p ${LICHEE_BR_OUT}/target/etc/mdev
fi

mkdir -p 	${USR_TARGET_DIR}/usr/local/etc/
mkdir -p 	${USR_TARGET_DIR}/usr/local/sbin/
mkdir -p 	${USR_TARGET_DIR}/usr/local/bin/
mkdir -p 	${USR_TARGET_DIR}/usr/local/libexec/
mkdir -p 	${USR_TARGET_DIR}/var/run/
mkdir -p 	${USR_TARGET_DIR}/var/empty/
mkdir -p    ${USR_TARGET_DIR}/var/run/dbus/

chmod 		755 ${USR_TARGET_DIR}/var/empty 


cp ${LICHEE_BR_DIR}/../tools/pack/chips/${MISC_CHIP}/hal/gpu/${MISC_FBDEV}/lib/* ${USR_TARGET_DIR}/usr/lib/ -rf
cp ${LICHEE_BR_DIR}/../tools/pack/chips/${MISC_CHIP}/hal/gpu/${MISC_FBDEV}/bin/* ${USR_TARGET_DIR}/target/bin/ -rf


if [ -d "${LICHEE_BR_DIR}/dl/qt-everywhere-opensource-src-5.9.0/Qt-5.9.0/bin/" ];then
(


if [ -d "${USR_TARGET_DIR}/usr/local/Qt-5.9.0/lib" ]; then
#    cp -rf ${USER_MISC}/sdk/fonts ${USR_TARGET_DIR}/usr/local/Qt-5.9.0/lib/
	(cd ${USR_TARGET_DIR}/usr/local/Qt-5.9.0/lib
	rm *.a *.la 
	rm cmake -rf
	)
fi
)
else
echo "you must compile QT5.9.0 first!!!!!!!"
sleep 2
fi
if [ -d ${USER_MISC}/sdk_lib ];then
(cd ${USER_MISC}/sdk_lib
make
cp -f ${USER_MISC}/sdk_lib/libsdk.so ${USR_TARGET_DIR}/usr/lib/
cp -f ${USER_MISC}/sdk_lib/${MISC_SDKLIB}/*.so* ${USR_TARGET_DIR}/usr/lib/
cp -v ${USER_MISC}/sdk_lib/cedarx/${MISC_CEDARXLIB}/*.so*  ${USR_TARGET_DIR}/usr/lib/
)
fi

if [ -d ${USER_MISC}/sdk_demo/sdktest ];then
(cd ${USER_MISC}/sdk_demo/sdktest
make
cp -f ${USER_MISC}/sdk_demo/sdktest/sdktest ${USR_TARGET_DIR}/
cp -f ${USER_MISC}/sdk_demo/sdktest/eve_module/classifier/* ${USR_TARGET_DIR}/usr/lib/
)
fi

if [ -d ${USER_MISC}/sdk_demo/fbinit ];then
(cd ${USER_MISC}/sdk_demo/fbinit
make
cp -f ${USER_MISC}/sdk_demo/fbinit/fbinit ${USR_TARGET_DIR}/bin/
)
fi

if [ -d ${USER_MISC}/sdk_demo/recordTest ];then
(cd ${USER_MISC}/sdk_demo/recordTest
make
cp -f ${USER_MISC}/sdk_demo/recordTest/recordTest ${USR_TARGET_DIR}/
)
fi

if [ -d ${USER_MISC}/sdk_demo/G2dDemo ];then
(cd ${USER_MISC}/sdk_demo/G2dDemo
make
cp -f ${USER_MISC}/sdk_demo/G2dDemo/G2dDemo ${USR_TARGET_DIR}/
)
fi

if [ -d ${USER_MISC}/sdk_demo/xplayerdemo ];then
(cd ${USER_MISC}/sdk_demo/xplayerdemo
make
cp -f ${USER_MISC}/sdk_demo/xplayerdemo/xplayerdemo ${USR_TARGET_DIR}/
)
fi

if [ -d ${USER_MISC}/sdk_demo/xplayerdemoH264 ];then
(cd ${USER_MISC}/sdk_demo/xplayerdemoH264
make
cp -f ${USER_MISC}/sdk_demo/xplayerdemoH264/xplayerdemoH264 ${USR_TARGET_DIR}/
)
fi

if [ -d ${USER_MISC}/sdk_demo/videoproxy ];then
(cd ${USER_MISC}/sdk_demo/videoproxy
make
cp -f ${USER_MISC}/sdk_demo/videoproxy/libvideoproxy.so ${USR_TARGET_DIR}/usr/lib
)
fi

if [ -d ${USER_MISC}/sdk_demo/videoservice ];then
(cd ${USER_MISC}/sdk_demo/videoservice
make
cp -f ${USER_MISC}/sdk_demo/videoservice/video_serv ${USR_TARGET_DIR}/
)
fi

if [ -d ${USER_MISC}/sdk_demo/carbit ];then
(cd ${USER_MISC}/sdk_demo/carbit
make
rm ${USR_TARGET_DIR}/usr/lib/libusb-1.0.so*
cp -f ${USER_MISC}/sdk_demo/carbit/libusb-1.0.so.0 ${USR_TARGET_DIR}/usr/lib/

rm ${USR_TARGET_DIR}/usr/lib/libevent*
cp -f ${USER_MISC}/sdk_demo/carbit/libevent* ${USR_TARGET_DIR}/usr/lib/

cp -f ${USER_MISC}/sdk_demo/carbit/redsocks ${USR_TARGET_DIR}/usr/bin/
cp -f ${USER_MISC}/sdk_demo/carbit/carbit ${USR_TARGET_DIR}/
)
fi

if [ -a ${USR_TARGET_DIR}/usr/lib/libcdc_ve.so ];then
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_memory.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_avs.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_h264.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_h265.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_mjpeg.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_mjpegs.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_mpeg2.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_mpeg4base.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_mpeg4dx.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_mpeg4h263.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_mpeg4normal.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_mpeg4vp6.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_vp8.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_vp9Hw.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vd_wmv3.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vdecoder.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_ve.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_vencoder.so
	rm ${USR_TARGET_DIR}/usr/lib/libcdc_videoengine.so
	echo "del old cdc lib finish"
fi


if [ -d ${USER_MISC}/qt_demo/MediaUI ];then
	cd ${USER_MISC}/qt_demo/MediaUI
	./makeMediaUI
	cp ${USER_MISC}/qt_demo/MediaUI/bin/MediaUI ${USR_TARGET_DIR}/
fi

if [ -d ${USER_MISC}/qt_demo/CameraUI ];then
	cd ${USER_MISC}/qt_demo/CameraUI
	./makeCameraUI
	cp ${USER_MISC}/qt_demo/CameraUI/CameraUI ${USR_TARGET_DIR}/
fi

if [ -d ${USER_MISC}/qt_demo/Launcher ];then
	cd ${USER_MISC}/qt_demo/Launcher
	./makeLauncher
	cp ${USER_MISC}/qt_demo/Launcher/bin/Launcher ${USR_TARGET_DIR}/
fi


echo "run user rootfs build script---------done!"