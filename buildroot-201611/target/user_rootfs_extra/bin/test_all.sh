#!/bin/bash


function tfcard()
{
echo "================ TF card test ==============="
TF_DISK=/mnt/sdcard/
if [ ! -f $TF_DISK ]; then
echo " _____ _____      ____    _    ____  ____     ___  _  __ "
echo "|_   _|  ___|    / ___|  / \  |  _ \|  _ \   / _ \| |/ / "
echo "  | | | |_ _____| |     / _ \ | |_) | | | | | | | | ' /  "
echo "  | | |  _|_____| |___ / ___ \|  _ <| |_| | | |_| | . \  "
echo "  |_| |_|        \____/_/   \_\_| \_\____/   \___/|_|\_\ "

else
       echo "NO TF"
	exit 1
fi
}


function ethernet()
{
echo "================ Ethernet test ==============="
ifconfig wlan0 down
udhcpc eth0 >/dev/null  2>&1
sleep 1
ping -c 3 www.baidu.com >/dev/null  2>&1
if [ $? -eq 0 ]
    then
echo " _____ _____ _   _ _____ ____  _   _ _____ _____    ___  _  __ "
echo "| ____|_   _| | | | ____|  _ \| \ | | ____|_   _|  / _ \| |/ / "
echo "|  _|   | | | |_| |  _| | |_) |  \| |  _|   | |   | | | | ' /  "
echo "| |___  | | |  _  | |___|  _ <| |\  | |___  | |   | |_| | . \  "
echo "|_____| |_| |_| |_|_____|_| \_\_| \_|_____| |_|    \___/|_|\_\ "

else
    echo "Ethernet connect fail"
	exit 1
fi
}


function wifi() 
{
echo "================ Wifi test ==============="
echo "__        _____ _____ ___   _____ _____ ____ _____  "
echo "\ \      / /_ _|  ___|_ _| |_   _| ____/ ___|_   _| "
echo " \ \ /\ / / | || |_   | |    | | |  _| \___ \ | |   "
echo "  \ V  V /  | ||  _|  | |    | | | |___ ___) || |   "
echo "   \_/\_/  |___|_|   |___|   |_| |_____|____/ |_|   "

ifconfig wlan0 up                                                  
ifconfig eth0 down
iw dev wlan0 scan | grep SSID
}

function music() 
{
echo "================ Audio test ==============="
echo "Please listen music"
echo " __  __ _   _ ____ ___ ____  "
echo "|  \/  | | | / ___|_ _/ ___| "
echo "| |\/| | | | \___ \| | |     "
echo "| |  | | |_| |___) | | |___  "
echo "|_|  |_|\___/|____/___\____| "

aplay  -D hw:audiocodec /test.wav                    

}

function uart2()
{
echo "================ Uart2 test ==============="
echo " _   _   _    ____ _____   _____ _____ ____ _____  "
echo "| | | | / \  |  _ \_   _| |_   _| ____/ ___|_   _| "
echo "| | | |/ _ \ | |_) || |     | | |  _| \___ \ | |   "
echo "| |_| / ___ \|  _ < | |     | | | |___ ___) || |   "
echo " \___/_/   \_\_| \_\|_|     |_| |_____|____/ |_|   "

echo "test uart2"
uart_test /dev/ttyS2
}

function uart4()
{
echo "================ Uart4 test ==============="
echo " _   _   _    ____ _____   _____ _____ ____ _____  "
echo "| | | | / \  |  _ \_   _| |_   _| ____/ ___|_   _| "
echo "| | | |/ _ \ | |_) || |     | | |  _| \___ \ | |   "
echo "| |_| / ___ \|  _ < | |     | | | |___ ___) || |   "
echo " \___/_/   \_\_| \_\|_|     |_| |_____|____/ |_|   "

echo "test uart4"
uart_test /dev/ttyS4
}

function uart5()
{
echo "================ Uart5 test ==============="
echo " _   _   _    ____ _____   _____ _____ ____ _____  "
echo "| | | | / \  |  _ \_   _| |_   _| ____/ ___|_   _| "
echo "| | | |/ _ \ | |_) || |     | | |  _| \___ \ | |   "
echo "| |_| / ___ \|  _ < | |     | | | |___ ___) || |   "
echo " \___/_/   \_\_| \_\|_|     |_| |_____|____/ |_|   "

echo "test uart5"
uart_test /dev/ttyS5
}


function uart7()
{
echo "================ Uart7 test ==============="
echo " _   _   _    ____ _____   _____ _____ ____ _____  "
echo "| | | | / \  |  _ \_   _| |_   _| ____/ ___|_   _| "
echo "| | | |/ _ \ | |_) || |     | | |  _| \___ \ | |   "
echo "| |_| / ___ \|  _ < | |     | | | |___ ___) || |   "
echo " \___/_/   \_\_| \_\|_|     |_| |_____|____/ |_|   "

echo "test uart7"
uart_test /dev/ttyS7
}



function net-4G()
{
echo "================ 4G test ==============="
ifconfig eth0 down
ifconfig wlan0 down
. /etc/ppp/peers/quectel-pppd.sh 2>&1

sleep 10
ping -c 3 www.baidu.com >/dev/null  2>&1

	if [ $? -eq 0 ]
		then
	echo " _  _    ____    ___  _  __ "
	echo "| || |  / ___|  / _ \| |/ / "
	echo "| || |_| |  _  | | | | ' /  "
	echo "|__   _| |_| | | |_| | . \  "
	echo "   |_|  \____|  \___/|_|\_\ "
	else
		echo "4G connect fail"
		exit 1
	fi
}

function record()
{
	echo "================ AUDIO test ==============="
	ps | grep -w '/CameraUI' | cut -c 2-5 | xargs kill -9 2>&1
	ps | grep -w '/MediaUI' | cut -c 2-5 | xargs kill -9 2>&1
	sleep 1

	echo " 	    _   _   _ ____ ___ ___    _____ _____ ____ _____ "
	echo "    / \ | | | |  _ \_ _/ _ \  |_   _| ____/ ___|_   _| "
	echo "   / _ \| | | | | | | | | | |   | | |  _| \___ \ | |   "
	echo "  / ___ \ |_| | |_| | | |_| |   | | | |___ ___) || |   "
	echo " /_/   \_\___/|____/___\___/    |_| |_____|____/ |_|   "
                                                     
	arecord  -D hw:audiocodec -d 5 -f cd -t wav 123.wav
	aplay  -D hw:audiocodec 123.wav
}

while [ 1 -lt 2 ]
do
echo "------------------------------------------------"
echo "---------------start function test--------------"
echo "------------------------------------------------"
echo "1、tf-card"
echo "2、ethernet"
echo "3、wifi"
echo "4、music"
echo "5、uart2"
echo "6、uart4"
echo "7、uart5"
echo "8、uart7"
echo "9、4G"
echo "10、audio"
echo -n "Please input num:"
read num

if [ $num -eq 1 ];then
	tfcard
elif [ $num -eq 2 ];then
	ethernet
elif [ $num -eq 3 ];then
	wifi
elif [ $num -eq 4 ];then
	music
elif [ $num -eq 5 ];then
	uart2
elif [ $num -eq 6 ];then
        uart4
elif [ $num -eq 7 ];then
        uart5
elif [ $num -eq 8 ];then
        uart7
elif [ $num -eq 9 ];then
	net-4G
elif [ $num -eq 10 ];then
	record
else
	echo "no valid"
fi
done
