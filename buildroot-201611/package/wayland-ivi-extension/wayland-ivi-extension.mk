################################################################################
#
# wayland-ivi-extension
#
################################################################################

WAYLAND_IVI_EXTENSION_VERSION = 1.13.0
WAYLAND_IVI_EXTENSION_SITE = http://github.com/GENIVI/wayland-ivi-extension/releases
WAYLAND_IVI_EXTENSION_SOURCE = wayland-ivi-extension-$(WAYLAND_IVI_EXTENSION_VERSION).tar.gz
WAYLAND_IVI_EXTENSION_LICENSE = MIT
WAYLAND_IVI_EXTENSION_LICENSE_FILES = COPYING
WAYLAND_IVI_EXTENSION_INSTALL_STAGING = YES
WAYLAND_IVI_EXTENSION_DEPENDENCIES = weston


# wayland-scanner is only needed for building, not on the target
WAYLAND_IVI_EXTENSION_CONF_OPTS = -DBUILD_ILM_API_TESTS=1 -DWITH_ILM_INPUT=1

$(eval $(cmake-package))
