#############################################################
#
# libmgeff
#
#############################################################
LIBMGEFF_VERSION = 1.0.0
LIBMGEFF_SOURCE = libmgeff-$(LIBMGEFF_VERSION).tar.gz
LIBMGEFF_SITE = http://www.minigui.org/downloads/

LIBMGEFF_INSTALL_STAGING=YES
#LIBMGEFF_CONF_OPT +=  --target=arm-linux --host=arm-linux --build=x86_64-unknown-linux-gnu \
--with-targetname=fbcon  --enable-autoial  --disable-pcxvfb \
--disable-vbfsupport  --enable-tslibial --enable-debug
#--prefix=$(TARGET_DIR)/usr
LIBMGEFF_DEPENDENCIES += tslib zlib jpeg libpng libmgplus
#LIBMINIGUI_FLAGS = -I$(TARGET_DIR)/usr/include/minigui 

$(eval $(autotools-package))
$(eval $(host-autotools-package))
