#############################################################
#
# LIBMGUTILS
#
#############################################################
LIBMGUTILS_VERSION = 1.0.4
LIBMGUTILS_SOURCE = libmgutils-$(LIBMGUTILS_VERSION).tar.gz
LIBMGUTILS_SITE = http://www.minigui.org/downloads/

LIBMGUTILS_INSTALL_STAGING=YES
LIBMGUTILS_CONF_ENV = LIBS=-lts
#--prefix=$(TARGET_DIR)/usr
LIBMGUTILS_DEPENDENCIES += tslib zlib jpeg libpng libminigui
#LIBMINIGUI_FLAGS = -I$(TARGET_DIR)/usr/include/minigui 

#$(eval $(call GENTARGETS,package,libminigui,host))
$(eval $(autotools-package))
