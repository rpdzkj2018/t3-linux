#############################################################
#
# MG_SAMPLES
#
#############################################################
MG_SAMPLES_VERSION = 3.0.12
MG_SAMPLES_SOURCE = mg-samples-$(LIBMINIGUI_VERSION).tar.gz
MG_SAMPLES_SITE = http://www.minigui.org/downloads


MG_SAMPLES_INSTALL_STAGING=NO
MG_SAMPLES_INSTALL_TARGET=YES
MG_SAMPLES_CONF_ENV = LIBS=-lts
MG_SAMPLES_CONF_OPTS +=  --target=$(GNU_TARGET_NAME) --host=$(GNU_TARGET_NAME) --build=$(GNU_HOST_NAME)

MG_SAMPLES_LDFLAGS = -I$(STAGING_DIR)/usr/include/minigui -I$(STAGING_DIR)/usr/include/ -L$(STAGING_DIR)/usr/lib/libgui.la

define MG_SAMPLES_INSTALL_TARGET_CMDS
	cp -rf $(@D)/same $(TARGET_DIR)/usr/bin/
endef




$(eval $(autotools-package))
