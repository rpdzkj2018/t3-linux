#############################################################
#
# iphone-like-demo
#
#############################################################
IPHONE_LIKE_DEMO_VERSION = 1.0.0
IPHONE_LIKE_DEMO_SOURCE = iphone-like-demo-$(IPHONE_LIKE_DEMO_VERSION).tar.gz
IPHONE_LIKE_DEMO_SITE = http://www.minigui.org/downloads/

IPHONE_LIKE_DEMO_INSTALL_STAGING=YES
#IPHONE_LIKE_DEMO_CONF_OPT +=  --target=arm-linux --host=arm-linux --build=x86_64-unknown-linux-gnu \
--with-targetname=fbcon  --enable-autoial  --disable-pcxvfb \
--disable-vbfsupport  --enable-tslibial --enable-debug
#--prefix=$(TARGET_DIR)/usr
IPHONE_LIKE_DEMO_DEPENDENCIES += tslib zlib jpeg libpng
#LIBMINIGUI_FLAGS = -I$(TARGET_DIR)/usr/include/minigui 

$(eval $(autotools-package))
