#############################################################
#
# libminigui
#
#############################################################
LIBMINIGUI_VERSION = 3.0.12
LIBMINIGUI_SOURCE = libminigui-gpl-$(LIBMINIGUI_VERSION).tar.gz
LIBMINIGUI_SITE = http://www.minigui.org/downloads

LIBMINIGUI_INSTALL_STAGING=YES
LIBMINIGUI_INSTALL_TARGET=YES
LIBMINIGUI_CONF_OPTS +=  --target=$(GNU_TARGET_NAME) --host=$(GNU_TARGET_NAME) --build=$(GNU_HOST_NAME) \
--with-targetname=fbcon  --enable-autoial  --disable-pcxvfb \
--disable-vbfsupport  --enable-tslibial --enable-debug
#--prefix=$(STAGING_DIR)/usr
LIBMINIGUI_DEPENDENCIES += tslib zlib jpeg libpng
#LIBMINIGUI_FLAGS = -I$(TARGET_DIR)/usr/include/minigui 

define LIBMINIGUI_INSTALL_STAGING_CMDS
	cp -rf $(@D)/src/.libs/libminigui_*.so* $(STAGING_DIR)/usr/lib/
	cp -rf $(@D)/src/libminigui_*.la $(STAGING_DIR)/usr/lib/
	cp -rf $(@D)/mgconfig.h $(STAGING_DIR)/usr/include/minigui/
	cp -rf $(@D)/src/include/dc.h $(STAGING_DIR)/usr/include/minigui/
	cp -rf $(@D)/minigui.pc $(STAGING_DIR)/usr/lib/pkgconfig/
endef

define LIBMINIGUI_INSTALL_TARGET_CMDS
	cp -rf $(@D)/src/.libs/libminigui_*.so* $(TARGET_DIR)/usr/lib/
	cp -rf $(@D)/src/libminigui_*.la $(TARGET_DIR)/usr/lib/
	cp -rf package/minigui/libminigui/MiniGUI.cfg $(TARGET_DIR)/etc/
endef


$(eval $(autotools-package))
$(eval $(host-autotoolsackage))
