#############################################################
#
# MINIGUI_RES_BE
# http://www.minigui.org/downloads/minigui-res-be-3.0.12.tar.gz
#############################################################
MINIGUI_RES_BE_VERSION:=3.0.12
MINIGUI_RES_BE_SOURCE:=minigui-res-be-$(MINIGUI_RES_BE_VERSION).tar.gz
MINIGUI_RES_BE_SITE:=http://www.minigui.org/downloads

MINIGUI_RES_BE_INSTALL_STAGING=YES
MINIGUI_RES_BE_INSTALL_TARGET = YES   
#MINIGUI_RES_BE_CONF_OPT =

define MINIGUI_RES_BE_CONFIGURE_CMDS
	(cd $(@D); \
		./configure \
	)
	mkdir -p $(TARGET_DIR)/usr/share/minigui/res/
	cp -rf $(@D)/bmp/ $(TARGET_DIR)/usr/share/minigui/res/
	cp -rf $(@D)/cursor/ $(TARGET_DIR)/usr/share/minigui/res/
	cp -rf $(@D)/font/ $(TARGET_DIR)/usr/share/minigui/res/
	cp -rf $(@D)/icon/ $(TARGET_DIR)/usr/share/minigui/res/
endef

define MINIGUI_RES_BE_BUILD_CMDS

endef

define MINIGUI_RES_BE_INSTALL_STAGING_CMDS

endef

define MINIGUI_RES_BE_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/share/minigui/res/
	cp -rf $(@D)/bmp/ $(TARGET_DIR)/usr/share/minigui/res/
	cp -rf $(@D)/cursor/ $(TARGET_DIR)/usr/share/minigui/res/
	cp -rf $(@D)/font/ $(TARGET_DIR)/usr/share/minigui/res/
	cp -rf $(@D)/icon/ $(TARGET_DIR)/usr/share/minigui/res/
endef


$(eval $(autotools-package))
