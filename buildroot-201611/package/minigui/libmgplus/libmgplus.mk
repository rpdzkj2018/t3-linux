#############################################################
#
# libmgplus
#
#############################################################
LIBMGPLUS_VERSION = 1.2.4
LIBMGPLUS_SOURCE = libmgplus-$(LIBMGPLUS_VERSION).tar.gz
LIBMGPLUS_SITE = http://www.minigui.org/downloads/

LIBMGPLUS_INSTALL_STAGING=YES
#LIBMGPLUS_CONF_OPT +=  --target=arm-linux --host=arm-linux --build=x86_64-unknown-linux-gnu \
--with-targetname=fbcon  --enable-autoial  --disable-pcxvfb \
--disable-vbfsupport  --enable-tslibial --enable-debug
#--prefix=$(TARGET_DIR)/usr
LIBMGPLUS_DEPENDENCIES += tslib zlib jpeg libpng libminigui
#LIBMINIGUI_FLAGS = -I$(TARGET_DIR)/usr/include/minigui 

#$(eval $(call GENTARGETS,package,libminigui,host))
$(eval $(autotools-package))
