#############################################################
#
# LIBMGNCS
#
#############################################################
LIBMGNCS_VERSION = 1.0.8
LIBMGNCS_SOURCE = libmgncs-$(LIBMGNCS_VERSION).tar.gz
LIBMGNCS_SITE = http://www.minigui.org/downloads/

LIBMGNCS_INSTALL_STAGING=YES
#LIBMGNCS_CONF_OPT +=  --target=arm-linux --host=arm-linux --build=x86_64-unknown-linux-gnu \
--with-targetname=fbcon  --enable-autoial  --disable-pcxvfb \
--disable-vbfsupport  --enable-tslibial --enable-debug
#--prefix=$(TARGET_DIR)/usr
LIBMGNCS_DEPENDENCIES += tslib zlib jpeg libpng libminigui libmgutils
#LIBMINIGUI_FLAGS = -I$(TARGET_DIR)/usr/include/minigui 

#$(eval $(call GENTARGETS,package,libmgncs,host))
$(eval $(autotools-package))
