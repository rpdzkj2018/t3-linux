#############################################################
#
# libmgeff
#
#############################################################
#LIBMGEFF_VERSION = 1.0.0
CELLPHONEUXDEMO_SOURCE = cellphoneuxdemo.tar.gz
CELLPHONEUXDEMO_SITE = http://www.minigui.org/downloads/

CELLPHONEUXDEMO_INSTALL_STAGING=NO
#LIBMGEFF_CONF_OPT +=  --target=arm-linux --host=arm-linux --build=x86_64-unknown-linux-gnu \
--with-targetname=fbcon  --enable-autoial  --disable-pcxvfb \
--disable-vbfsupport  --enable-tslibial --enable-debug
#--prefix=$(TARGET_DIR)/usr
CELLPHONEUXDEMO_DEPENDENCIES += tslib zlib jpeg libpng libminigui sqlite libmgncs libmgutils \
     libmgplus libmgeff minigui-res-be
#LIBMINIGUI_FLAGS = -I$(TARGET_DIR)/usr/include/minigui 


$(eval $(autotools-package))
$(eval $(host-autotools-package))
