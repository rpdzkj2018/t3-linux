FSCK_MSDOS_DIR:=$(BUILD_DIR)/fsck-msdos

	
define FSCK_MSDOS_BUILD_CMDS
	mkdir -pv $(FSCK_MSDOS_DIR)  $(FSCK_MSDOS_HOST_DIR)
	cp -rf package/fsck_msdos/fsck/* $(FSCK_MSDOS_DIR)
	mkdir -pv $(HOST_DIR)/usr/bin
	$(MAKE) BUILD_DIR=$(BUILD_DIR) CC="$(TARGET_CC)" -C $(FSCK_MSDOS_DIR) 
endef

define FSCK_MSDOS_INSTALL_TARGET_CMDS
	$(MAKE) DESTDIR="$(TARGET_DIR)" -C $(FSCK_MSDOS_DIR) install
endef

$(eval $(generic-package))
