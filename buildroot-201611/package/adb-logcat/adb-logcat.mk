################################################################################
#
# adb_logcat
#
################################################################################

ADB_LOGCAT_VERSION = 1.0
ADB_LOGCAT_SITE = https://adb.android.com/releases
ADB_LOGCAT_SOURCE = adb-logcat-$(ADB_LOGCAT_VERSION).tar.gz
ADB_LOGCAT_LICENSE = LGPL
ADB_LOGCAT_LICENSE_FILES = COPYING


define ADB_LOGCAT_INSTALL_INIT_SYSV
	$(INSTALL) -D -m 0755 package/adb-logcat/S02adbd $(TARGET_DIR)/etc/init.d/S02adbd
endef

$(eval $(autotools-package))
