bsptest_err()
{
    if [ -L /bin/sh ];then
        printf "\033[47;31mERROR: $*\033[0m\n"
    else
        echo -e "\033[47;31mERROR: $*\033[0m"
    fi
}

bsptest_warn()
{
    if [ -L /bin/sh ];then
        printf "\033[47;34mWARN: $*\033[0m\n"
    else
        echo -e "\033[47;34mWARN: $*\033[0m"
    fi
}

bsptest_pass()
{
    if [ -L /bin/sh ];then
        printf "\033[47;34m$*\033[0m\033[0;32m[PASS] \033[0m\n"
    else
        echo -e "\033[47;34m$*\033[0m\033[0;32m[PASS] \033[0m"
    fi
}

bsptest_fail()
{
    if [ -L /bin/sh ];then
        printf "\033[47;34m$*\033[0m\033[0;31m[FAIL] \033[0m\n"
    else
        echo -e "\033[47;34m$*\033[0m\033[0;31m[FAIL] \033[0m"
    fi
}

bsptest_info()
{
    if [ -L /bin/sh ];then
        printf "\033[47;30mINFO: $*\033[0m\n"
    else
        echo -e "\033[47;30mINFO: $*\033[0m"
    fi
}

bsptest_regmask()
{
    para_num=$#
    if [ "x$para_num" = "x2" ];then
        let "var=1<<$2"
        let "var=$var-1"
        let "var=$var<<$1"
        printf "0x%x" $var
    elif [ "x$para_num" = "x4" ];then
        let "var1=1<<$2"
        let "var1=$var1-1"
        let "var1=$var1<<$1"

        let "var2=1<<$4"
        let "var2=$var2-1"
        let "var2=$var2<<$3"

        let "var=$var1|$var2"
        printf "0x%x" $var
    elif [ "x$para_num" = "x6" ];then
        let "var1=1<<$2"
        let "var1=$var1-1"
        let "var1=$var1<<$1"

        let "var2=1<<$4"
        let "var2=$var2-1"
        let "var2=$var2<<$3"

        let "var3=1<<$6"
        let "var3=$var3-1"
        let "var3=$var3<<$5"

        let "var=$var1|$var2|$var3"
        printf "0x%x" $var
    elif [ "x$para_num" = "x8" ];then
        let "var1=1<<$2"
        let "var1=$var1-1"
        let "var1=$var1<<$1"

        let "var2=1<<$4"
        let "var2=$var2-1"
        let "var2=$var2<<$3"

        let "var3=1<<$6"
        let "var3=$var3-1"
        let "var3=$var3<<$5"

        let "var4=1<<$8"
        let "var4=$var4-1"
        let "var4=$var4<<$7"

        let "var=$var1|$var2|$var3|$var4"
        printf "0x%x" $var
    elif [ "x$para_num" = "x10" ];then
        let "var1=1<<$2"
        let "var1=$var1-1"
        let "var1=$var1<<$1"

        let "var2=1<<$4"
        let "var2=$var2-1"
        let "var2=$var2<<$3"

        let "var3=1<<$6"
        let "var3=$var3-1"
        let "var3=$var3<<$5"

        let "var4=1<<$8"
        let "var4=$var4-1"
        let "var4=$var4<<$7"

        let "var5=1<<$10"
        let "var5=$var5-1"
        let "var5=$var5<<$9"

        let "var=$var1|$var2|$var3|$var4|$var5"
        printf "0x%x" $var
    else
        exit 1
    fi
}

bsptest_regvar()
{
    para_num=$#
    if [ "x$para_num" = "x2" ];then
        let "var=$2<<$1"
        printf "0x%x" $var
    elif [ "x$para_num" = "x4" ];then
        let "var1=$2<<$1"
        let "var2=$4<<$3"
        let "var=$var1|$var2"
        printf "0x%x" $var
    elif [ "x$para_num" = "x6" ];then
        let "var1=$2<<$1"
        let "var2=$4<<$3"
        let "var3=$6<<$5"
        let "var=$var1|$var2|$var3"
        printf "0x%x" $var
    elif [ "x$para_num" = "x8" ];then
        let "var1=$2<<$1"
        let "var2=$4<<$3"
        let "var3=$6<<$5"
        let "var4=$8<<$7"
        let "var=$var1|$var2|$var3|$var4"
        printf "0x%x" $var
    elif [ "x$para_num" = "x10" ];then
        let "var1=$2<<$1"
        let "var2=$4<<$3"
        let "var3=$6<<$5"
        let "var4=$8<<$7"
        let "var5=$10<<$9"
        let "var=$var1|$var2|$var3|$var4|$var5"
        printf "0x%x" $var
    else
        exit 1
    fi
}

bsptest_get_cpufreq()
{
    echo "=========cpufreq begin=============="
    i=0
    while [ $i -lt $1 ]
    do
        if [ -f /sys/devices/system/cpu/cpu${i}/cpufreq/stats/time_in_state ];then
            echo "cpu${i}"
            cat /sys/devices/system/cpu/cpu${i}/cpufreq/stats/time_in_state
        else
	        echo "cpu${i} not support cpufreq"
        fi

        i=`busybox expr $i + 1`
    done
    echo "=========cpufreq end  =============="
}

bsptest_get_cpuidle()
{
    echo "=========cpuidle begin=============="
    i=0
    while [ $i -lt $1 ]
    do
        if [ -d /sys/devices/system/cpu/cpu${i}/cpuidle ];then
            state_list=`ls /sys/devices/system/cpu/cpu${i}/cpuidle`
            for state in $state_list
            do
                state_name=$(cat /sys/devices/system/cpu/cpu${i}//cpuidle/${state}/name)
                state_time=$(cat /sys/devices/system/cpu/cpu${i}//cpuidle/${state}/time)
                printf "cpu${i}\t$state_name\t$state_time\n"
            done
        else
            echo "cpu${i} not support cpuidle"
        fi

        i=`busybox expr $i + 1`
    done
    echo "=========cpuidle end  =============="
}
bsptest_get_hotplug()
{
    echo "=========hotplug begin=============="
    i=0
    while [ $i -lt $1 ]
    do
        if [ -d /sys/kernel/autohotplug ];then
            printf "cpu${i}\t$(cat /sys/kernel/autohotplug/cpu${i}*)\n"
        fi

        i=`busybox expr $i + 1`
    done
    echo "=========hotplug end  =============="
}

bsptest_get_stat()
{
    echo "=========stat begin=============="
    cat /proc/stat
    echo "=========stat end  =============="
}

bsptest_get_uptime()
{
    echo "=========uptime begin=============="
    cat /proc/uptime|busybox awk '{print $1 }'
    echo "=========uptime end  =============="
}

bsptest_get_desc()
{
    echo "=========desc begin=============="
    printf "CPU possible:\t$(cat  /sys/devices/system/cpu/possible)\n"
    printf "CPU online:\t$(cat  /sys/devices/system/cpu/online)\n"
    if [ -f /sys/kernel/bL_switcher/active ];then
        if cat /sys/kernel/bL_switcher/active|grep "0" >/dev/null 2>&1;then
            printf "HMP: \t\tYes\n"
            printf "IKS: \t\tNo\n"
        else
            printf "HMP: \t\tNo\n"
            printf "IKS: \t\tYes\n"
        fi
    else
        printf "HMP: \t\tNo\n"
        printf "IKS: \t\tNo\n"
    fi
    if [ -f /sys/kernel/autohotplug ];then
        if [ "x$(echo $(cat /sys/kernel/autohotplug/enable))" = "x1" ];then
            printf "autohotplug: \tYes\n"
        else
            printf "autohotplug: \tNo\n"
        fi
    else
        printf "autohotplug:  \tNo\n"
    fi
    echo "=========desc end  =============="
}

bsptest_get_hwmon()
{
    echo "=========hwmon begin=============="
    if [ -d /sys/class/hwmon ];then
        for subdir in `ls /sys/class/hwmon/`
        do
            echo $(cat /sys/class/hwmon/$subdir/device/*_label) $(cat /sys/class/hwmon/$subdir/device/*_input)
        done
    fi
    echo "=========hwmon end  =============="
}

bsptest_get_info_all()
{
    cpu_num=`cat /sys/devices/system/cpu/online`
    cpu_num=${cpu_num##*-}
    cpu_num=`expr $cpu_num + 1`
    bsptest_get_desc $cpu_num
    bsptest_get_uptime
    bsptest_get_cpufreq $cpu_num
    bsptest_get_stat $cpu_num
    bsptest_get_hotplug $cpu_num
    bsptest_get_cpuidle $cpu_num
    bsptest_get_hwmon $cpu_num
}

bsptest_get_info()
{
    bsptest_get_info_all >$1 2>&1
}

# fix sata config according to .buildconfig
bsptest_fix_config()
{
    if [ ! -f $BTEST_TOP/../../.buildconfig ]; then
        bsptest_err "please build kernel first"
        exit 1
    fi

    echo update Config...
    . $BTEST_TOP/../../.buildconfig

    sed -i s/BTEST_PLATFORM=.*/BTEST_PLATFORM=${LICHEE_CHIP%%p*}/ $BTEST/Config
    sed -i s/TEST_HOST_NAME=.*/TEST_HOST_NAME=${LICHEE_CHIP%%w*}/ $BTEST/Config
    sed -i 's/KERNEL_DIR=.*/KERNEL_DIR=$BTEST\/..\/..\/..\/..\/'"$LICHEE_KERN_VER"'/' $BTEST/Config

    if [ x${LICHEE_BOARD} = "xfpga" ]; then
        sed -i s/BTEST_BOARD=.*/BTEST_BOARD=fpga/ $BTEST/Config
    else
        sed -i s/BTEST_BOARD=.*/BTEST_BOARD=evb/ $BTEST/Config
    fi
    sed -i s/LICHEE_BOARD=.*/LICHEE_BOARD=${LICHEE_BOARD}/ $BTEST/Config

    if [ x${LICHEE_ARCH} = "xarm64" ]; then
        sed -i s/ARCH=.*/ARCH=arm64/ $BTEST/Config
        sed -i s/CROSS_COMPILE=.*/CROSS_COMPILE=aarch64-linux-gnu-/ $BTEST/Config
        sed -i 's/CROSSTOOLS=.*/CROSSTOOLS=$BTEST\/..\/..\/..\/..\/out\/external-toolchain\/gcc-aarch64\//' $BTEST/Config
	if [ x$LICHEE_KERN_VER = "xlinux-4.4" -o x$LICHEE_KERN_VER = "xlinux-4.9" ];then
	    sed -i 's/CROSSTOOLS=.*/CROSSTOOLS=$BTEST\/..\/..\/..\/..\/out\/gcc-linaro-5.3.1-2016.05\/gcc-aarch64\//' $BTEST/Config
	fi
    else
        sed -i s/ARCH=.*/ARCH=arm/ $BTEST/Config
        if [ x${LICHEE_CHIP} = "xsun3iw1p1" ]; then
            sed -i s/CROSS_COMPILE=.*/CROSS_COMPILE=arm-none-linux-gnueabi-/ $BTEST/Config
        else
            sed -i s/CROSS_COMPILE=.*/CROSS_COMPILE=arm-linux-${LICHEE_GNUEABI}-/ $BTEST/Config
        fi
        sed -i 's/COMPILER_PATH=.*/COMPILER_PATH=$BTEST\/..\/..\/..\/..\/out\/${LICHEE_CHIP}\/linux\/common\/buildroot\/host\/opt\/ext-toolchain\/bin\//' $BTEST/Config
	if [ x$LICHEE_KERN_VER = "xlinux-4.4" -o x$LICHEE_KERN_VER = "xlinux-4.9" ];then
	    sed -i 's/COMPILER_PATH=.*/COMPILER_PATH=$BTEST\/..\/..\/..\/..\/out\/gcc-linaro-5.3.1-2016.05\/gcc-arm\/bin\//' $BTEST/Config
	fi
    fi
    . $BTEST/Config
    echo "p env begin"
    env
    echo "p env end"
}

bsptest_fix_data_json(){
    data_json=$BTEST_TOP/bsptest/configs/report_markdown/data.json
    if [ -f $data_json ];then
        market_code=`grep $BTEST_PLATFORM $BTEST_TOP/bsptest/configs/develop_market_table | awk -F " " '{print $2}'`
        sed -i "s/\"title_line_1\".*/\"title_line_1\" : \"$market_code\",/" $data_json
        tester=`whoami`
        sed -i "1,/\"dev\".*/{s/\"dev\".*/\"dev\":\"$tester\"\}\,/}" $data_json
        date_now=`date "+%Y.%m.%d"`
        sed -i "1,/\"date\":.*/{s/\"date\":.*/\"date\":\"$date_now\"\},/}" $data_json
    fi
}
