#/bin/sh
set -e
. ../.buildconfig

cp -f sata_platform_cfg/common.sh  linux/bsptest/script/common.sh

cd linux/bsptest

./script/bsptest.sh -b all
./script/bsptest.sh -s all

if [ -f ../${LICHEE_CHIP}.tar ];then
    cd ../
    if [ ! -d $(pwd)/../../out/${LICHEE_CHIP}/linux/common/buildroot/target/target ];then
        mkdir $(pwd)/../../out/${LICHEE_CHIP}/linux/common/buildroot/target/target
    fi
    tar xvf ${LICHEE_CHIP}.tar -C $(pwd)/../../out/${LICHEE_CHIP}/linux/common/buildroot/target/target
fi