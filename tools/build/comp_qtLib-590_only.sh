#!/bin/bash
echo "ui_autobuild:build.sh"
cd  buildroot-201611/dl

if [ x$1 != "x" ];then
    AWQT_VERSION=$1
else
    AWQT_VERSION=5.9.0
fi

echo "in param is AWQT_VERSION=${AWQT_VERSION}"
if [ -d qt-everywhere-opensource-src-${AWQT_VERSION} ];then
( 
    echo "qt-everywhere-opensource-src-${AWQT_VERSION} have been compiled before,skip!!!"
    cd ../../
    ./build.sh
    ./build.sh pack
)
else(
    echo "ui_autobuild:tar qt-everywhere-opensource-src-${AWQT_VERSION}.tgz"
    tar  -zxvf  qt-everywhere-opensource-src-${AWQT_VERSION}.tgz

    echo "ui_autobuild:cp setenvs.sh"
    cp  -f  setenvs.sh  qt-everywhere-opensource-src-${AWQT_VERSION}/

    echo "ui_autobuild:source  setenvs.sh"
    cd qt-everywhere-opensource-src-${AWQT_VERSION}/

    source  ./setenvs.sh

    echo "ui_autobuild:makeconfig"
    makeconfig

    echo "ui_autobuild:makeall"
    makeall

    echo "ui_autobuild:makeinstall"
    makeinstall

    cd ../../../
    ./build.sh
    echo "ui_autobuild: pack"
    ./build.sh pack
)
fi
